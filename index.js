// [v0.1.018-20240623]

// === module init block ===

const bsf = require('#lib/baseFunc.js');
const bso = require('#lib/baseObj.js');
const bsc = require('#lib/baseClass.js');

// === module extra block (helper functions) ===

// === module main block ===

// === module exports block ===

module.exports.isValidId = bsf.isValidId;
module.exports.isValidIndex = bsf.isValidIndex;
module.exports.isEmptyString = bsf.isEmptyString;
module.exports.isEmptyOrNotString = bsf.isEmptyOrNotString;
module.exports.isNotEmptyString = bsf.isNotEmptyString;
module.exports.isNotEmptyStringEx = bsf.isNotEmptyStringEx;
module.exports.isNullOrUndef = bsf.isNullOrUndef;
module.exports.isInteger = bsf.isInteger;
module.exports.inRangeNum = bsf.inRangeNum;

module.exports.valueToIndex = bsf.valueToIndex;
module.exports.valueToIDString = bsf.valueToIDString;
module.exports.readAsBool = bsf.readAsBool;
module.exports.readAsNumber = bsf.readAsNumber;
module.exports.readAsString = bsf.readAsString;
module.exports.readAsBoolEx = bsf.readAsBoolEx;
module.exports.readAsNumberEx = bsf.readAsNumberEx;
module.exports.tryNotNullish = bsf.tryNotNullish;
module.exports.tryToBool = bsf.tryToBool;
module.exports.tryToNumber = bsf.tryToNumber;

module.exports.isArray = bso.isArray;
module.exports.isObject = bso.isObject;
module.exports.isPlainObject = bso.isPlainObject;
module.exports.isDateObject = bso.isDateObject;
module.exports.isPropertyExists = bso.isPropertyExists;
module.exports.isMethodExists = bso.isMethodExists;

module.exports.arrayFrom = bso.arrayFrom;
module.exports.valueToArray = bso.valueToArray;
module.exports.valueToArrayEx = bso.valueToArrayEx;
module.exports.valueToEntry = bso.valueToEntry;
module.exports.valueToEntries = bso.valueToEntries;
module.exports.readAsListE = bso.readAsListE;
module.exports.readAsListS = bso.readAsListS;
module.exports.doMethodIfExists = bso.doMethodIfExists;

module.exports.classes = bsc;
module.exports.TItemsList = bsc.TItemsList;
module.exports.TItemsListEx = bsc.TItemsListEx;
module.exports.TNamedItemsCollection = bsc.TNamedItemsCollection;
module.exports.TNamedSetsCollection = bsc.TNamedSetsCollection;
module.exports.TItemsICollection = bsc.TItemsICollection;
