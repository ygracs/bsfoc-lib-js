// [v0.1.053-20240623]

// === module init block ===

const {
  isArray, isPlainObject,
} = require('./lib-hfunc');

// === module extra block (helper functions) ===

// === module main block ===

/***
 * (* constant definitions *)
 */

/**
 * a ref to `Number.isInteger()` method
 */
const isInteger = Number.isInteger;

/**
 * @inner
 * @description A base map for ref from a string value to a boolean value.
 */
const mpStrToBoolRefBase = new Map([
  [ 'false', false ],
  [ 'FALSE', false ],
  [ 'true', true ],
  [ 'TRUE', true ],
]);

/**
 * @inner
 * @description A extended map for ref from a string value to a boolean value.
 */
const mpStrToBoolRefEx = new Map([
  [ 'false', false ],
  [ 'FALSE', false ],
  [ 'true', true ],
  [ 'TRUE', true ],
  [ 'null', false ],
  [ 'NULL', false ],
  [ 'off', false ],
  [ 'OFF', false ],
  [ 'on', true ],
  [ 'ON', true ],
  [ 'no', false ],
  [ 'No', false ],
  [ 'NO', false ],
  [ 'yes', true ],
  [ 'Yes', true ],
  [ 'YES', true ],
]);

/***
 * (* function definitions *)
 */

/**
 * @function valueToIndex
 * @param {any}
 * @returns {number}
 * @description Converts a given value to an index value.
 *              If failed a `-1` is returned.
 */
function valueToIndex(value){
  let index = value;
  let result = -1;
  switch (typeof index) {
    case 'string': {
      if ((index = index.trim()) === '') break;
      index = Number(index);
    }
    case 'number': {
      if (Number.isNaN(index)) break;
      if (index < 0 || !isInteger(index)) break;
      result = index;
      break;
    }
    default: {
      break;
    }
  };
  return result;
};

/**
 * @function isValidIndex
 * @param {any}
 * @returns {bool}
 * @description Checks whether a given value can be converted
 *              to an index value.
 */
function isValidIndex(value){
  return valueToIndex(value) !== -1;
};

/**
 * @function isEmptyString
 * @param {any}
 * @returns {bool}
 * @description Checks whether a given value is an empty string.
 */
function isEmptyString(value){
  if (typeof value !== 'string') return false;
  return value === '' || value.trim() === '';
};

/**
 * @function isEmptyOrNotString
 * @param {any}
 * @returns {bool}
 * @description Checks whether a given value is an empty string
 *              or not a string at all.
 */
function isEmptyOrNotString(value){
  if (typeof value !== 'string') return true;
  return value === '' || value.trim() === '';
};

/**
 * @function isNotEmptyString
 * @param {any}
 * @returns {bool}
 * @description Checks whether a given value is not an empty string.
 */
function isNotEmptyString(value){
  if (typeof value !== 'string') return false;
  return value !== '' && value.trim() !== '';
};

// // NOTE: experimental function
function isNotEmptyStringEx(...args) {
  let isSUCCEED = false;
  for (let value of args) {
    if (isArray(value)) {
      isSUCCEED = isNotEmptyStringEx(...value);
    } else {
      isSUCCEED = isNotEmptyString(value);
    };
    if (!isSUCCEED) break;
  };
  return isSUCCEED;
};

/**
 * @function valueToIDString
 * @param {any}
 * @param {(bool|object)} [opt={}]
 * @param {bool} [opt.onlyNES=true]
 * @param {bool} [opt.ignoreNumbers=false]
 * @returns {ID_STRING}
 * @description Converts a given value to an ID.
 *              If failed `null` is returned.
 */
function valueToIDString(value, opt = {}){
  let result = null;
  let _options = typeof opt !== 'boolean' ? opt : { onlyNES: opt };
  if (!isPlainObject(_options)) _options = {};
  let {
    onlyNES,
    ignoreNumbers,
  } = _options;
  if (typeof onlyNES !== 'boolean') onlyNES = true;
  if (typeof ignoreNumbers !== 'boolean') ignoreNumbers = false;
  switch (typeof value) {
    case 'string': {
      let id = value.trim();
      if (id === '' && onlyNES) break;
      if (id !== '') {
        let num = Number(id);
        if (!Number.isNaN(num)) {
          if (ignoreNumbers) break;
          if (num < 0 || !isInteger(num)) break;
        };
      };
      result = id;
      break;
    }
    case 'number': {
      if (!ignoreNumbers) {
        let id = valueToIndex(value);
        if (id !== -1) result = id.toString();
      };
      break;
    }
    default: {}
  };
  return result;
};

/**
 * @function isValidId
 * @see valueToIDString
 * @returns {bool}
 * @experimental
 * @description Checks whether a given value can be converted to an ID.
 */
function isValidId(...args){
  return valueToIDString(...args) !== null;
};

/**
 * @function isNullOrUndef
 * @param {any}
 * @returns {bool}
 * @description Checks whether a given value is an `undefined` or a `null`.
 */
function isNullOrUndef(value = null){
  return value === null;
};

/**
 * @function tryToBool
 * @param {any}
 * @returns {?bool}
 * @description Tries to read a given value as a boolean type.
 *              If failed `null` is returned.
 */
function tryToBool(value){
  switch (typeof value) {
    case 'boolean': {
      return value;
      break;
    }
    case 'string': {
      const result = mpStrToBoolRefBase.get(value.trim());
      if (result !== undefined) return result;
      break;
    }
    default: {}
  };
  return null;
};

/**
 * @function readAsBool
 * @param {any}
 * @param {bool} [defValue=false]
 * @returns {bool}
 * @description Reads a given value and translates it to a boolean type.
 */
function readAsBool(value, defValue){
  let result = tryToBool(value);
  if (result === null) {
    result = typeof defValue === 'boolean' ? defValue : false;
  };
  return result;
};

/**
 * @function tryToNumber
 * @param {any}
 * @returns {?number}
 * @description Tries to read a given value as a number type.
 *              If failed `null` is returned.
 */
function tryToNumber(value){
  let result = value;
  switch (typeof value) {
    case 'string': {
      if ((result = result.trim()) === '') break;
      result = Number(result);
    }
    case 'number': {
      if (!Number.isNaN(result)) return result;
      break;
    }
    default: {}
  };
  return null;
};

/**
 * @function readAsNumber
 * @param {any}
 * @param {number} [defValue=0]
 * @returns {number}
 * @description Reads a given value and translates it to a number type.
 */
function readAsNumber(value, defValue){
  let result = tryToNumber(value);
  if (result === null) {
    result = (
      typeof defValue === 'number' && !Number.isNaN(defValue)
      ? defValue
      : 0
    );
  };
  return result;
};

/**
 * @function readAsString
 * @param {any}
 * @param {(bool|string|object)} [opt]
 * @param {bool} [opt.useTrim=false]
 * @param {string} [opt.defValue='']
 * @param {bool} [opt.numberToString=false]
 * @param {bool} [opt.boolToString=false]
 * @param {string} [opt.letterCase]
 * @param {bool} [opt.boolToUpperCase=false]
 * @returns {string}
 * @description Reads a given value and translates it to a string type.
 */
function readAsString(value, opt){
  let _options = opt;
  switch (typeof _options) {
    case 'boolean': {
      _options = { useTrim: _options };
      break;
    }
    case 'string': {
      _options = { defValue: _options };
      break;
    }
    default: {
      if (!isPlainObject(_options)) _options = {};
      break;
    }
  };
  let useUpperCase = false;
  let useLowerCase = false;
  let {
    useTrim,
    letterCase,
    numberToString,
    boolToString,
    boolToUpperCase,
    defValue,
  } = _options;
  if (typeof useTrim !== 'boolean') useTrim = false;
  if (typeof numberToString !== 'boolean') numberToString = false;
  if (typeof boolToString !== 'boolean') boolToString = false;
  if (typeof boolToUpperCase !== 'boolean') boolToUpperCase = false;
  switch (letterCase) {
    case 'upper': {
      useUpperCase = true;
      boolToUpperCase = true;
      break;
    }
    case 'lower': {
      useLowerCase = true;
      break;
    }
    default: {
      break;
    }
  };
  if (typeof defValue !== 'string') defValue = '';
  let result = '';
  if (numberToString && typeof value === 'number') {
    if (!Number.isNaN(value)) return value.toString();
  } else if (boolToString && typeof value === 'boolean') {
    result = value ? 'true' : 'false';
    return boolToUpperCase ? result.toUpperCase() : result;
  };
  result = typeof value === 'string' ? value : defValue;
  if (useTrim) result = result.trim();
  if (useUpperCase) result = result.toUpperCase();
  if (useLowerCase) result = result.toLowerCase();
  return result;
};

/**
 * @function readAsBoolEx
 * @param {any}
 * @param {bool} [defValue=false]
 * @returns {bool}
 * @description Reads a given value and translates it to a boolean type.
 */
function readAsBoolEx(value, defValue){
  let _value = value;
  if (_value === null) return false;
  switch (typeof value) {
    case 'boolean': {
      return _value;
    }
    case 'string': {
      _value = _value.trim();
      const result = mpStrToBoolRefEx.get(_value);
      if (result !== undefined) return result;
      _value = Number(_value);
    }
    case 'number': {
      if (!Number.isNaN(_value)) return _value !== 0;
    }
    default: {}
  };
  return typeof defValue === 'boolean' ? defValue : false;
};

/**
 * @function readAsNumberEx
 * @param {any}
 * @param {number} [defValue=0]
 * @returns {number}
 * @description Reads a given value and translates it to a number type.
 */
function readAsNumberEx(value, defValue){
  let _value = value;
  if (_value === null) return 0;
  switch (typeof _value) {
    case 'boolean': {
      return _value ? 1 : 0;
    }
    case 'string': {
      _value = _value.trim();
      const result = mpStrToBoolRefEx.get(_value);
      if (result !== undefined) return result ? 1 : 0;
      _value = Number(_value);
    }
    case 'number': {
      if (!Number.isNaN(_value)) return _value;
    }
    default: {}
  };
  return (
    typeof defValue === 'number' && !Number.isNaN(defValue)
    ? defValue
    : 0
  );
};

/**
 * @function tryNotNullish
 * @param {...any}
 * @returns {any}
 * @description Reads a list of a given arguments and returns
 *              a first one that value is not a `null` or `undefined`.
 */
function tryNotNullish(...args){
  let result = null;
  let value = undefined;
  for (value of args) {
    if (!isNullOrUndef(value)) {
      result = value;
      break;
    };
  };
  return result;
};

/**
 * @function inRangeNum
 * @param {number}
 * @param {number}
 * @param {number}
 * @returns {bool}
 * @description Checks whether a given value is into a defined range.
 */
function inRangeNum(minR, maxR, value){
  return value >= minR && value <= maxR;
};

/***
 * (* class definitions *)
 */

// === module exports block ===

module.exports.isInteger = isInteger;
module.exports.isValidIndex = isValidIndex;
module.exports.isEmptyString = isEmptyString;
module.exports.isEmptyOrNotString = isEmptyOrNotString;
module.exports.isNotEmptyString = isNotEmptyString;
module.exports.isNullOrUndef = isNullOrUndef;
module.exports.inRangeNum = inRangeNum;

module.exports.valueToIndex = valueToIndex;
module.exports.valueToIDString = valueToIDString;
module.exports.readAsBool = readAsBool;
module.exports.readAsBoolEx = readAsBoolEx;
module.exports.readAsNumber = readAsNumber;
module.exports.readAsNumberEx = readAsNumberEx;
module.exports.readAsString = readAsString;
module.exports.tryNotNullish = tryNotNullish;
module.exports.tryToBool = tryToBool;
module.exports.tryToNumber = tryToNumber;

// * experimental *
module.exports.isNotEmptyStringEx = isNotEmptyStringEx;
module.exports.isValidId = isValidId;
