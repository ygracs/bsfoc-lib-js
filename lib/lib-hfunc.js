// [v0.0.001-20240622]

// === module init block ===

// === module extra block (helper functions) ===

// === module main block ===

/***
 * (* constant definitions *)
 */

/**
 * a ref to the `Array.isArray()` method
 */
const isArray = Array.isArray;

/***
 * (* function definitions *)
 */

/**
 * @function isPlainObject
 * @param {any} [obj=null]
 * @returns {bool}
 * @description Checks if the given value is a plain object
 */
function isPlainObject(obj = null){
  // // TODO: consider if an instance of "Set" can be treated as plain object
  return obj !== null && typeof obj === 'object' && !isArray(obj);
};

/***
 * (* class definitions *)
 */

// === module exports block ===

module.exports.isArray = isArray;
module.exports.isPlainObject = isPlainObject;
