// [v0.1.077-20240623]

// === module init block ===

const {
  readAsBool, readAsNumber, readAsString,
  isNullOrUndef, isNotEmptyString,
} = require('./baseFunc.js');

const {
  isArray, isPlainObject,
} = require('./lib-hfunc');


// === module extra block (helper functions) ===

// === module main block ===

/***
 * (* constant definitions *)
 */

/**
 * a ref to `Array.from()` method
 */
const arrayFrom = Array.from;

/***
 * (* function definitions *)
 */

/**
 * @function isObject
 * @param {any}
 * @returns {bool}
 * @description Checks whether a given value is an object.
 */
function isObject(obj){
  return obj !== null && typeof obj === 'object';
};

/**
 * @function isDateObject
 * @param {any}
 * @returns {bool}
 * @description Checks whether a given value is an `DateTime`-object.
 */
function isDateObject(obj){
  return Object.prototype.toString.call(obj) === '[object Date]';
};

/**
 * @function valueToArray
 * @param {any}
 * @returns {array}
 * @description Contents a given value to an array.
 */
function valueToArray(value){
  if (isNullOrUndef(value)) return [];
  if (isArray(value)) return value;
  if (
    value instanceof Set || value instanceof Map
  ) {
    return arrayFrom(value);
  };
  return [ value ];
}

/**
 * @function valueToArrayEx
 * @param {any}
 * @param {func} cb - some callback function
 * @returns {array}
 * @experimental
 * @description Contents a given value to an array.
 */
function valueToArrayEx(value = [], cb){
  const mapFn = typeof cb === 'function' ? cb : undefined;
  if (
    value instanceof Set || value instanceof Map
  ) {
    return arrayFrom(value, mapFn);
  };
  return arrayFrom(isArray(value)? value : [ value ], mapFn);
}

/**
 * @function valueToEntry
 * @param {any}
 * @returns {?array}
 * @description Contents a given value to an entry (aka `Key-Value`-pair).
 *              If failed `null` is returned.
 */
function valueToEntry(value = null) {
  if (value === null) return null;
  let [ key, _value ] = isArray(value) ? value : [ '', value ];
  if (isNullOrUndef(key)) return null;
  if (typeof key === 'string') key = key.trim();
  return [ key, _value ];
};

/**
 * @function valueToEntries
 * @param {any}
 * @param {object} [opt]
 * @returns {array}
 * @description Contents a given value to an array of entries.
 */
function valueToEntries(value, opt){
  let _options = isPlainObject(opt) ? opt : {};
  let result = [];
  valueToArray(value).forEach((item) => {
    const entry = valueToEntry(item);
    if (entry !== null) {
      let [ key, value ] = entry;
      if (typeof key === 'string') {
        value = readAsString(value, _options);
        if (value !== '') result.push([ key, value ]);
      };
    };
  });
  return result;
};

function readAsListE(opt, ...args){
  const __retItem = function(item){
    return isPlainObject(item) ? item : null;
  };
  const __chkItem = function(item, opt){
    return readAsBool(opt, false) ? true : (item ? true : false);
  };
  let _options = typeof opt !== 'boolean' ? opt : { keepEmpty: opt };
  if (!isPlainObject(_options)) _options = {};
  let {
    keepEmpty,
    unfoldArray,
    userFn,
    userOptions,
  } = _options;
  if (typeof keepEmpty !== 'boolean') keepEmpty = false;
  if (typeof unfoldArray !== 'boolean') unfoldArray = false;
  if (!isPlainObject(userFn)) userFn = {};
  let { checkItemFn, readItemFn } = userFn;
  if (typeof readItemFn !== 'function') readItemFn = __retItem;
  if (typeof checkItemFn !== 'function') checkItemFn = __chkItem;
  const __readElements = (
    arr, result, readFn, checkFn, keepEmpty, unfoldArray
  ) => {
    for (let value of arr) {
      if (isArray(value) && unfoldArray) {
        __readElements(value, result, readFn, checkFn, keepEmpty, false);
      } else if ((value instanceof Set) && unfoldArray) {
        __readElements([...value], result, readFn, checkFn, keepEmpty, false);
      } else {
        value = readFn(value);
        if (value !== undefined && checkFn(value, keepEmpty)) {
          result.push(value);
        };
      };
    };
    return result;
  };
  return __readElements(
    [ ...args ], [], readItemFn, checkItemFn, keepEmpty, unfoldArray,
  );
};

function readAsListS(opt, ...args){
  let _options = opt;
  switch (typeof _options) {
    case 'boolean': {
      _options = { useTrim: _options };
      break;
    }
    case 'string': {
      _options = { defValue: _options };
      break;
    }
    default: {
      if (!isPlainObject(_options)) _options = {};
      break;
    }
  };
  let {
    keepEmpty,
    unfoldArray,
    unfoldLevel,
  } = _options;
  if (typeof keepEmpty !== 'boolean') keepEmpty = false;
  if (typeof unfoldArray !== 'boolean') unfoldArray = false;
  if (unfoldArray) {
    unfoldLevel = readAsNumber(unfoldLevel, 1);
    if (unfoldLevel < -1) unfoldLevel = 0;
  } else {
    unfoldLevel = 0;
  };
  const __readElements = (
    arr, result, opt, keepEmpty, unfoldLevel,
  ) => {
    const unfoldArray = unfoldLevel > 0 || unfoldLevel === -1;
    for (let value of arr) {
      if (unfoldArray && isArray(value)) {
        if (unfoldLevel > 0) unfoldLevel--;
        __readElements(value, result, opt, keepEmpty, unfoldLevel);
      } else {
        value = readAsString(value, opt);
        if (keepEmpty || isNotEmptyString(value)) result.push(value);
      };
    };
    return result;
  };
  return __readElements([ ...args ], [], _options, keepEmpty, unfoldLevel);
};

/**
 * @function isPropertyExists
 * @param {object}
 * @param {string} - a property name
 * @returns {bool}
 * @description Checks whether an object has a target property.
 */
function isPropertyExists(obj, name) {
  let result = false;
  let key = '';
  if (
    isObject(obj)
    // // TODO: consider if an array is acceptable as "obj"
    && typeof name === 'string'
    && ((key = name.trim()) !== '')
  ) {
    switch (typeof obj[key]) {
      case 'undefined':
      case 'function': {
        result = false;
        break;
      }
      default: {
        result = true;
        break;
      }
    };
  };
  return result;
};

/**
 * @function isMethodExists
 * @param {object}
 * @param {string} - a method name
 * @returns {bool}
 * @description Checks whether an object has a target method.
 */
function isMethodExists(obj, name){
  let result = false;
  let key = '';
  if (
    isObject(obj)
    // // TODO: consider if an array is acceptable as "obj"
    && typeof name === 'string'
    && ((key = name.trim()) !== '')
  ) {
    result = typeof obj[key] === 'function';
  };
  return result;
};

/**
 * @function doMethodIfExists
 * @param {object}
 * @param {string} - a method name
 * @param {...any} - a method arguments
 * @returns {any}
 * @experimental
 * @description Perfoms a target method of an object if it's exists.
 */
function doMethodIfExists(obj, name, ...args) {
  if (isMethodExists(obj, name)) return obj[name.trim()](...args);
};

/***
 * (* class definitions *)
 */

// === module exports block ===

module.exports.isArray = isArray;
module.exports.isObject = isObject;
module.exports.isPlainObject = isPlainObject;
module.exports.isDateObject = isDateObject;
module.exports.isPropertyExists = isPropertyExists;
module.exports.isMethodExists = isMethodExists;

module.exports.arrayFrom = arrayFrom;
module.exports.valueToArray = valueToArray;
module.exports.valueToEntry = valueToEntry;
module.exports.valueToEntries = valueToEntries;
module.exports.readAsListE = readAsListE;
module.exports.readAsListS = readAsListS;

// * experimental *
module.exports.doMethodIfExists = doMethodIfExists;
module.exports.valueToArrayEx = valueToArrayEx;
