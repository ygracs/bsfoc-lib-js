// [v0.1.069-20220830]

// === module init block ===

const {
  TItemsList,
  TItemsListEx,
} = require('./baseClass/lists.js');

const {
  TNamedItemsCollection,
  TNamedSetsCollection,
  TItemsICollection,
} = require('./baseClass/collections.js');

// === module extra block (helper functions) ===

// === module main block (function definitions) ===

// === module main block (class definitions) ===

// === module exports block ===

exports.TItemsList = TItemsList;
exports.TItemsListEx = TItemsListEx;

exports.TNamedItemsCollection = TNamedItemsCollection;
exports.TNamedSetsCollection = TNamedSetsCollection;
exports.TItemsICollection = TItemsICollection;
