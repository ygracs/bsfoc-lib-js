// [v0.1.080-20240623]

// === module init block ===

const {
  valueToIndex,
} = require('../baseFunc.js');

const {
  isPlainObject,
  valueToArray,
} = require('../baseObj.js');

// === module extra block (helper functions) ===

// === module main block ===

/***
 * (* constant definitions *)
 */

/***
 * (* function definitions *)
 */

/***
 * (* class definitions *)
 */

/**
 * @description This class implements an instance of the items list
 */
class TItemsList {
  #_items = null;
  #_curIndex = -1;

  constructor(){
    this.#_items = [];
    this.#_curIndex = -1;
  }

  /**
   * @property {number}
   * @readonly
   */
  get count(){
    return this.#_items.length;
  }

  /**
   * @property {index}
   */
  get curIndex(){
    return this.#_curIndex;
  }

  set curIndex(index){
    this.setIndex(index);
  }

  /**
   * @property {any}
   * @readonly
   */
  get curItem(){
    return this.getItem(this.curIndex);
  }

  set curItem(item){
    this.setItem(this.curIndex, item);
  }

  /**
   * @property {index}
   * @readonly
   */
  get minIndex(){
    return this.count !== 0 ? 0 : -1;
  }

  /**
   * @property {index}
   * @readonly
   */
  get maxIndex(){
    return this.count - 1;
  }

  /**
   * @returns {none}
   * @description Deletes all items from the list.
   */
  clear(){
    this.#_items = [];
    this.#_curIndex = -1;
  }

  /**
   * @returns {bool}
   * @description Indicates whether a list is empty.
   */
  isEmpty(){
    return this.count === 0;
  }

  /**
   * @returns {bool}
   * @description Indicates whether a list is not empty.
   */
  isNotEmpty(){
    return this.count > 0;
  }

  /**
   * @param {any}
   * @returns {bool}
   * @description Checks whether a given value is a valid index value and
   *              it is not exceeds an index range within the list.
   */
  chkIndex(value){
    const index = valueToIndex(value);
    return index !== -1 && index < this.count;
  }

  /**
   * @param {index}
   * @returns {bool}
   * @description Sets a current list index.
   */
  setIndex(value){
    if (!this.chkIndex(value)) return false;
    this.#_curIndex = Number(value);
    return true;
  }

  /**
   * @returns {none}
   * @description Resets a current list index.
   */
  rstIndex(){
    this.#_curIndex = -1;
  }

  /**
   * @param {any}
   * @param {index} [index=0] - a position to start
   * @returns {index}
   * @description Searchs an index of a given item in the list and
   *              returns its index.
   */
  srchIndex(item, index = 0){
    const sPos = valueToIndex(index);
    return this.#_items.indexOf(item, sPos !== -1 ? sPos : 0);
  }

  /**
   * @param {index}
   * @returns {any}
   * @description Returns an item by a given index.
   */
  getItem(index){
    if (this.chkIndex(index)) return this.#_items[Number(index)];
  }

  /**
   * @param {any}
   * @param {index}
   * @returns {bool}
   * @description Places an item into a position addressed by a given index.
   */
  setItem(index, item){
    if (item === undefined || !this.chkIndex(index)) return false;
    this.#_items[Number(index)] = item;
    return true;
  }

  /**
   * @param {any}
   * @returns {bool}
   * @description Adds an item to a list.
   */
  addItem(item){
    if (item === undefined) return false;
    this.#_items.push(item);
    return true;
  }

  /**
   * @param {any}
   * @param {bool} [opt=false]
   * @returns {index}
   * @description Adds an item to a list.
   */
  addItemEx(item, opt = false){
    let index = -1;
    if (item !== undefined) {
      const forceCI = typeof opt === 'boolean' ? opt : false;
      index = this.count;
      this.#_items.push(item);
      if (forceCI) this.#_curIndex = index;
    };
    return index;
  }

  /**
   * @returns {bool}
   * @see TItemsList.delItemEx
   * @description Deletes an item from a list.
   */
  delItem(...args){
    return this.delItemEx(...args) !== undefined;
  }

  /**
   * @param {index}
   * @param {bool} [opt=true]
   * @returns {any}
   * @description Deletes an item from a list.
   */
  delItemEx(index, opt = true){
    if (!this.chkIndex(index)) return;
    const cPos = Number(index);
    const item = this.#_items.splice(cPos, 1)[0];
    let current = this.curIndex;
    if (current !== -1) {
      const forceCI = typeof opt === 'boolean' ? opt : true;
      const count = this.count;
      if (forceCI) {
          if (cPos < current) {
            this.#_curIndex = --current;
          } else {
            if (current === count) this.#_curIndex = --current;
          };
      } else {
        if (current === count) this.#_curIndex = -1;
      };
    };
    return item;
  }

  /**
   * @param {array}
   * @param {(bool|object)} [opt]
   * @returns {number}
   * @description Adds an items to a list.
   */
  loadItems(list, opt){
    const items = valueToArray(list);
    let result = 0;
    if (items.length > 0) {
      let _options = opt;
      if (typeof _options === 'boolean') _options = { useClear: _options };
      if (!isPlainObject(_options)) _options = {};
      let {
        useClear,
        resetIndex,
        curIndex,
      } = _options;
      if (typeof useClear !== 'boolean') useClear = true;
      if (typeof resetIndex !== 'boolean') resetIndex = false;
      if (useClear) {
        this.clear();
        resetIndex = false;
      };
      const count = this.count;
      items.forEach((item) => { this.addItem(item); });
      result = this.count - count;
      if (!this.setIndex(curIndex) && resetIndex) this.rstIndex();
    };
    return result;
  }

  /**
   * @since 0.2.1
   * @param {func} - a callback function
   * @returns {none}
   * @description Calls a given function for each list element
   */
  forEach(cb) {
    if (typeof cb === 'function') this.#_items.forEach(cb);
  }

};

/**
 * @augments TItemsList
 * @description This class enhanced a capabilities implemented
 *              in the `TItemsList` class
 */
class TItemsListEx extends TItemsList {
  #_lastSrchIndex = -1;

  constructor(){
    super();
    this.#_lastSrchIndex = -1;
  }

  /**
   * @property {index}
   * @readonly
   */
  get prevIndex(){
    let result = super.curIndex;
    if (result > 0) result--;
    return result;
  }

  /**
   * @property {index}
   * @readonly
   */
  get nextIndex(){
    let result = this.maxIndex;
    let maxIndex = result;
    if (result !== -1) {
      result = super.curIndex;
      if (result !== -1 && result < maxIndex) result++;
    };
    return result;
  }

  /**
   * @returns {none}
   * @description Deletes all items from the list.
   */
  clear(){
    super.clear();
    this.#_lastSrchIndex = -1;
  }

  /**
   * @param {any}
   * @param {bool} [opt=false] - defines a type of result
   * @returns {(bool|index)}
   * @description Checks whether a given value is a valid index value and
   *              it is not exceeds an index range within the list.
   *              The type of result returned by a method defined by
   *              an `opt` parameter.
   */
  chkIndexEx(value, opt = false){
    const index = valueToIndex(value);
    const result = index !== -1 && index < super.count;
    if (typeof opt === 'boolean' ? opt : false) {
      return result ? index : -1;
    } else {
      return result;
    };
  }

  /**
   * @see TItemsList.srchIndex
   */
  srchIndex(...args){
    const index = super.srchIndex(...args);
    this.#_lastSrchIndex = index;
    return index;
  }

  /**
   * @param {any}
   * @returns {index}
   * @description Searchs an index of a given item in the list and
   *              returns its index. The search starts from a last
   *              position of a previous call.
   */
  srchNextIndex(item){
    let index = this.#_lastSrchIndex;
    if (index !== -1) {
      if (index < this.maxIndex) {
        index = this.srchIndex(item, index + 1);
      } else {
        index = -1;
      }
      this.#_lastSrchIndex = index;
    };
    return index;
  }

};

// === module exports block ===

module.exports.TItemsList = TItemsList;
module.exports.TItemsListEx = TItemsListEx;
