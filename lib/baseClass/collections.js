// [v0.1.076-20240623]

// === module init block ===

const {
  isNotEmptyString,
  readAsString,
} = require('../baseFunc.js');

const {
  isPlainObject,
  readAsListE, arrayFrom, valueToArray,
} = require('../baseObj.js');

// === module extra block (helper functions) ===

// === module main block (function definitions) ===

// === module main block (class definitions) ===

/***
 * (* constant definitions *)
 */

const ITEMSCOLL_DEF_CAT_NAME = 'ALL';

/***
 * (* function definitions *)
 */

/***
 * (* class definitions *)
 */

/**
 * @description This class implements an instance of the items collection
 */
class TNamedItemsCollection {
  #_items = null;
  #_options = null;
  #_manip = null;

  /**
   * @param {object} [opt]
   * @param {bool} [opt.forceIfKeyExists=false]
   * @param {object} [opt.userFn]
   */
  constructor(opt){
    // define default functions for manipulators
    const __user_index_getter = (key) => {
      return key !== undefined ? key : null;
    };
    // load options
    let _options = isPlainObject(opt) ? opt : {};
    let {
      forceIfKeyExists,
      userFn,
    } = _options;
    if (typeof forceIfKeyExists !== 'boolean') forceIfKeyExists = false;
    // init manipulators
    if (!isPlainObject(userFn)) userFn = {};
    let { checkIndexFn, readIndexFn } = userFn;
    if (typeof checkIndexFn !== 'function') {
      checkIndexFn = TNamedItemsCollection.isValidKey;
    };
    if (typeof readIndexFn !== 'function') readIndexFn = __user_index_getter;
    // save options
    _options.forceIfKeyExists = forceIfKeyExists;
    this.#_options = _options;
    // store manipulators
    this.#_manip = {
      __isValidIndex: checkIndexFn,
      __readIndexName: readIndexFn,
    };
    // init items collection
    this.#_items = new Map();
  }

  /**
   * @property {array}
   * @readonly
   */
  get keys(){
    return arrayFrom(this.#_items.keys());
  }

  /**
   * @property {array}
   * @readonly
   */
  get values(){
    return arrayFrom(this.#_items.values());
  }

  /**
   * @property {array}
   * @readonly
   */
  get entries(){
    return arrayFrom(this.#_items.entries());
  }

  /**
   * @property {number}
   * @readonly
   */
  get size(){
    return this.#_items.size;
  }

  /**
   * @param {any}
   * @returns {bool}
   * @description Checks whether a key.
   */
  has(key){
    return this.#_items.has(this.#_manip.__readIndexName(key));
  }

  /**
   * @param {any}
   * @returns {?any}
   * @description Returns an element addressed by a given key.
   */
  get(key){
    const item = this.#_items.get(this.#_manip.__readIndexName(key));
    return item !== undefined ? item : null;
  }

  /**
   * @param {any}
   * @returns {bool}
   * @description Adds a key to a collection.
   */
  add(key){
    const _key = this.#_manip.__readIndexName(key);
    const { forceIfKeyExists } = this.#_options;
    let isSUCCEED = (
      this.isValidKey(_key)
      && (!this.#_items.has(_key) || forceIfKeyExists)
    );
    if (isSUCCEED) this.#_items.set(_key, null);
    return isSUCCEED;
  }

  /**
   * @param {any}
   * @param {any}
   * @returns {bool}
   * @description Sets a value for the element addressed by a given key.
   */
  set(key, value){
    const _key = this.#_manip.__readIndexName(key);
    let isSUCCEED = this.#_items.has(_key) && value !== undefined;
    if (isSUCCEED) this.#_items.set(_key, value);
    return isSUCCEED;
  }

  /**
   * @param {any}
   * @param {any}
   * @returns {bool}
   * @description Sets a value for the element addressed by a given key.
   *              If key is not exists, it will be added.
   */
  insert(key, value){
    const _key = this.#_manip.__readIndexName(key);
    let isSUCCEED = this.isValidKey(_key) && value !== undefined;
    if (isSUCCEED) this.#_items.set(_key, value);
    return isSUCCEED;
  }

  /**
   * @param {any}
   * @returns {any}
   * @description Deletes an element addressed by a given key.
   */
  delete(key){
    return this.#_items.delete(this.#_manip.__readIndexName(key));
  }

  /**
   * @param {any}
   * @param {any}
   * @returns {bool}
   * @description Renames a given key with a new one.
   */
  rename(oldKey, newKey){
    const _oldKey = this.#_manip.__readIndexName(oldKey);
    const _newKey = this.#_manip.__readIndexName(newKey);
    const _items = this.#_items;
    let isSUCCEED = (
      this.isValidKey(_newKey)
      && _items.has(_oldKey)
    );
    if (isSUCCEED && _oldKey !== _newKey) {
      isSUCCEED = (
        (!_items.has(_newKey) || this.#_options.forceIfKeyExists)
      );
      if (isSUCCEED) {
        const value = _items.get(_oldKey);
        _items.delete(_oldKey);
        _items.set(_newKey, value);
      };
    };
    return isSUCCEED;
  }

  /**
   * @returns {none}
   * @description Removes all elements from a collection
   */
  clear(){
    this.#_items.clear();
  }

  /**
   * @param {any}
   * @returns {bool}
   * @static
   * @description Performs a base check to verify whether
   *              a given key value is valid or not.
   */
  static isValidKey(value){
    return value !== undefined && value !== null;
  }

  /**
   * @property {func} - returns a function for a key validation
   * @readonly
   */
  get isValidKey(){
    return (
      typeof this.#_manip.__isValidIndex === 'function'
      ? this.#_manip.__isValidIndex
      : TNamedItemsCollection.isValidKey
    );
  }

};

/**
 * @augments TNamedItemsCollection
 * @description This class implements an instance of the sets collection
 */
class TNamedSetsCollection extends TNamedItemsCollection {
  #_options = null;
  #_manip = null;

  /**
   * @param {object} [opt]
   * @param {bool} [opt.forceIfKeyExists=false]
   * @param {bool} [opt.forceIfItemExists=false]
   * @param {object} [opt.userFn]
   */
  constructor(opt){
    // define default functions for manipulators
    const __user_index_getter = (key) => {
      return key !== undefined ? key : null;
    };
    const __user_item_getter = (item) => {
      return item !== undefined ? item : null;
    };
    // load options
    let _options = isPlainObject(opt) ? opt : {};
    let {
      forceIfKeyExists,
      forceIfItemExists,
      userFn,
    } = _options;
    if (typeof forceIfKeyExists !== 'boolean') forceIfKeyExists = false;
    if (typeof forceIfItemExists !== 'boolean') forceIfItemExists = false;
    // init manipulators
    if (!isPlainObject(userFn)) userFn = {};
    let {
      checkIndexFn,
      readIndexFn,
      checkItemFn,
      readItemFn,
    } = userFn;
    if (typeof checkIndexFn !== 'function') {
      checkIndexFn = TNamedSetsCollection.isValidKey;
    };
    if (typeof readIndexFn !== 'function') readIndexFn = __user_index_getter;
    if (typeof checkItemFn !== 'function') {
      checkItemFn = TNamedSetsCollection.isValidItem;
    };
    if (typeof readItemFn !== 'function') readItemFn = __user_item_getter;
    // call parent constructor
    super({
      forceIfKeyExists: forceIfKeyExists,
      userFn: {
        checkIndexFn,
        readIndexFn,
      },
    });
    // save options
    _options.forceIfKeyExists = forceIfKeyExists;
    _options.forceIfItemExists = forceIfItemExists;
    this.#_options = _options;
    // save manipulators
    this.#_manip = {
      __isValidIndex: checkIndexFn,
      __readIndexName: readIndexFn,
      __isValidItem: checkItemFn,
      __readItem: readItemFn,
    };
  }

  __createNewSet(...args){
    return new Set(
      readAsListE({
        //keep_empty: false,
        userFn: {
          readItemFn: this.#_manip.__readItem,
          checkItemFn: this.#_manip.__isValidItem,
        },
      }, ...args)
    );
  }

  /**
   * @param {any}
   * @returns {?Set}
   * @description Returns a set addressed by a given key.
   */
  get(key){
    const item = super.get(key);
    return item instanceof Set ? item : null;
  }

  /**
   * @param {any}
   * @returns {bool}
   * @description Adds a key to a collection.
   */
  add(key){
    let isSUCCEED = super.add(key);
    if (isSUCCEED) isSUCCEED = super.set(key, new Set());
    return isSUCCEED;
  }

  /**
   * @param {any}
   * @param {any}
   * @returns {bool}
   * @description Adds a value to a set addressed by a given key.
   */
  addTo(key, item){
    let isSUCCEED = super.has(key) && this.isValidItem(item);
    if (isSUCCEED) {
      let obj = super.get(key);
      if (!(obj instanceof Set)) {
        obj = new Set();
        isSUCCEED = super.set(key, obj);
      };
      if (isSUCCEED) obj.add(item);
    };
    return isSUCCEED;
  }

  /**
   * @param {any}
   * @param {any}
   * @returns {bool}
   * @description Sets a value for the set addressed by a given key.
   */
  set(key, value){
    return super.set(key, this.__createNewSet(...valueToArray(value)));
  }

  /**
   * @param {any}
   * @param {any}
   * @returns {bool}
   * @description Sets a value for the set addressed by a given key.
   *              If key is not exists, it will be added.
   */
  insert(key, value){
    return super.insert(key, this.__createNewSet(...valueToArray(value)));
  }

  /**
   * @param {any}
   * @param {any}
   * @returns {bool}
   * @description Deletes a value from a set addressed by a given key.
   */
  deleteFrom(key, item){
    let obj = this.get(key);
    return (
      obj instanceof Set && this.isValidItem(item)
      ? obj.delete(item)
      : false
    );
  }

  /**
   * @param {any}
   * @param {...any}
   * @returns {bool}
   * @description Merges all listed keys into a key given by a first parameter.
   */
  merge(key, ...extras){
    let isSUCCEED = this.isValidKey(key);
    if (isSUCCEED) {
      let keys = arrayFrom(new Set([ key, ...extras ]));
      let result = [];
      let item = null;
      for (let name of keys) {
        item = this.get(name);
        if (item instanceof Set) {
          result.push(...arrayFrom(item));
          super.delete(name);
        };
      };
      isSUCCEED = super.insert(key, new Set(result));
    };
    return isSUCCEED;
  }

  /**
   * @param {any}
   * @param {any} - old element
   * @param {any} - new element
   * @returns {bool}
   * @description Replaces an element in a set addressed by a given key.
   */
  replace(key, oldItem, newItem){
    let obj = this.get(key);
    let isSUCCEED = false;
    if (obj) {
      isSUCCEED = obj.has(oldItem) && this.isValidItem(newItem);
      if (isSUCCEED && oldItem !== newItem) {
        obj.delete(oldItem);
        obj.add(newItem);
      };
    };
    return isSUCCEED;
  }

  /**
   * @returns {none}
   * @description Removes all listed keys from a collection
   */
  clear(...keys){
    if (keys.length > 0) {
      for (let key of keys) {
        let item = this.get(key);
        if (item instanceof Set) item.clear();
      };
    } else {
      super.clear();
    };
  }

  /**
   * @param {any}
   * @returns {bool}
   * @static
   * @description Performs a base check to verify whether
   *              a given key value is valid or not.
   */
  static isValidKey(value){
    return value !== undefined && value !== null;
  }

  /**
   * @property {func} - returns a function for a key validation
   * @readonly
   */
  get isValidKey(){
    return (
      typeof this.#_manip.__isValidIndex === 'function'
      ? this.#_manip.__isValidIndex
      : TNamedSetsCollection.isValidKey
    );
  }

  /**
   * @param {any}
   * @returns {bool}
   * @static
   * @description Performs a base check to verify whether
   *              a given item value is valid or not.
   */
  static isValidItem(value){
    return value !== undefined && value !== null;
  }

  /**
   * @property {func} - returns a function for an item validation
   * @readonly
   */
  get isValidItem(){
    return (
      typeof this.#_manip.__isValidItem === 'function'
      ? this.#_manip.__isValidItem
      : TNamedSetsCollection.isValidItem
    );
  }

}

/**
 * @description This class implements an instance of an indexed collection
 *              of an items
 */
class TItemsICollection {
  #_items = null;
  #_cats = null;
  #_index = null;
  #_manip = null;

  /**
   * @param {object} [opt]
   * @param {bool} [opt.forceIfKeyExists=false]
   * @param {bool} [opt.forceIfItemExists=false]
   * @param {object} [opt.userFn]
   */
  constructor(opt){
    // define default functions for manipulators
    const __user_index_getter = (value) => {
      return readAsString(value, { useTrim: true, numberToString: true });
    };
    const __user_item_getter = (value) => {
      return isPlainObject(value) ? value : null;
    };
    // load options
    let _options = isPlainObject(opt) ? opt : {};
    let {
      forceIfKeyExists,
      forceIfItemExists,
      userFn,
    } = _options;
    if (typeof forceIfKeyExists !== 'boolean') forceIfKeyExists = false;
    if (typeof forceIfItemExists !== 'boolean') forceIfItemExists = false;
    // init manipulators
    if (!isPlainObject(userFn)) userFn = {};
    let {
      checkIndexFn,
      readIndexFn,
      checkItemFn,
      readItemFn,
    } = userFn;
    if (typeof checkIndexFn !== 'function') checkIndexFn = isNotEmptyString;
    if (typeof readIndexFn !== 'function') readIndexFn = __user_index_getter;
    if (typeof checkItemFn !== 'function') checkItemFn = isPlainObject;
    if (typeof readItemFn !== 'function') readItemFn = __user_item_getter;
    // save options
    // // TODO: [?] consider to save options
    // save manipulators
    this.#_manip = {
      __isValidIndex: checkIndexFn,
      __readIndexName: readIndexFn,
      __isValidItem: checkItemFn,
      __readItem: readItemFn,
    };
    // init items collection
    this.#_items = new TNamedItemsCollection({
      forceIfKeyExists: forceIfKeyExists,
      userFn: {
        checkIndexFn,
        readIndexFn,
      },
    });
    // init categories collection
    this.#_cats = new TNamedSetsCollection({
      forceIfKeyExists: forceIfKeyExists,
      forceIfItemExists: forceIfItemExists,
      userFn: {
        checkIndexFn,
        readIndexFn,
        checkItemFn,
        readItemFn,
      },
    });
    // init collection index set
    this.#_index = new TNamedSetsCollection({
      // // TODO: [?] forceIfKeyExists: _options.forceIfKeyExists,
      // // TODO: [?] forceIfItemExists: _options.forceIfItemExists,
      userFn: {
        checkItemFn,
        readIndexFn,
        checkItemFn: readIndexFn,
        readItemFn: readIndexFn,
      },
    });
  }

  /**
   * @property {array}
   * @readonly
   */
  get ItemNames(){
    return this.#_items.keys;
  }

  /**
   * @property {array}
   * @readonly
   */
  get CategoryNames(){
    return this.#_cats.keys;
  }

  /**
   * @returns {none}
   * @description Removes all elements from a collection
   */
  clear(){
    this.#_index.clear();
    this.#_cats.clear();
    this.#_items.clear();
  }

  /**
   * @param {any}
   * @param {any}
   * @returns {bool}
   * @description .
   */
  hasItem(name, cat){
    const _name = this.#_manip.__readIndexName(name);
    let isSUCCEED = this.#_items.has(_name);
    if (isSUCCEED && cat !== undefined) {
      // process <cat> as a targeted category, if present
      const _cat = this.#_manip.__readIndexName(cat);
      let obj = this.#_index.get(_name);
      isSUCCEED = (obj instanceof Set) && obj.has(_cat);
    };
    return isSUCCEED;
  }

  /**
   * @param {any}
   * @returns {any}
   * @description Returns an element addressed by a given key.
   */
  getItem(name){
    const _name = this.#_manip.__readIndexName(name);
    const item = this.#_items.get(_name);
    return this.#_manip.__isValidItem(item) ? item : null;
  }

  /**
   * @param {any}
   * @returns {array}
   * @description Returns a list of a categories into which an element
   *              addressed by a given key is included.
   */
  getItemCategories(name){
    const _name = this.#_manip.__readIndexName(name);
    let obj = this.#_index.get(_name);
    return (obj instanceof Set) ? [ ...obj.values() ] : [];
  }

  /**
   * @param {any}
   * @param {any}
   * @param {...any} [cats] - list of a categories
   * @returns {bool}
   * @description Adds an element under a given key.
   */
  addItem(name, item, ...cats){
    const _name = this.#_manip.__readIndexName(name);
    let isSUCCEED = false;
    if (
      this.#_manip.__isValidIndex(_name)
      &&  this.#_manip.__isValidItem(item)
    ) {
      if (this.#_items.has(_name)) {
        isSUCCEED = this.#_items.set(_name, item);
      } else {
        // TODO: check if item is not the same
        isSUCCEED = this.#_items.insert(_name, item);
      };
      // process <cats> (add item to groups list)
      if (isSUCCEED && cats.length > 0) this.addItemToCategory(_name, ...cats);
    };
    return isSUCCEED;
  }

  /**
   * @param {any}
   * @param {...any} [cats] - list of a categories
   * @returns {bool}
   * @description Adds an element with a given key to a listed categories.
   */
  addItemToCategory(name, ...cats){
    const _name = this.#_manip.__readIndexName(name);
    let item = this.#_items.get(_name);
    let isSUCCEED = false;
    if (this.#_manip.__isValidItem(item)) {
      let list = readAsListE({
        //keep_empty: false,
        userFn: {
          readItemFn: this.#_manip.__readIndexName,
          checkItemFn: this.#_manip.__isValidIndex,
        },
      }, ...cats);
      let count = 0;
      for (let cat of list) {
        if (this.#_cats.addTo(cat, item)) {
          if (this.#_index.has(_name)) {
            this.#_index.addTo(_name, cat);
          } else {
            this.#_index.insert(_name, cat);
          };
          count++;
        };
      };
      isSUCCEED = count > 0;
    };
    return isSUCCEED;
  }

  /**
   * @param {any}
   * @returns {bool}
   * @description Deletes an element addressed by a given key.
   */
  delItem(name){
    const _name = this.#_manip.__readIndexName(name);
    let isSUCCEED = this.#_items.has(_name);
    if (isSUCCEED) {
      let cats = valueToArray(this.#_index.get(_name));
      if (cats.length > 0) this.delItemFromCategory(_name, ...cats);
      isSUCCEED = this.#_items.delete(_name);
    };
    return isSUCCEED;
  }

  /**
   * @param {any}
   * @param {...any} [cats] - list of a categories
   * @returns {bool}
   * @description Deletes an element addressed by a given key
   *              from a listed categories.
   */
  delItemFromCategory(name, ...cats){
    const _name = this.#_manip.__readIndexName(name);
    let item = this.#_items.get(_name);
    let isSUCCEED = false;
    if (this.#_manip.__isValidItem(item)) {
      let list = readAsListE({
        //keep_empty: false,
        userFn: {
          readItemFn: this.#_manip.__readIndexName,
          checkItemFn: this.#_manip.__isValidIndex,
        },
      }, ...cats);
      if (list.length > 0) {
        let count = 0; let cat_items = null;
        let index = this.#_index.get(_name);
        let do_index = index instanceof Set;
        for (let cat of list) {
          if (do_index && index.delete(cat)) {
            cat_items = this.#_cats.get(cat);
            if ((cat_items instanceof Set)) {
              if (cat_items.delete(item)) count++;
            };
          } else {
            // duplicate ops in case the index is broken
            cat_items = this.#_cats.get(cat);
            if ((cat_items instanceof Set)) {
              if (cat_items.delete(item)) count++;
            };
          };
        };
        isSUCCEED = count > 0;
      };
    };
    return isSUCCEED;
  }

  /**
   * @param {any} - old key
   * @param {any} - new key
   * @returns {bool}
   * @description Renames a given key with a new one.
   */
  renameItem(oldName, newName){
    const _oldName = this.#_manip.__readIndexName(oldName);
    const _newName = this.#_manip.__readIndexName(newName);
    let isSUCCEED = (
      this.#_items.has(_oldName)
      && this.#_manip.__isValidIndex(_newName)
    );
    if (isSUCCEED && _oldName !== _newName) {
      isSUCCEED = this.#_items.rename(_oldName, _newName);
      if (isSUCCEED && this.#_index.has(_oldName)) {
        isSUCCEED = this.#_index.rename(_oldName, _newName);
      };
    };
    return isSUCCEED;
  }

  /**
   * @param {any}
   * @param {any}
   * @returns {bool}
   * @description Replaces an element addressed by a given key with a new one.
   */
  replaceItem(name, newItem){
    const _name = this.#_manip.__readIndexName(name);
    let isSUCCEED = (
      this.#_items.has(_name)
      && this.#_manip.__isValidItem(newItem)
    );
    if (isSUCCEED) {
      let oldItem = this.#_items.get(_name);
      if (oldItem !== newItem) {
        isSUCCEED = this.#_items.insert(_name, newItem);
        if (isSUCCEED) {
          let cats = this.getItemCategories(_name);
          let cat_items = null;
          for (let cat of cats) {
            cat_items = this.#_cats.get(cat);
            if (cat_items instanceof Set) {
              cat_items.delete(oldItem);
              cat_items.add(newItem);
            };
          };
        };
      };
    };
    return isSUCCEED;
  }

  /**
   * @param {any}
   * @returns {bool}
   * @description Checks whether a category addressed by a given key is exists.
   */
  hasCategory(name){
    return this.#_cats.has(this.#_manip.__readIndexName(name));
  }

  /**
   * @param {any}
   * @returns {any}
   * @description Returns a category addressed by a given key.
   */
  getCategory(name){
    const _name = this.#_manip.__readIndexName(name);
    return this.#_cats.get(_name);
  }

  /**
   * @param {any}
   * @returns {bool}
   * @description Adds a category under a given key.
   */
  addCategory(name){
    const _name = this.#_manip.__readIndexName(name);
    let isSUCCEED = this.#_manip.__isValidIndex(_name);
    if (isSUCCEED) {
      if (this.#_cats.has(_name)) {
        isSUCCEED = this.#_cats.set(_name, new Set());
      } else {
        isSUCCEED = this.#_cats.insert(_name, new Set());
      };
    };
    return isSUCCEED;
  }

  /**
   * @param {any}
   * @returns {bool}
   * @description Deletes a category addressed by a given key.
   */
  delCategory(name){
    const _name = this.#_manip.__readIndexName(name);
    let isSUCCEED = this.#_cats.delete(_name);
    if (isSUCCEED && this.#_index.size > 0) {
      for (let obj of this.#_index.values) {
        if (obj instanceof Set) obj.delete(_name);
      };
    };
    return isSUCCEED;
  }

  /**
   * @param {any} - old key
   * @param {any} - new key
   * @returns {bool}
   * @description Renames a category addressed by a given key.
   */
  renameCategory(oldName, newName){
    const _oldName = this.#_manip.__readIndexName(oldName);
    const _newName = this.#_manip.__readIndexName(newName);
    let isSUCCEED = (
      this.#_manip.__isValidIndex(_newName)
      && this.#_cats.has(_oldName)
    );
    if (isSUCCEED && _oldName !== _newName) {
      isSUCCEED = this.#_cats.rename(_oldName, _newName);
      if (isSUCCEED && this.#_index.size > 0) {
        for (let obj of this.#_index.values) {
          if (obj instanceof Set) {
            obj.delete(_oldName);
            obj.add(_newName);
          };
        };
      };
    };
    return isSUCCEED;
  }

  /**
   * @param {any}
   * @param {...any} [cats] - categories list
   * @returns {bool}
   * @description Merges all listed keys into a key given by a first parameter.
   */
  mergeCategories(name, ...cats){
    const _name = this.#_manip.__readIndexName(name);
    let isSUCCEED = this.#_cats.merge(_name, ...cats);
    if (isSUCCEED) {
      let list = readAsListE({
        //keep_empty: false,
        userFn: {
          readItemFn: this.#_manip.__readIndexName,
          checkItemFn: this.#_manip.__isValidIndex,
        },
      }, ...cats);
      if (list.length > 0) {
        let index = this.#_index.keys;
        for (let item_name of index) {
          let item_cats = this.#_index.get(item_name);
          if (item_cats instanceof Set) {
            let do_index = false;
            for (let cat_name of list) {
              if (item_cats.has(cat_name)) {
                item_cats.delete(cat_name);
                do_index = true;
              };
            };
            if (do_index) item_cats.add(_name);
          };
        };
      };
    };
    return isSUCCEED;
  }

};

// === module exports block ===

exports.TNamedItemsCollection = TNamedItemsCollection;
exports.TNamedSetsCollection = TNamedSetsCollection;
exports.TItemsICollection = TItemsICollection;
