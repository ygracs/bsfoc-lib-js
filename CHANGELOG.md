#### *v0.2.1*

Release version.

> - `baseFunc.md` updated in module `doc` directory;
> - `baseObj.md` updated in module `doc` directory;
> - `baseClass.md` updated in module `doc` directory;
> - move function `isArray` and `isPlainObject` into `lib-hfunc.js` module;
> - fix behavior of `valueToIndex` function in cases when an empty string like arguments is passed;
> - change behavior of `valueToIDString` function (see docs for details);
> - add function `tryToBool` into `baseFunc.js` module;
> - add function `tryToNumber` into `baseFunc.js` module;
> - add function `valueToEntry` into `baseObj.js` module;
> - add method to `TItemsList` class: `forEach';
> - `README.md` updated.

#### *v0.2.0*

Release version.

> - `baseFunc.md` updated in module `doc` directory;
> - `baseObj.md` updated in module `doc` directory;
> - `baseClass.md` updated in module `doc` directory;
> - change behavior of `readAsString` function (see docs for details);
> - change behavior of `readAsBool` function: values of "FALSE" and "TRUE" (string value in upper case) are also acceptable;
> - change behavior of `readAsListS` function (see docs for details);
> - change behavior of `readAsListE` function (see docs for details);
> - add properties to `TItemsList` class: `minIndex`, 'maxIndex';
> - change behavior of `loadItems` method of `TItemsList` class (see docs for details);
> - change behavior of `TNamedItemsCollection` class constructor (see docs for details);
> - change behavior of `TNamedSetsCollection` class constructor (see docs for details);
> - change behavior of `TItemsICollection` class constructor (see docs for details).

#### *v0.1.4*

Release version.

> - `baseFunc.md` updated in module `doc` directory;
> - `baseObj.md` updated in module `doc` directory;
> - add function `isEmptyOrNotString` into `baseFunc.js` module;
> - add function `valueToEntries` into `baseObj.js` module;
> - change behavior of `readAsListE` function (see docs for details).

#### *v0.1.3*

Release version.

> - `baseFunc.md` updated in module `doc` directory;
> - `baseObj.md` updated in module `doc` directory;
> - `baseClass.md` updated in module `doc` directory;
> - add function `valueToIDString` into `baseFunc.js` module;
> - change behavior of `readAsBool` function (see docs for details);
> - change behavior of `readAsListS` function (see docs for details).

#### *v0.1.2*

Release version.

> - `baseClass.md` updated in module `doc` directory;
> - split `baseClass.js` module into two sub-modules: `baseClass/lists.js` and `baseClass/collections.js`;
> - change default behavior of `delItem`/`delItemEx` method of `TItemsList` class to the previous spec;
> - some fixes in `baseClass/lists.js` module;
> - some fixes in `baseClass/collections.js` module.

#### *v0.1.1*

Release version.

> - `baseFunc.md` updated in module `doc` directory;
> - `baseObj.md` updated in module `doc` directory;
> - some fixes in `baseFunc.js` module;
> - some fixes in `baseObj.js` module;
> - some fixes in `baseClass.js` module;
> - added function `valueToArrayEx` to `baseFunc.js` module.

#### *v0.1.0*

Release version.

> - `baseClass.md` updated in module `doc` directory;
> - some fixes in `baseClass.js` module;
> - some fixes in `baseObj.js` module.

#### *v0.0.1-v0.0.11*

Pre-release version.
