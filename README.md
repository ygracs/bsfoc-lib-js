| ***rev.*:** | 0.1.11     |
|:----------- | ----------:|
| ***date***: | 2024-06-23 |

## Introduction

This module provide a base set of a helper functions and an objects collection for javascript.

## Use cases

### Installation

`npm install @ygracs/bsfoc-lib-js`

## Content

> Notes on upgrade to `v0.2.0`
> > The `v0.2.0` is not fully compatible with a previous one due to some breaking changes. For more details read the docs for a functions and a classes used in your project.

### 1. list of helper functions:

- logical functions:
  - isValidIndex;
  - isEmptyString;
  - isEmptyOrNotString;
  - isNotEmptyString;
  - isNullOrUndef;
  - isInteger;
  - inRangeNum;
- special functions:
  - tryNotNullish;
  - readAsBoolEx;
  - readAsNumberEx;
- other functions:
  - valueToIndex;
  - valueToIDString;
  - tryToBool;
  - tryToNumber;
  - readAsBool;
  - readAsNumber;
  - readAsString;
- experimental functions:
  - isValidId;
  - isNotEmptyStringEx.

> for more see "[`baseFunc.md`](doc/baseFunc.md)" in a package docs

### 2. list of object functions:

- logical functions:
  - isArray;
  - isObject;
  - isPlainObject;
  - isDateObject;
  - isPropertyExists;
  - isMethodExists;
- other functions:
  - arrayFrom;
  - valueToArray;
  - valueToEntry;
  - valueToEntries;
  - readAsListE;
  - readAsListS;
- experimental functions:
  - doMethodIfExists;
  - valueToArrayEx.

> for more see "[`baseObj.md`](doc/baseObj.md)" in a package docs

### 3. list of classes:

- lists:
  - TItemsList;
  - TItemsListEx;
- collections:
  - TNamedItemsCollection;
  - TNamedSetsCollection;
  - TItemsICollection.

> for more see "[`baseClass.md`](doc/baseClass.md)" in a package docs
