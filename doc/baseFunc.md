|***rev.*:**|0.1.18|
|:---|---:|
|***date***:|2024-06-23|

## Introduction

This paper contains a descriptions of all helper functions presented in "`baseFunc.js`".

### Logical functions

#### **isValidId(id_string)**

This function returns `true` if `id_string` is a valid identifier.

#### **isValidIndex(index)**

This function returns `true` if the value given by `index` parameter can be interpreted as a valid index value.

> Note: *for use in this case, index is validated as a non-negative integer number.*

#### **isEmptyString(value)**

This function returns `true` if a given value represents an empty string.

> Note: Any strings filled with a space characters only regarded by this function as an empty strings.

#### **isEmptyOrNotString(value)**

> since: \[v0.1.4]

This function returns `true` if a given value represents an empty string or is not a string at all.

> Note: Any strings filled with a space characters only regarded by this function as an empty strings.

#### **isNotEmptyString(value)**

This function returns `true` if a given value represents a non-empty string.

#### **isNullOrUndef(value)**

This function returns `true` if a given value is equal to `undefined` or `null`.

#### **isInteger(value)**

This function returns `true` if a given value is an integer number.

> Note: technically `isInteger` is simply a shorter reference to a `Number.isInteger` method.

#### **inRangeNum(min, max, value)**

This function returns `true` if a given `value` is in range between `min` and `max` values or equal to one of them.

### Special functions

#### **tryNotNullish(args_list)**

This function scans a list of a given arguments and returns the first one that is not equal to `undefined` or `null`. If no one was found, `null` will be returned.

#### **readAsBoolEx(value\[, defValue])**

This function similar to the [`readAsBool`](#readasboolvalue-defvalue) function but in contrast tries to convert a `value` parameter to a `boolean` type before the result is retuned.

The `value` conversion process consists of the following rules:

1. if `value` is equal to `null`, `false` is returned;
2. if `value` is a number then:
   - `false` will be returned if a value is equal to `0`,
   - `true` will be returned in other cases (except `NaN`),
   - in cases if `NaN` is matched a `defValue` is returned;
3. if `value` is a string then:
   - if a values are `null` or `false` (also its uppercase variants), `false` is returned,
   - if a values are `off` (also its uppercase variants), `false` is returned `[since v0.2.1]`,
   - if a values are `no`, `No` or `NO`, `false` is returned `[since v0.2.1]`,
   - if a value is `true` (also its uppercase variant), `true` is returned,
   - if a values are `on` (also its uppercase variants), `true` is returned `[since v0.2.1]`,
   - if a values are `yes`, `Yes` or `YES, `true` is returned `[since v0.2.1]`,
   - for values that can represents a numbers the rule "2" is applied,
   - in other cases a `defValue` will be returned.

> Note: For a strings, the value is trimmed before it's processed.

#### **readAsNumberEx(value\[, defValue])**

This function similar to the [`readAsNumber`](#readasnumbervalue-defvalue) function but in contrast tries to convert a `value` parameter to a `number` type before the result is retuned.

The `value` conversion process consists of the following rules:

1. if `value` is equal to `null`, `0` is returned;
2. if `value` is a boolean then:
   - if `false`, `0` will be returned,
   - if `true`, `1` will be returned,
3. if `value` is a number and `NaN` is matched a `defValue` is returned;
4. if `value` is a string then:
   - if a values are `null` (also its uppercase variants), `0` is returned,
   - for values that can represents a booleans (i.e, `false` or `true` and also its uppercase variants) the rule "2" is applied,
   - for values that can represents a numbers the rule "3" is applied,
   - in other cases a `defValue` will be returned.

> Note: For a strings, the value is trimmed before it's processed.

### Other functions

#### **valueToIndex(value)**

This function tries to convert a given value to a valid index value (see [`isValidIndex`](#isvalidindexindex)). If function failed, `-1` is returned.

#### **valueToIDString(value\[, options])**

> since: \[v0.1.3]

This function tries to convert a given value to a valid `ID_STRING` value. If function failed, `null` is returned.

An `options` parameter must be a boolean or an object.

If `options` is an `object` it contains the settings listed in table below:

|name option|option type|default value|description|
|:---|---|---:|:---|
|`onlyNES`|`boolean`|`true`|defines whether or not the empty value is allowed|
|`ignoreNumbers`|`boolean`|`false`|defines whether or not a number can be treated as a valid ID `[since v0.2.1]`|

If `options` is of a `boolean` type the function treats it as a value of `options.onlyNES`.

#### **tryToBool(value)**

> since: \[v0.2.1]

This function tries to read a given value and returns it if that value has a `boolean` type. If function failed, `null` is returned.

> NOTE: For details of a `value` parameter see the description for the [`readAsBool`](#readAsBool) function.

#### **tryToNumber(value)**

> since: \[v0.2.1]

This function tries to read a given value and retuns it if that value has a `number` type. If function failed, `null` is returned.

> NOTE: For `value` parameter the function accepts a strings if those can be converted to a number.

#### **readAsBool(value\[, defValue])**

This function reads a given value and returns it if that value has a `boolean` type. If function failed, one of the following cases may occur:

- if `defValue` is defined and has a `boolean` type, then `defValue` will be returned;
- `false` will be returned in other cases.

> NOTE: `[since v0.1.3]` For `value` parameter the function accepts a strings if those value equal to `true` or `false` (*both strictly in low case and each may be padded with spaces before and/or after*) and treats those as booleans.

> NOTE: `[since v0.2.0]` For `value` parameter the function also accepts a strings if those value equal to `TRUE` or `FALSE` (*both strictly in upper case and each may be padded with spaces before and/or after*) and treats those as booleans.

#### **readAsNumber(value\[, defValue])**

This function reads a given value and retuns it if that value has a `number` type. If function failed, one of the following cases may occur:

- if `defValue` is defined and has a `number` type, then `defValue` will be returned;
- `0` will be returned in other cases.

> NOTE: `[since v0.1.3]` For `value` parameter the function accepts a strings if those can be converted to a number.

#### **readAsString(value\[, options]])**

> since: \[v0.2.0]

This function reads a given value and returns it as a string.

An `options` parameter must be a boolean, a string or an object.

If `options` is an `object` it contains the settings listed in table below:

|name option|option type|default value|description|
|:---|---|---:|:---|
|`useTrim`|`boolean`|`false`|defines whether or not the resulting string must to be trimmed|
|`letterCase`|`string`|---|if given defines a function behavior on the resulting string|
|`numberToString`|`boolean`|`false`|if set a values of `number` types will be converted to a string|
|`boolToString`|`boolean`|`false`|if set a values of `boolean` types will be converted to a string|
|`boolToUpperCase`|`boolean`|`false`|if set a values of `boolean` types when converted to a string will be present in upper case|
|`defValue`|`string`|`EMPTY_STRING`|if given represents a value which will be returned in case the function failed|

If `options` is of a `boolean` type the function treats it as a value of `options.useTrim`.

If `options` is of a `string` type the function treats it as a value of `options.defValue`.

The function behavior for `options.letterCase` settings see in the talbe below:

|option value|description|
|:---|:---|
|`upper`|the resulting string must to be present in upper case|
|`lower`|the resulting string must to be present in lower case|
|any other values|the resulting string will be not transformed|

### Experimental functions

> Note: Purpose of those functions will be discussed and some may be deprecate and make obsolete or functionality may be altered. So use it with cautions in mind.

#### **isNotEmptyStringEx(args)**

This function reads a given arguments and returns `true` if none of them can be consider as an empty string value.
