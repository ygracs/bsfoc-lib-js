|***rev.*:**|0.1.15|
|:---|---:|
|***date***:|2024-06-23|

## Introduction

This paper contains descriptions of all object functions presented in "`baseObj.js`".

### Logical functions

#### **isArray(value)**

This function checks if a given value represents an `Array` and if so returns `true`.

> Note: technically `isArray` is simply a shorter reference to an `Array.isArray` method.

#### **isObject(value)**

This function checks if a given value represents an `Object` instance of any kind and if so returns `true`.

#### **isPlainObject(value)**

This function checks if a given value represents a simple object and if so returns `true`.

> Note: technically for current implementation the method checks whether an object not present an instance of `Array`. So for example an instance of `Set` will pass a check process.

#### **isDateObject(value)**

This function checks if a given value represents a `DateTime` object and if so returns `true`.

#### **isPropertyExists(object, prop)**

This function checks if the named property given by a `prop` parameter exists for `object` and if succeed returns `true`.

> Note: This function will fail if a given name for searched object attribute represents an object method. In such case you need to use [`isMethodExists`](#ismethodexistsobject-method).

#### **isMethodExists(object, method)**

This function checks if a given object have the method named by a `method` parameter and if so returns `true`.

> Note: This function will fail if a given name for searched object attribute represents an object property. In such case you need to use [`isPropertyExists`](#ispropertyexistsobject-method).

### Other functions

#### **arrayFrom(value)**

This function transforms a given `value` to an array.

> Note: technically `arrayFrom` is simply a shorter reference to an `Array.from` method.

#### **valueToArray(value)**

This function transforms a given `value` to an array in case it is not. If given value is `undefined` or `null` an empty array will be returned.

#### **valueToEntry(value)**

> since: \[v0.2.1]

This function transforms a given `value` to an entry which represents a pair of \<key> and its value. If failed `null` is returned.

#### **valueToEntries(value\[, options])**

> since: \[v0.1.4]

This function transforms a given `value` to an array of an entries which represents a pairs of \<key> and its value.

For details on `options` parameter see a description of a [`readAsString`](baseFunc.md) function.

#### **readAsListE(options, args)**

This functions reads arguments given by `args` and returns it as an array of elements according to the given options.

The `options` parameter must be a boolean or an object.

If `options` is of an `object` type it contains the following parameters:

|option name|value type|default value|description|
|:---|---|---:|:---|
|`keepEmpty`|`boolean`|`false`|`[since v0.1.4]` defines a behavior of a `checkItemFn` function|
|`unfoldArray`|`boolean`|`false`|`[since v0.1.4]` if set to `true` require to unfold 1st level of arrays listed in `args` (*also applied to a set instances if present*)|
|`userFn`|`object`|---|`[since v0.2.0]` contains a user defined callbacks to perform ops upon a given elements|
|`userOptions`|`any`|---|`[since v0.2.0]` <reserved>|

If `options` is of a `boolean` type the function treats it as a value of `options.keepEmpty`.

The `options.userFn` contains the following parameters:

|option name|value type|default value|description|
|:---|---|---:|:---|
|`checkItemFn`|`function`|---|defines a function that checks whether or not the given element can be accepted.|
|`readItemFn`|`function`|---|defines a function that handles the given element.|

The `checkItemFn` function gets two parameters:

1. `item` - the element that must be verified;
2. `keepEmpty` - a `boolean` value which defined whether or not an element that failed a verification process must be accepted.

The `readItemFn` function gets an element and returns it transformed according to the rules set by some user logics.

#### **readAsListS(options, args)**

This functions reads arguments given by `args` and returns it as an array of strings according to the given options.

An `options` parameter must be a boolean, a string or an object.

If `options` is an `object` it contains the settings listed in table below:

|option name|value type|default value|description|
|:---|---|---:|:---|
|`keepEmpty`|`boolean`|`false`|`[since v0.1.3]` defines whether or not the empty string must to be kept|
|`unfoldArray`|`boolean`|`false`|`[since v0.1.3]` defines whether or not the arrays must be unwrapped|
|`unfoldLevel`|`number`|`1`|`[since v0.1.3]` defines a level at which the arrays must be unwrapped|

If `options` is of a `boolean` type the function treats it as a value of `options.useTrim`.

If `options` is of a `string` type the function treats it as a default value.

The `options.unfoldLevel` applied only if an `option.unfoldArray` is set to `true`. It must be equal or greater then `0` if else it sets to default value. Also a value of `-1` is accepted, in such a case no limit will be applied.

Also `options` parameter may contain other settings. For those see a description to an `options` parameter of a [`readAsString`](baseFunc.md) function

### Experimental functions

> Note: Purpose of those functions will be discussed and some may be deprecate and make obsolete or functionality may be altered. So use it with cautions in mind.

#### **doMethodIfExists(object, method\[, args])**

This function tries to call method named by a `method` parameter for a given object if such a method exists.

#### **valueToArrayEx(value, callback)**

This function transforms a given `value` to an array in case it is not. Unlike `valueToArray` if given value is `null` this function returns an array containing only one element equal to `null`.

The function can receive a "callback"-function which purpose is verify and transform elements through a conversion process. The "callback"-function if given is called on every element of a processed array, receives only two arguments (`element` and `index`) and must return that element after its transformation.
