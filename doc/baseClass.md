|***rev.*:**|0.1.24|
|:---|---:|
|***date***:|2024-06-23|

## Introduction

This paper contains descriptions of all classes presented in "`baseClass.js`".

## Module classes

### **TItemsList**

This class implements a base container for managing an objects such as lists.

#### class constructor

The class constructor creates a new instance of the class. No arguments is received by it.

#### class properties

The table below describes a properties of a `TItemsList` class:

|property name|property type|read only|description|
|:---|---|---|:---|
|`count`|`number`|yes|presents a quantity of items|
|`curIndex`|`index`|no|presents an index of a current item|
|`curItem`|`any`|no|presents a current item addressed by the `curIndex`|
|`minIndex`|`index`|yes|presents a min index|
|`maxIndex`|`index`|yes|presents a max index|

#### class methods

##### **clear()**

This method deletes all items from the list.

##### **isEmpty()**

This method returns `true` if no items exists in the list.

##### **isNotEmpty()**

This method returns `true` if at least one item is exists in the list.

##### **chkIndex(value)**

This method checks if the given `value` represents a valid index value and fits the range of all possible indexes belonging to the list. If so `true` is returned.

##### **setIndex(value)**

This method sets a current item index for the list if the given `value` represents a valid index value and fits the range of all possible indexes belonging to the list. If succeed `true` is returned.

##### **rstIndex()**

This method resets a current item index for the list.

> Note: You need to use this method if you want to reset the value of a `curIndex` property to its initial status which is set to `-1`. You can\'t perform for that a direct action on a `curIndex` property or with the `setIndex` method.

##### **srchIndex(item\[, index])**

This method seeks an index of a given `item` and if it is found returns its index in the list. If failed `-1` is returned.

Searching starts from the position given by the `index`, if it is given and is valid. By default the searching starts from the first item in the list.

> Note: In case a given `index` is valid but its value is greater than the max index of the list, the method will fail.

##### **getItem(index)**

This method returns the item addressed by the `index`.

> Note: If failed `undefined` is returned.

##### **setItem(index, item)**

This method places the given `item` into position addressed by the `index`. If succeed `true` is returned.

> Note: You can\'t apply the item, which value is `undefined`, to the list members. In such a case the method will failed.

##### **addItem(item)**

This method adds the given `item` to a list members. If succeed `true` is returned.

> Note: You can\'t add the item, which value is `undefined`, to the list members. In such a case the method will failed.

##### **addItemEx(item\[, options])**

This method adds the given `item` to a list members. If succeed the item index will be returned, if else `-1` is returned.

If `options` parameter is given and is set to `true`, in case of success, the `curIndex` property is adjusted by the method.

> Note: The `options` parameter must be a `boolean` type. If not a default value will be used (*by default it is `false`*).

##### **delItem(index\[, options])**

This method deletes the item addressed by the given `index` parameter from a list members. If succeed `true` is returned.

If `options` parameter is set to `true` (*a default value*), the `curIndex` property is adjusted by the method. If it is set to `false` the `curIndex` property is not adjusted before a value of the property exited out of the list indexes range that caused the property to be reset. In case the `options` is not given or is not a type of a `boolean`, its receives the default value.

##### **delItemEx(index\[, options])**

This method deletes the item addressed by the given `index` parameter from a list members. If succeed, the method returns an item that was deleted, if failed, `undefined` is returned.

The use of `options` parameter is similar to the `delItem` method.

##### **loadItems(list, options)**

This method adds all given items listed by the `list` to a list members and returns a quantity of the items that was added successfully.

The `options` parameter is an `object` that contains the following set of parameters:

|option name|value type|default value|description|
|:---|---|---:|:---|
|`useClear`|`boolean`|`true`|if set to `true`, before the load of the items, the list is cleared|
|`curIndex`|`index`|---|if given, after the items was loaded, defines which index will be set as current|
|`resetIndex`|`boolean`|`false`|if set to `true`, after the load of the items, the list index will be reset|

> ***Note**: The `resetIndex` is used only if `useClear` is `false` and an attempt to set `curIndex` is failed.*

##### **forEach(cb)**

> since: \[v0.2.1]

This method calls a given function for each item in a list.

### **TItemsListEx**

This class extends a base container provided by the `TItemsList` class with a new functionalities.

#### class constructor

The class constructor creates a new instance of the class. No arguments is received by it.

#### class properties

The table below describes a properties of a `TItemsListEx` class:

|property name|property type|read only|description|
|:---|---|---|:---|
|`count`|`number`|yes|presents a quantity of items|
|`curIndex`|`index`|no|presents an index of a current item|
|`curItem`|`any`|no|presents a current item addressed by the `curIndex`|
|`minIndex`|`index`|yes|represents a min value of a possible index that an item can obtains|
|`maxIndex`|`index`|yes|represents a max value of a possible index that an item can obtains|
|`prevIndex`|`index`|yes|represents an index of an item that is previous to the current|
|`nextIndex`|`index`|yes|represents an index of an item that is next to the current|

#### class methods

##### **clear()**

This method deletes all items from the list.

##### **isEmpty()**

This method returns `true` if no items exists in the list.

##### **isNotEmpty()**

This method returns `true` if at least one item is exists in the list.

##### **chkIndex(value)**

This method checks if the given `value` represents a valid index value and fits a range of all possible indexes belonging to the list. If so `true` is returned.

##### **chkIndexEx(value\[, mode])**

This method is similar to a `chkIndex` methods but receives a `mode` parameter which allow to choose what type of result the function returned.

The `mode` parameter treats as follows:

- if set to `false`, the result returned by the method is a `boolean` value;
- if `mode` parameter set to `true`, the result returned by the method has an index type;
- if not given or has a type of not a `boolean` value, the default value is used which is set to `false`.

##### **setIndex(value)**

This method sets a current item index for the list if the given value represents a valid index value and fits a range of all possible indexes belonging to the list. If succeed `true` is returned.

##### **rstIndex()**

This method resets a current item index for the list.

> Note: You need to use this method if you want to reset the value of a `curIndex` property to its initial status which is set to `-1`. You can\'t perform for that a direct action on a `curIndex` property or with the `setIndex` method.

##### **srchIndex(item\[, index])**

This method seeks an index of a given item and if found returns its index in the list. If failed `-1` is returned.

Searching starts from the position given by the `index`, if it is given and is valid. By default the searching starts from the first item in the list.

> Note: In case a given `index` is valid but its value is greater than the max index of the list, the method will fail.

##### **srchNextIndex(item)**

This method is similar to a `srchIndex` method but it starts from where the previous search calls was stopped.

> Note: You can\'t use this method if a previous search operation has failed. In such a case, you must start a new search with a `srchIndex` method. Also the method will failed if a `srchIndex` method was not called early.

##### **getItem(index)**

This method returns the item addressed by the `index`.

> Note: If failed `undefined` is returned.

##### **setItem(index, item)**

This method places the given `item` into position addressed by the `index`. If succeed `true` is returned.

> Note: You can\'t apply the item, which value is `undefined`, to the list members. In such a case the method will failed.

##### **addItem(item)**

This method adds the given `item` to a list members. If succeed `true` is returned.

> Note: You can\'t add the item, which value is `undefined`, to the list members. In such a case the method will failed.

##### **addItemEx(item\[, options])**

This method adds the given `item` to a list members. If succeed the item index will be returned, if else `-1` is returned.

If `options` parameter is given and is set to `true`, in case of success, the `curIndex` property is adjusted by the method.

> Note: The `options` parameter must be a `boolean` type. If not a default value will be used (*by default it is `false`*).

##### **delItem(index\[, options])**

This method deletes the item addressed by the given `index` parameter from a list members. If succeed `true` is returned.

If `options` parameter is set to `true` (*a default value*), the `curIndex` property is adjusted by the method. If it is set to `false` the `curIndex` property is not adjusted before a value of the property exited out of the list indexes range that caused the property to be reset. In case the `options` is not given or is not a type of a `boolean`, its receives the default value.

##### **delItemEx(index\[, options])**

This method deletes the item addressed by the given `index` parameter from a list members. If succeed, the method returns an item that was deleted, if failed, `undefined` is returned.

The use of `options` parameter is similar to the `delItem` method.

##### **loadItems(list, options)**

This method adds all given items listed by the `list` to a list members and returns a quantity of the items that was added successfully.

The `options` parameter is an `object` that contains the following set of parameters:

|option name|value type|default value|description|
|:---|---|---:|:---|
|`useClear`|`boolean`|`true`|if set to `true`, before the load of the items, the list is cleared|
|`curIndex`|`index`|`undefined`|if given, after the items was loaded, defines which index will be set as current|
|`resetIndex`|`boolean`|`false`|if set to `true`, before the load of the items, the list index will be reset|

> ***Note**: The `resetIndex` is used only if `useClear` is `false` and an attempt to set `curIndex` is failed.*

##### **forEach(cb)**

> since: \[v0.2.1]

This method calls a given function for each item in a list.

### **TNamedItemsCollection**

This class implements a simple collection of named elements.

#### class constructor

The class constructor creates a new instance of the class. It receives an `object` as its argument which contains an options listed below:

|option name|option type|default value|description|
|:---|---|---:|:---|
|`forceIfKeyExists`|`boolean`|`false`|alters behavior of the `add` and `rename` class methods. If set to `true`, the replacement of existing keys is allowed|
|`userFn`|`object`|---|contains a set of user defined functions|

The `userFn` provides the set of next parameters:

|option name|option type|default value|description|
|:---|---|---:|:---|
|`checkIndexFn`|`function`|---|defines a function that checks whether or not the given key can be accepted|
|`readIndexFn`|`function`|---|defines a function that handles the given key|

The `checkIndexFn` function gets the key that must be verified and returns a `boolean` value as a result of a verification process.

The `readIndexFn` function gets a key and returns it transformed according to the rules set by some user logics.

#### class properties

The table below describes a properties of a `TNamedItemsCollection` class:

|property name|property type|read only|description|
|:---|---|---|:---|
|`keys`|`array`|yes|returns an array of all collection keys|
|`values`|`array`|yes|returns an array of all collection values|
|`entries`|`array`|yes|returns an array of all collection entries as a "key-value" pairs|
|`size`|`number`|yes|represents a collection size|

#### class methods

##### **clear()**

This method deletes all items that a collection holds.

##### **has(key)**

This method checks if the given `key` exists in the collection.

##### **get(key)**

This method returns a value of a key given to the method. If the key not found, `null` is returned.

##### **add(key)**

This method adds the given `key` to a collection members. If succeed, `true` returned.

> Note: if a given `key` is already exists, the method behavior depends on the value of a `forceIfKeyExists` collection settings option. In case, this option sets to `true` the existing key will be reset, if sets to `false` (*the default settings*) the method failed.

##### **set(key, value)**

This method sets a value of a key addressed by the `key` parameter, if such a key exists. If succeed, `true` returned.

> Note: You can\'t set a key if a given `item` value is `undefined`. In such a case the method will failed.

##### **insert(key, value)**

This method sets a value of a key addressed by the `key` parameter. If succeed, `true` returned.

> Note: You can\'t set a key if a given `item` value is `undefined`. In such a case the method will failed.

##### **delete(key)**

This method deletes element addressed by a given `key` from a collection members.

##### **rename(oldKey, newKey)**

This method renames a key addressed by the `oldKey` parameter to a name given by a `newKey`, if an `oldKey` exists.

> Note: if a key addressed by a `newKey` already exists, the method behavior depends on the value of a `forceIfKeyExists` collection settings option. In case, this option sets to `true` the existing key will be replaced, if sets to `false` (*the default settings*) the method failed.

#### class methods (*special*)

##### **isValidKey(name)**

This special method verifies if a value given by a `name` parameter is valid.

> Note: A value which returned by the method depends on the wrapped function. By default the `isValidKey` will use a static class method of the same name.

#### class methods (*static*)

##### **isValidKey(name)**

This method verifies that a value given by a `name` parameter is valid according to the following base rules: the value is not equal to `undefined` or `null`, if so `true` is returned.

### **TNamedSetsCollection**

This class extends a 'TNamedItemsCollection' class with a new functionalities allow to handle a sets of elements.

#### class constructor

The class constructor creates a new instance of the class. It receives an argument which contains an options listed below:

|option name|option type|default value|description|
|:---|---|---:|:---|
|`forceIfKeyExists`|`boolean`|`false`|alters behavior of the `add` and `rename` class methods. If set to `true`, the replacement of existing keys is allowed|
|`forceIfItemExists`|`boolean`|`false`|*reserved*|
|`userFn`|`object`|---|contains a set of user defined functions|

The `userFn` provides the set of next parameters:

|option name|option type|default value|description|
|:---|---|---:|:---|
|`checkIndexFn`|`function`|---|defines a function that checks whether or not the given key can be accepted|
|`readIndexFn`|`function`|---|defines a function that handles the given key|
|`checkItemFn`|`function`|---|defines a function that checks whether or not the given item can be accepted|
|`readItemFn`|`function`|---|defines a function that handles the given item|

The `checkIndexFn` function gets the key that must be verified and returns a `boolean` value as a result of a verification process.

The `readIndexFn` function gets a key and returns it transformed according to the rules set by some user logics.

The `checkItemFn` function gets the item that must be verified and returns a `boolean` value as a result of a verification process.

The `readItemFn` function gets an item and returns it transformed according to the rules set by some user logics.

#### class properties

The table below describes a properties of a `TNamedSetsCollection` class:

|property name|property type|read only|description|
|:---|---|---|:---|
|`keys`|`array`|yes|returns an array of all collection keys|
|`values`|`array`|yes|returns an array of all collection values|
|`entries`|`array`|yes|returns an array of all collection entries as a "key-value" pairs|
|`size`|`number`|yes|represents a collection size|

#### class methods

##### **clear(\[keys])**

This method deletes all items and all keys that collection holds in case if none was given. If any keys are given, then only the items belonging to the sets addressed by a listed keys will be deleted from the collection.

##### **has(key)**

This method checks if the given `key` exists in the collection.

##### **get(key)**

This method returns a value of a key given to the method. If the key not found, `null`is returned.

##### **add(key)**

This method adds the given key to a collection members. If succeed, `true` returned.

> Note: if a given key is already exists, the method behavior depends on the value of a `forceIfKeyExists` collection settings option. In case, this option sets to `true` the existing key will be reset, if sets to `false` (*the default settings*) the method failed.

##### **addTo(key, item)**

This method adds a given item to a members of the set addressed by a given key. If succeed `true` is returned.

##### **set(key, value)**

This method sets a value of a key addressed by the `key` parameter, if such a key exists. If succeed, `true` returned.

The `value` can be any valid item accepted by the instance or an array or a set of those items.

> Note: If 'value' is omitted the empty set is used by this methods.

##### **insert(key, value)**

This method sets a value of a key addressed by the `key` parameter. If succeed, `true` returned.

The `value` can be any valid item accepted by the instance or an array or a set of those items.

> Note: If 'value' is omitted the empty set is used by this methods.

##### **delete(key)**

This method deletes the given key from a collection members.

##### **deleteFrom(key, item)**

This method deletes a given item from the set members addressed by a `key` parameter. If succeed `true` returned.

##### **rename(oldKey, newKey)**

This method renames a key addressed by the `oldKey` parameter to a name given by a `newKey`, if an `oldKey` exists.

> Note: if a key addressed by a `newKey` already exists inside the collection, the method behavior depends on the value of a `forceIfKeyExists` collection settings option. In case, this option sets to `true` the existing key will be replaced, if sets to `false` (*the default settings*) the method failed.

##### **merge(key, args)**

This method merge all sets addressed by a keys listed in `args` with a set addressed by a `key` parameter and places a resulted set into `key`. All keys listed by `args` will be deleted from the collection. If succeed `true` is returned.

##### **replace(key, oldItem, newItem)**

This method tries to replace an item with a new one in a set addressed by a given `key`. If succeed `true` returned.

#### class methods (*special*)

##### **isValidKey(name)**

This special method verifies if a value given by a `name` parameter is valid.

> Note: A value which returned by the method depends on the wrapped function. By default the `isValidKey` will use a static class method of the same name.

##### **isValidItem(item)**

This special method verifies if a value given by an `item` parameter is valid.

> Note: A value which returned by the method depends on the wrapped function. By default the `isValidItem` will use a static class method of the same name.

#### class methods (*static*)

##### **isValidKey(name)**

This method verifies that a value given by a `name` parameter is valid according to a followed base rules: the value is not equal to `undefined` or `null`, if so `true` is returned.

##### **isValidItem(name)**

This method verifies that a value given by a `name` parameter is valid according to a followed base rules: the value is not equal to `undefined` or `null`, if so `true` is returned.

#### class methods (*experimental*)

##### **__createNewSet(args)**

This method creates a new set of items from an items given by `args`.

### **TItemsICollection**

This class implements an indexed collection of a named elements.

#### class constructor

The class constructor creates a new instance of the class. It receives an argument which contains an options listed bellow:

|option name|option type|default value|description|
|:---|---|---:|:---|
|`forceIfKeyExists`|`boolean`|`false`|alters behavior of the `add` and `rename` class methods. If set to `true`, the replacement of existing keys is allowed|
|`forceIfItemExists`|`boolean`|`false`|<*reserved*>|
|`userFn`|`object`|---|contains a set of user defined functions|

The `userFn` provides the set of next parameters:

|option name|option type|default value|description|
|:---|---|---:|:---|
|`checkIndexFn`|`function`|---|defines a function that checks whether or not the given key can be accepted|
|`readIndexFn`|`function`|---|defines a function that handles the given key|
|`checkItemFn`|`function`|---|defines a function that checks whether or not the given item can be accepted|
|`readItemFn`|`function`|---|defines a function that handles the given item|

The `checkIndexFn` function gets the key that must be verified and returns a `boolean` value as a result of a verification process.

The `readIndexFn` function gets a key and returns it transformed according to the rules set by some user logics.

The `checkItemFn` function gets the item that must be verified and returns a `boolean` value as a result of a verification process.

The `readItemFn` function gets an item and returns it transformed according to the rules set by some user logics.

#### class properties

The table below describes a properties of a `TItemsICollection` class:

|property name|property type|read only|description|
|:---|---|---|:---|
|`ItemNames`|`array`|yes|returns a list of an item names for all items that collection holds as an array|
|`CategoryNames`|`array`|yes|returns a list of all items categories existing within collection as an array|

#### class methods

##### **clear()**

This method deletes a collection content.

##### **hasItem(name)**

This method checks if the item addressed by a given `name` exists in the collection.

##### **getItem(name)**

This method returns the item addressed by a given `name` if it exists in the collection. If not `null` is returned.

##### **addItem(name, item, cats)**

This method adds the given item with a name given by a `name` parameter to the members of all categories listed by a `cats`. If succeed `true` is returned.

##### **addItemToCategory(name, cats)**

This method adds the item addressed by a `name` parameter to the members of all categories listed by a `cats`. If succeed `true` is returned.

##### **delItem(name)**

This method deletes the item addressed by a given name from the collection. If succeed `true` is returned.

##### **delItemFromCategory(name, cats)**

This method deletes the item addressed by a `name` parameter from the members of all categories listed by a `cats`. If succeed `true` is returned.

##### **renameItem(name, newName)**

This method renames the item addressed by a `name` to its `newName` and if succeeds returned `true`.

##### **replaceItem(name, item)**

This method replaces the item addressed by a `name` with a new `item`. If succeed `true` is returned.

##### **hasCategory(name)**

This method checks if the category addressed by a given name exists in the collection.

##### **getCategory(name)**

This method returns the items that are belongs to the category addressed by a given name if the targeted category exists in the collection. If not `null` is returned.

##### **addCategory(name)**

This method adds the category named by a `name` parameter in the collection. If succeed `true` is returned.

##### **delCategory(name)**

This method deletes the category named by a `name` parameter from the collection. If succeed `true` is returned.

##### **renameCategory(name, newName)**

This method renames the category addressed by a `name` to its `newName`. If succeed `true` is returned.

##### **mergeCategories(name, cats)**

This method merges all categories listed by `name` (base category) and `cats` and places it in the collection under the base category name. If succeed `true` is returned.
