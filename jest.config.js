// [v0.1.004-20240622]

const cmdArgs = require('minimist')(process.argv.slice(2));

const testObj = new Map([
  [ 'bsf', 'baseFunc' ],
  [ 'bso', 'baseObj' ],
  [ 'bsc', 'baseClass' ],
  [ 'bsc:list', 'baseClass/TItemsList' ],
  [ 'bsc:listex', 'baseClass/TItemsListEx' ],
  [ 'bsc:colni', 'baseClass/TNamedItemsCollection' ],
  [ 'bsc:colns', 'baseClass/TNamedSetsCollection' ],
]);

let rootDirExt = '';
let obj = cmdArgs.object;
let tmSym = cmdArgs.target;
if (
  typeof tmSym !== 'string'
  || ((tmSym = tmSym.trim()) === '')
) {
  tmSym = '*';
};
if (
  typeof obj === 'string'
  && ((obj = obj.trim()) !== '')
) {
  if (testObj.has(obj)) rootDirExt = `/${testObj.get(obj)}/`;
};

module.exports = {
  rootDir: `__test__${rootDirExt}`,
  testMatch: [`**/?(${tmSym}.)+(spec|test).[jt]s?(x)`],
  //verbose: false,
};
