// [v0.0.002-20230627]

// === module init block ===

// === module extra block (helper functions) ===

const initOptions = (obj, options) => {
  if (options) {
    let {
      //container,
      items,
      curIndex,
      status,
    } = obj.preloads;
    obj.preloads = {
      //container,
      options,
      items,
      curIndex,
      status,
    };
  };
  return obj;
};

// === module main block ===

const initTest = (obj, options) => {
  obj.status.before = obj.preloads.status;
  return options ? initOptions(obj, options) : obj;
};

function runTestFn(obj, args){
  const getArgsList = (args) => {
    if (args !== null && typeof args === 'object') {
      if (Array.isArray(args)) return args;
      return Object.values(args);
    };
    return [];
  };
  if (
    obj !== null
    && typeof obj === 'object'
    && !Array.isArray(obj)
  ) {
    const { testInst, method, prop } = obj;
    if (
      testInst !== null
      && typeof testInst === 'object'
    ) {
      let testFn = testInst[method];
      if (typeof testFn === 'function') {
        //const descr = `${testInst[method]}`;
        //console.log('[*] CHECH Fn => '+descr);
        return testInst[method](...getArgsList(args));
      };
    };
  };
};

function genTestCase(data){
  if (
    data !== null
    && typeof data === 'object'
    && !Array.isArray(data)
  ) {
    const {
      cases,
      status,
    } = data;
    let {
      inGroup,
    } = data;
    if (Array.isArray(cases)) {
      let result = [];
      cases.forEach((data) => {
        if (
          data !== null
          && typeof data === 'object'
          && !Array.isArray(data)
        ) {
          const {
            msg,
            rem,
            values,
          } = data;
          result.push({
            msg,
            rem,
            values,
            status,
          });
        };
      });
      if (result.length) {
        if (
          typeof inGroup === 'string'
          && ((inGroup = inGroup.trim()) !== '')
        ) {
          result = [{
            msg: inGroup,
            param: result,
          }];
        };
        return result;
      };
    };
  };
};

// === module exports block ===

module.exports.runTestFn = runTestFn;
module.exports.genTestCase = genTestCase;
module.exports.initTest = initTest;
