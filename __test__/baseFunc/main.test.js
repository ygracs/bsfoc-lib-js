// [v0.1.027-20240622]

const t_Mod = require('#lib/baseFunc.js');

const t_Dat = require('./data/index.data.js');

const { runTestFn } = require('#test-dir/test-hfunc.js');

describe.each([
  {
    msg: 'function: tryNotNullish()',
    method: 'tryNotNullish',
  },
])('$msg', ({ method }) => {
  const testData = t_Dat[method];
  const testFn = (...args) => { return t_Mod[method](...args); };
  describe.each(testData.descr)('+ $msg $rem', ({ param }) => {
    describe.each(param)('$msg', ({ param }) => {
      it.each(param)('$msg $rem', ({ values, status }) => {
        const { ops, assertQty } = status;
        let result = undefined; let isERR = false;
        try {
          result = testFn(...values);
          //console.log('CHECK: => NO_ERR');
        } catch (err) {
          //console.log('CHECK: IS_ERR => '+err);
          isERR = true;
          result = err;
        } finally {
          expect.assertions(assertQty);
          expect(isERR).toBe(ops.isERR);
          if (isERR) {
            const { errType, errCode } = ops;
            expect(result instanceof errType).toBe(true);
            expect(result.code).toStrictEqual(errCode);
          } else {
            const { className, value } = ops;
            switch (typeof className) {
              case 'string' : {
                expect(result instanceof globalThis[className]).toBe(true);
                if (className !== 'HTMLElement') break;
              }
              default : {
                expect(result).toStrictEqual(value);
                break;
              }
            };
          };
        };
      });
    });
  });
});

describe.each([
  {
    msg: 'function: valueToIDString()',
    method: 'valueToIDString',
  }, {
    msg: 'function: isValidId()',
    method: 'isValidId',
  }, {
    msg: 'function: valueToIndex()',
    method: 'valueToIndex',
  }, {
    msg: 'function: isValidIndex()',
    method: 'isValidIndex',
  }, {
    msg: 'function: isEmptyString()',
    method: 'isEmptyString',
  }, {
    msg: 'function: isEmptyOrNotString()',
    method: 'isEmptyOrNotString',
  }, {
    msg: 'function: isNotEmptyString()',
    method: 'isNotEmptyString',
  }, {
    msg: 'function: isNullOrUndef()',
    method: 'isNullOrUndef',
  }, {
    msg: 'function: tryToBool()',
    method: 'tryToBool',
  }, {
    msg: 'function: readAsBool()',
    method: 'readAsBool',
  }, {
    msg: 'function: readAsBoolEx()',
    method: 'readAsBoolEx',
  }, {
    msg: 'function: readAsNumber()',
    method: 'readAsNumber',
  }, {
    msg: 'function: tryToNumber()',
    method: 'tryToNumber',
  }, {
    msg: 'function: readAsNumberEx()',
    method: 'readAsNumberEx',
  },
])('$msg', ({ method }) => {
  const testData = t_Dat[method];
  describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
    describe.each(param)('+ $msg', ({ param }) => {
      it.each(param)('+ $msg $rem', ({ values, status }) => {
        const { ops, assertQty } = status;
        let result = undefined; let isERR = false;
        try {
          result = runTestFn({ testInst: t_Mod, method }, values);
          //console.log('CHECK: => NO_ERR');
        } catch (err) {
          //console.log('CHECK: IS_ERR => '+err);
          isERR = true;
          result = err;
        } finally {
          expect.assertions(assertQty);
          expect(isERR).toBe(ops.isERR);
          if (isERR) {
            const { errType, errCode } = ops;
            expect(result instanceof errType).toBe(true);
            expect(result.code).toStrictEqual(errCode);
          } else {
            const { className, value } = ops;
            switch (typeof className) {
              case 'string' : {
                expect(result instanceof globalThis[className]).toBe(true);
                if (className !== 'HTMLElement') break;
              }
              default : {
                expect(result).toStrictEqual(value);
                break;
              }
            };
          };
        };
      });
    });
  });
});

// *** function: inRangeNum()
describe('function: inRangeNum()', () => {
  const testFn = (...args) => { return t_Mod.inRangeNum(...args); };
  it.each([
    {
      msg: 'less than "minR"',
      args: [ 0, 15, -1 ], result: false,
    }, {
      msg: 'equal to "minR"',
      args: [ 0, 15, 0 ], result: true,
    }, {
      msg: 'between "minR" and "maxR"',
      args: [ 0, 15, 7 ], result: true,
    }, {
      msg: 'equal to "maxR"',
      args: [ 0, 15, 15 ], result: true,
    }, {
      msg: 'greater than "maxR"',
      args: [ 0, 15, 25 ], result: false,
    }
  ])('value is $msg', ({ args, result }) => {
    expect(testFn(...args)).toBe(result);
  });
});
