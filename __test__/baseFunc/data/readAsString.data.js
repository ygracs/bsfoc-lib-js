// [v0.1.004-20240622]

/* module:   `baseFunc`
 * function: readAsString
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '(defaults is used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            value: undefined,
            options: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: '',
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
            options: undefined,
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: TEST_FUNC,
            options: undefined,
          },
        }, {
          msg: 'value is an array',
          rem: '',
          values: {
            value: [ 1, 2, 3 ],
            options: undefined,
          },
        }, {
          msg: 'value is an object',
          rem: '',
          values: {
            value: { data: [ 1, 2, 3 ] },
            options: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: '',
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      cases: [
        {
          msg: 'value is a string',
          rem: '',
          values: {
            value: 'abc',
            options: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 'abc',
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args but not accepted by default',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '',
          values: {
            value: true,
            options: undefined,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: {
            value: 21,
            options: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: '',
        },
        assertQty: 2,
      },
    }),
  ],
};

const optionTestDescr = {
  msg: 'run tests against "options" param',
  rem: '',
  param: [
    {
      msg: 'value is a boolean',
      param: [
        {
          msg: 'options: true',
          rem: '',
          values: {
            value: ' abc  ',
            options: true,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 'abc',
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'value is a string',
      param: [
        {
          msg: 'perform test',
          rem: '',
          values: {
            value: null,
            options: 'aBc',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 'aBc',
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'value is an object',
      param: [
        {
          msg: '[options.useTrim]: true',
          rem: '',
          values: {
            value: ' abc  ',
            options: { useTrim: true },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 'abc',
            },
            assertQty: 2,
          },
        }, {
          msg: '[options.letterCase]: "upper"',
          rem: '',
          values: {
            value: ' abc  ',
            options: { letterCase: 'upper' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: ' ABC  ',
            },
            assertQty: 2,
          },
        }, {
          msg: '[options.letterCase]: "lower"',
          rem: '',
          values: {
            value: ' ABC  ',
            options: { letterCase: 'lower' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: ' abc  ',
            },
            assertQty: 2,
          },
        }, {
          msg: '[options.boolToString]: true',
          rem: '',
          values: {
            value: true,
            options: { boolToString: true },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 'true',
            },
            assertQty: 2,
          },
        }, {
          msg: '[options.boolToString]: true',
          rem: '(when [options.boolToUpperCase] is set to "true")',
          values: {
            value: true,
            options: { boolToString: true, boolToUpperCase: true },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 'TRUE',
            },
            assertQty: 2,
          },
        }, {
          msg: '[options.boolToString]: true',
          rem: '(when [options.letterCase] is set to "upper")',
          values: {
            value: true,
            options: { boolToString: true, letterCase: 'upper' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 'TRUE',
            },
            assertQty: 2,
          },
        }, {
          msg: '[options.numberToString]: true',
          rem: '',
          values: {
            value: 214,
            options: { numberToString: true },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: '214',
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'case when [options.defValue] is used',
      param: [
        {
          msg: 'value is a string',
          rem: '',
          values: {
            value: null,
            options: { defValue: 'aBc' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 'aBc',
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a string',
          rem: '[options.useTrim] is set to "true"',
          values: {
            value: null,
            options: { defValue: ' aBc  ', useTrim: true },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 'aBc',
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a string',
          rem: '[options.letterCase] is set to "upper"',
          values: {
            value: null,
            options: { defValue: 'aBc', letterCase: 'upper' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 'ABC',
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a string',
          rem: '[options.letterCase] is set to "lower"',
          values: {
            value: null,
            options: { defValue: 'aBc', letterCase: 'lower' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 'abc',
            },
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
    optionTestDescr,
  },
  tests: [
    valueTestDescr,
    optionTestDescr,
  ],
};
