// [v0.1.005-20240622]

/* module:   `baseFunc`
 * function: isEmptyString
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
          },
        }, {
          msg: 'value is a boolean',
          rem: '',
          values: {
            value: true,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: {
            value: 21,
          },
        }, {
          msg: 'value is a string containing any non-space character',
          rem: '("false" returned)',
          values: {
            value: 'abc',
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: TEST_FUNC,
          },
        }, {
          msg: 'value is an array',
          rem: '',
          values: {
            value: [],
          },
        }, {
          msg: 'value is an object',
          rem: '',
          values: {
            value: {},
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'args are given',
      cases: [
        {
          msg: 'value is a string of zero length',
          rem: '("true" returned)',
          values: {
            value: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against special cases',
  param: [
    ...genTestCase({
      inGroup: 'ensure that a value padded with spaces is trimmed',
      cases: [
        {
          msg: 'string filled with spaces only',
          rem: '("true" returned)',
          values: {
            value: '         ',
          },
        }, {
          msg: 'case when a string not containing a non-space character',
          rem: '("true" returned)',
          values: {
            value: '  \n \r\n \n\t ',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'ensure that a value padded with spaces is trimmed',
      cases: [
        {
          msg: 'case when a string containing one or more non-space character',
          rem: '("false" returned)',
          values: {
            value: '  \n  E \r\n\n\t ',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
    resultTestDescr,
  },
  tests: [
    valueTestDescr,
    resultTestDescr,
  ],
};
