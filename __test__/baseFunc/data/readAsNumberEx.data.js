// [v0.1.003-20240622]

/* module:   `baseFunc`
 * function: readAsNumberEx
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '(defaults is used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            value: undefined,
            defs: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 0,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a string that not represent a number',
          rem: '',
          values: {
            value: 'abc',
            defs: undefined,
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: TEST_FUNC,
            defs: undefined,
          },
        }, {
          msg: 'value is an array',
          rem: '',
          values: {
            value: [],
            defs: undefined,
          },
        }, {
          msg: 'value is an object',
          rem: '',
          values: {
            value: {},
            defs: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 0,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
            defs: undefined,
          },
        }, {
          msg: 'value is a boolean',
          rem: '(value: "false")',
          values: {
            value: false,
            defs: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 0,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '(value: "true")',
          values: {
            value: true,
            defs: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 1,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      cases: [
        {
          msg: 'value is a number',
          rem: '',
          values: {
            value: 21,
            defs: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 21,
        },
        assertQty: 2,
      },
    }),
  ],
};

const defsTestDescr = {
  msg: 'run tests against "defValue" param',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          value: {
            value: 'non-valid value',
            defs: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 0,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: 'non-valid value',
            defs: null,
          },
        }, {
          msg: 'value is a boolean',
          rem: '',
          values: {
            value: 'non-valid value',
            defs: true,
          },
        }, {
          msg: 'value is a string',
          rem: '',
          values: {
            value: 'non-valid value',
            defs: 'some string',
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: 'non-valid value',
            defs: TEST_FUNC,
          },
        }, {
          msg: 'value is an array',
          rem: '',
          values: {
            value: 'non-valid value',
            defs: [],
          },
        }, {
          msg: 'value is an object',
          rem: '',
          values: {
            value: 'non-valid value',
            defs: {},
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 0,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      cases: [
        {
          msg: 'value is a number',
          rem: '',
          values: {
            value: 'non-valid value',
            defs: 21,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 21,
        },
        assertQty: 2,
      },
    }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against special cases',
  param: [
    ...genTestCase({
      inGroup: 'case when a given "value" is valid and equal to "0"',
      cases: [
        {
          msg: 'ensure that the "value" is returned but not a "defValue"',
          rem: '',
          values: {
            value: 0,
            defs: 21,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 0,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'case when a given "value" is "NaN"',
      cases: [
        {
          msg: 'ensure that the "defValue" is returned',
          rem: '',
          values: {
            value: NaN,
            defs: 21,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 21,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'case when all "value" and "defValue" is "NaN"',
      cases: [
        {
          msg: 'ensure that "0" is returned',
          rem: '',
          values: {
            value: NaN,
            defs: NaN,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 0,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'case when a given "value" must be proccessed not a "defValue"',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
            defs: 21,
          },
        }, {
          msg: 'value is string in lower case',
          rem: '(value: "null")',
          values: {
            value: 'null',
            defs: 21,
          },
        }, {
          msg: 'value is string in upper case',
          rem: '(value: "NULL")',
          values: {
            value: 'NULL',
            defs: 21,
          },
        }, {
          msg: 'value is a boolean',
          rem: '(value: "false")',
          values: {
            value: false,
            defs: 21,
          },
        }, {
          msg: 'value is string in lower case',
          rem: '(value: "false")',
          values: {
            value: 'false',
            defs: 21,
          },
        }, {
          msg: 'value is in uppercase case',
          rem: '(value: "FALSE")',
          values: {
            value: 'FALSE',
            defs: 21,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 0,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'case when a given "value" must be proccessed not a "defValue"',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '(value: "true")',
          values: {
            value: true,
            defs: 21,
          },
        }, {
          msg: 'value is string in lower case',
          rem: '(value: "true")',
          values: {
            value: 'true',
            defs: 21,
          },
        }, {
          msg: 'value is string in upper case',
          rem: '(value: "TRUE")',
          values: {
            value: 'TRUE',
            defs: 21,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 1,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'case when a given "value" must be proccessed not a "defValue"',
      cases: [
        {
          msg: 'value is a string convertable to a number',
          rem: '',
          values: {
            value: '-21',
            defs: 211,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: -21,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'ensure that a string value is trimmed',
      cases: [
        {
          msg: 'perform test',
          rem: '',
          values: {
            value: '    TRUE  ',
            defs: 21,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 1,
        },
        assertQty: 2,
      },
    }),
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
    defsTestDescr,
    resultTestDescr,
  },
  tests: [
    valueTestDescr,
    defsTestDescr,
    resultTestDescr,
  ],
};
