// [v0.1.001-20240622]

/* module:   `baseFunc`
 * function: tryToBool
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '(defaults is used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: {
            value: 21,
          },
        }, {
          msg: 'value is a string that not represents a boolean',
          rem: '',
          values: {
            value: 'not a boolean',
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: TEST_FUNC,
          },
        }, {
          msg: 'value is an array',
          rem: '',
          values: {
            value: [],
          },
        }, {
          msg: 'value is an object',
          rem: '',
          values: {
            value: {},
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '(value is "true")',
          values: {
            value: true,
          },
        }, {
          msg: 'value is a string that represents a boolean',
          rem: '(value is "true")',
          values: {
            value: 'true',
          },
        }, {
          msg: 'value is a string that represents a boolean',
          rem: '(value is "TRUE")',
          values: {
            value: 'TRUE',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against special cases',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'case when a given "value" is valid and equal to "false"',
      cases: [
        {
          msg: 'ensure that the "value" is returned but not a "defValue"',
          rem: '(boolean value)',
          values: {
            value: false,
          },
        }, {
          msg: 'ensure that the "value" is returned but not a "defValue"',
          rem: '(string in lower case)',
          values: {
            value: `false`,
          },
        }, {
          msg: 'ensure that the "value" is returned but not a "defValue"',
          rem: '(string in upper case)',
          values: {
            value: `FALSE`,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'ensure that a string value is trimmed',
      cases: [
        {
          msg: 'value is equal to "false"',
          rem: '',
          values: {
            value: '  \n  false  \n  ',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
    resultTestDescr,
  },
  tests: [
    valueTestDescr,
    resultTestDescr,
  ],
};
