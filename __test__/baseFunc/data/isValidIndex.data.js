// [v0.1.004-20240622]

/* module:   `baseFunc`
 * function: isValidIndex
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
          },
        }, {
          msg: 'value is a boolean',
          rem: '',
          values: {
            value: true,
          },
        }, {
          msg: 'value is a number',
          rem: '(a negative one)',
          values: {
            value: -21,
          },
        }, {
          msg: 'value is a number',
          rem: '(a positive but not integer)',
          values: {
            value: 21.175,
          },
        }, {
          msg: 'value is a string',
          rem: '(not convertable to a number)',
          values: {
            value: 'abc',
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: TEST_FUNC,
          },
        }, {
          msg: 'value is an array',
          rem: '',
          values: {
            value: [],
          },
        }, {
          msg: 'value is an object',
          rem: '',
          values: {
            value: {},
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      cases: [
        {
          msg: 'value is a number',
          rem: '(a positive one)',
          values: {
            value: 15,
          },
        }, {
          msg: 'value is a string',
          rem: '(convertable to a positive integer number)',
          values: {
            value: '21175',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against special cases',
  param: [
    ...genTestCase({
      inGroup: 'ensure that a value is not acceptable',
      cases: [
        {
          msg: 'value is an empty string',
          rem: '',
          values: {
            value: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'ensure that a value padded with spaces is accepted',
      cases: [
        {
          msg: 'value is a string',
          rem: '(convertable to a positive integer number)',
          values: [ '  1521 ' ],
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
    resultTestDescr,
  },
  tests: [
    valueTestDescr,
    resultTestDescr,
  ],
};
