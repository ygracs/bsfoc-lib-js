// [v0.1.003-20240622]

/* module:   `baseFunc`
 * function: valueToIDString
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '(options: default)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            value: undefined,
            options: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
            options: undefined,
          },
        }, {
          msg: 'value is a boolean',
          rem: '',
          values: {
            value: true,
            options: undefined,
          },
        }, {
          msg: 'value is a number',
          rem: '(a negative one)',
          values: {
            value: -21,
            options: undefined,
          },
        }, {
          msg: 'value is a number',
          rem: '(a positive but not integer)',
          values: {
            value: 21.75,
            options: undefined,
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: TEST_FUNC,
            options: undefined,
          },
        }, {
          msg: 'value is an array',
          rem: '',
          values: {
            value: [],
            options: undefined,
          },
        }, {
          msg: 'value is an object',
          rem: '',
          values: {
            value: {},
            options: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      cases: [
        {
          msg: 'value is a number',
          rem: '(a positive integer one)',
          values: {
            value: 15,
            options: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: '15',
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      cases: [
        {
          msg: 'value is a string that not represents a number',
          rem: '',
          values: {
            value: 'abc',
            options: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 'abc',
        },
        assertQty: 2,
      },
    }),
  ],
};

const optionTestDescr = {
  msg: 'run tests aganst "options" param',
  rem: '(a "value" param is an empty string)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given (defaults will be used)',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "null" retuned)',
          values: {
            value: '',
            options: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a "boolean" args are given',
      cases: [
        {
          msg: 'options: "false"',
          rem: 'a "value" param will be accepted',
          values: {
            value: '',
            options: false,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: '',
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a "boolean" args are given',
      cases: [
        {
          msg: 'options: "true"',
          rem: 'a "value" param will be rejected',
          values: {
            value: '',
            options: true,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'an "object" args are given',
      cases: [
        {
          msg: 'options: an empty object',
          rem: '(defaults will be used)',
          values: {
            value: '',
            options: {},
          },
        }, {
          msg: 'options: { onlyNES: "true" }',
          rem: 'a "value" param will be rejected',
          values: {
            value: '',
            options: { onlyNES: true },
          },
        }, {
          msg: 'options: { ignoreNumbers: "true" }',
          rem: 'a "value" param will be rejected',
          values: {
            value: '123',
            options: { ignoreNumbers: true },
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'an "object" args are given',
      cases: [
        {
          msg: 'options: { onlyNES: "false" }',
          rem: 'a "value" param will be accepted',
          values: {
            value: '',
            options: { onlyNES: false },
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: '',
        },
        assertQty: 2,
      },
    }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against special cases',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'ensure that a string is trimmed',
      cases: [
        {
          msg: 'any containing at least one non-space character',
          rem: '',
          values: {
            value: '  abc ',
            options: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 'abc',
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'ensure that a string is trimmed',
      cases: [
        {
          msg: 'any containing only a space characters',
          rem: '',
          values: {
            value: '  \t \t ',
            options: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
    optionTestDescr,
    resultTestDescr,
  },
  tests: [
    valueTestDescr,
    optionTestDescr,
    resultTestDescr,
  ],
};
