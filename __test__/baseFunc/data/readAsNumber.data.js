// [v0.1.003-20240622]

/* module:   `baseFunc`
 * function: readAsNumber
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests aganst "value" param',
  rem: '(defaults is used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            value: undefined,
            defs: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 0,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
            defs: undefined,
          },
        }, {
          msg: 'value is a boolean',
          rem: '',
          values: {
            value: true,
            defs: undefined,
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: TEST_FUNC,
            defs: undefined,
          },
        }, {
          msg: 'value is an array',
          rem: '',
          values: {
            value: [],
            defs: undefined,
          },
        }, {
          msg: 'value is an object',
          rem: '',
          values: {
            value: {},
            defs: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 0,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      cases: [
        {
          msg: 'value is a number',
          rem: '',
          values: {
            value: 21,
            defs: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 21,
        },
        assertQty: 2,
      },
    }),
  ],
};

const defsTestDescr = {
  msg: 'run tests aganst "defValue" param',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults will be used)',
          values: {
            value: 'some non-valid value',
            defs: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 0,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'perform ops',
          rem: '(value will be ignored)',
          values: {
            value: 'some non-valid value',
            defs: 'non-valid defs',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 0,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      cases: [
        {
          msg: 'value is a number',
          rem: '',
          values: {
            value: 'some non-valid value',
            defs: 21,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 21,
        },
        assertQty: 2,
      },
    }),
  ],
};

const resultTestDescr = {
  msg: 'run tests aganst special cases',
  param: [
    {
      msg: 'case when a given "value" is valid and equal to "0"',
      param: [
        {
          msg: 'ensure that the "value" is returned but not a "defValue"',
          rem: '',
          values: {
            value: 0,
            defs: 21,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'case when a given "value" is "NaN"',
      param: [
        {
          msg: 'ensure that the "defValue" is returned',
          rem: '',
          values: {
            value: NaN,
            defs: 21,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 21,
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'case when all "value" and "defValue" is "NaN"',
      param: [
        {
          msg: 'ensure that "0" is returned',
          rem: '',
          values: {
            value: NaN,
            defs: NaN,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'case when a given "value" is a string',
      param: [
        {
          msg: 'value is a string',
          rem: '(any not represents some number)',
          values: {
            value: 'abc',
            defs: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a string',
          rem: '(any represents some number)',
          values: {
            value: '   -121  ',
            defs: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: -121,
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is an empty string',
          rem: '("defValue" will be returned)',
          values: {
            value: '    ',
            defs: 64,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 64,
            },
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    valueTestDescr,
    defsTestDescr,
    resultTestDescr,
  ],
  tests: [
    valueTestDescr,
    defsTestDescr,
    resultTestDescr,
  ],
};
