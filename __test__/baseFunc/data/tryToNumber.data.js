// [v0.1.001-20240622]

/* module:   `baseFunc`
 * function: tryToNumber
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests aganst "value" param',
  rem: '(defaults is used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
          },
        }, {
          msg: 'value is a boolean',
          rem: '',
          values: {
            value: true,
          },
        }, {
          msg: 'value is a "NaN"',
          rem: '',
          values: {
            value: NaN,
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: TEST_FUNC,
          },
        }, {
          msg: 'value is an array',
          rem: '',
          values: {
            value: [],
          },
        }, {
          msg: 'value is an object',
          rem: '',
          values: {
            value: {},
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      cases: [
        {
          msg: 'value is a number',
          rem: '',
          values: {
            value: 21,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 21,
        },
        assertQty: 2,
      },
    }),
  ],
};

const resultTestDescr = {
  msg: 'run tests aganst special cases',
  param: [
    ...genTestCase({
      inGroup: 'case when a given "value" is a string',
      cases: [
        {
          msg: 'value is a string',
          rem: '(any not represents some number)',
          values: {
            value: 'abc',
          },
        }, {
          msg: 'value is an empty string',
          rem: '("null" will be returned)',
          values: {
            value: '   ',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'case when a given "value" is a string',
      cases: [
        {
          msg: 'value is a string',
          rem: '(any represents some number)',
          values: {
            value: '  -121 ',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: -121,
        },
        assertQty: 2,
      },
    }),
  ],
};

module.exports = {
  descr: [
    valueTestDescr,
    resultTestDescr,
  ],
  tests: [
    valueTestDescr,
    resultTestDescr,
  ],
};
