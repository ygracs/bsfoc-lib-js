// [v0.1.004-20240622]

/* module:   `baseFunc`
 * function: isNullOrUndef
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests aganst "value" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'args are given',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '',
          values: {
            value: true,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: {
            value: 21,
          },
        }, {
          msg: 'value is a string',
          rem: '',
          values: {
            value: 'abc',
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: TEST_FUNC,
          },
        }, {
          msg: 'value is an array',
          rem: '',
          values: {
            value: [],
          },
        }, {
          msg: 'value is an object',
          rem: '',
          values: {
            value: {},
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
  },
  tests: [
    valueTestDescr,
  ],
};
