// [v0.1.009-20240622]

/* module:   `baseFunc`
 * function: ---
 */

module.exports = {
  valueToIndex: require('./valueToIndex.data.js'),
  isValidIndex: require('./isValidIndex.data.js'),
  valueToIDString: require('./valueToIDString.data.js'),
  isValidId: require('./isValidId.data.js'),
  isEmptyString: require('./isEmptyString.data.js'),
  isEmptyOrNotString: require('./isEmptyOrNotString.data.js'),
  isNotEmptyString: require('./isNotEmptyString.data.js'),
  isNullOrUndef: require('./isNullOrUndef.data.js'),
  tryToBool: require('./tryToBool.data.js'),
  readAsBool: require('./readAsBool.data.js'),
  readAsBoolEx: require('./readAsBoolEx.data.js'),
  tryToNumber: require('./tryToNumber.data.js'),
  readAsNumber: require('./readAsNumber.data.js'),
  readAsNumberEx: require('./readAsNumberEx.data.js'),
  readAsString: require('./readAsString.data.js'),
  tryNotNullish: require('./tryNotNullish.data.js'),
};
