// [v0.1.005-20240622]

/* module:   `baseFunc`
 * function: readAsBool
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '(defaults is used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            value: undefined,
            defs: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
            defs: undefined,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: {
            value: 21,
            defs: undefined,
          },
        }, {
          msg: 'value is a string that not represents a boolean',
          rem: '',
          values: {
            value: 'not a boolean',
            defs: undefined,
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: TEST_FUNC,
            defs: undefined,
          },
        }, {
          msg: 'value is an array',
          rem: '',
          values: {
            value: [],
            defs: undefined,
          },
        }, {
          msg: 'value is an object',
          rem: '',
          values: {
            value: {},
            defs: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '(value is "true")',
          values: {
            value: true,
            defs: undefined,
          },
        }, {
          msg: 'value is a string that represents a boolean',
          rem: '(value is "true")',
          values: {
            value: 'true',
            defs: undefined,
          },
        }, {
          msg: 'value is a string that represents a boolean',
          rem: '(value is "TRUE")',
          values: {
            value: 'true',
            defs: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
  ],
};

const defsTestDescr = {
  msg: 'run tests against "defValue" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults will be used)',
          values: {
            value: 'some non-valid value',
            defs: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(value will be ignored)',
          values: {
            value: 'non-valid value',
            defs: null,
          },
        }, {
          msg: 'value is a number',
          rem: '(value will be ignored)',
          values: {
            value: 'non-valid value',
            defs: 21,
          },
        }, {
          msg: 'value is a string',
          rem: '(value will be ignored)',
          values: {
            value: 'non-valid value',
            defs: 'non-valid defs',
          },
        }, {
          msg: 'value is a function',
          rem: '(value will be ignored)',
          values: {
            value: 'non-valid value',
            defs: TEST_FUNC,
          },
        }, {
          msg: 'value is an array',
          rem: '(value will be ignored)',
          values: {
            value: 'non-valid value',
            defs: [],
          },
        }, {
          msg: 'value is an object',
          rem: '(value will be ignored)',
          values: {
            value: 'non-valid value',
            defs: {},
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '',
          values: {
            value: 'non-valid value',
            defs: true,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against special cases',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'case when a given "value" is valid and equal to "false"',
      cases: [
        {
          msg: 'ensure that the "value" is returned but not a "defValue"',
          rem: '(boolean value)',
          values: {
            value: false,
            defs: true,
          },
        }, {
          msg: 'ensure that the "value" is returned but not a "defValue"',
          rem: '(string in lower case)',
          values: {
            value: `false`,
            defs: true,
          },
        }, {
          msg: 'ensure that the "value" is returned but not a "defValue"',
          rem: '(string in upper case)',
          values: {
            value: `FALSE`,
            defs: true,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'ensure that a string value is trimmed',
      cases: [
        {
          msg: 'value is equal to "false"',
          rem: '',
          values: {
            value: '  \n  false  \n  ',
            defs: true,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
    defsTestDescr,
    resultTestDescr,
  },
  tests: [
    valueTestDescr,
    defsTestDescr,
    resultTestDescr,
  ],
};
