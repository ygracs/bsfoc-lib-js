// [v0.1.002-20230619]

/* module:   `baseFunc`
 * function: tryNotNullish
 */

const TEST_FUNC = (value) => { return value; };

const resultTestDescr = {
  msg: 'run tests aganst result value',
  rem: '',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'the method should return "null"',
          rem: '',
          values: [],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: null,
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'some args are given',
      param: [
        {
          msg: 'there are only elements which is "undefined" or "null"',
          rem: '("null" is returned)',
          values: [ undefined, null, undefined ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: null,
            },
            assertQty: 2,
          },
        }, {
          msg: 'there is some element except "undefined" or "null"',
          rem: '(the first matching element is returned)',
          values: [ undefined, null, 1, undefined ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 1,
            },
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    resultTestDescr,
  ],
};
