// [v0.1.006-20230905]

const t_Mod = require('#lib/baseFunc.js');

const t_Dat = require('./data/index.data.js');

const { runTestFn } = require('../test-hfunc.js');

// *** function: readAsString()
describe.each([
  {
    msg: 'function: readAsString()',
    method: 'readAsString',
  },
])('$msg', ({ method }) => {
  const testData = t_Dat[method];
  describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
    describe.each(param)('+ $msg', ({ param }) => {
      it.each(param)('$msg $rem', ({ values, status }) => {
        const { ops, assertQty } = status;
        let result = undefined; let isERR = false;
        try {
          result = runTestFn({ testInst: t_Mod, method }, values);
          //console.log('CHECK: => NO_ERR');
        } catch (err) {
          //console.log('CHECK: IS_ERR => '+err);
          isERR = true;
          result = err;
        } finally {
          expect.assertions(assertQty);
          expect(isERR).toBe(ops.isERR);
          if (isERR) {
            const { errType, errCode } = ops;
            expect(result instanceof errType).toBe(true);
            expect(result.code).toStrictEqual(errCode);
          } else {
            const { className, value } = ops;
            switch (typeof className) {
              case 'string' : {
                expect(result instanceof globalThis[className]).toBe(true);
                if (className !== 'HTMLElement') break;
              }
              default : {
                expect(result).toStrictEqual(value);
                break;
              }
            };
          };
        };
      });
    });
  });
});
