// [v0.1.007-20230910]

const t_Mod = require("#lib/baseObj.js");

const t_Dat = require('./data/index.data.js');

const { runTestFn } = require('../test-hfunc.js');

const { readAsListE } = t_Mod;

const { isNotEmptyString, readAsString } = require("#lib/baseFunc.js");

const {
  //samples,
  data_readAsListE
} = require("./data/readAsListE.data.js");

// *** function: readAsListE()
describe('function: readAsListE()', () => {
  const testFn = (...args) => { return readAsListE(...args); };
  const testData = data_readAsListE.baseTest;
  describe('3. run enhanced (manipulators are used)', () => {
    const testResults = data_readAsListE.baseTest.options_object;
    describe.each([
      {
        msg: 'pickup strings',
        manip: {
          __user_item_getter: (item) => {
            return readAsString(item, true);
          },
          __user_item_checker: (item, opt) => {
            return opt ? true : isNotEmptyString(item)
          },
        },
        result: [
          {
            msg: 'keep empties',
            param: { keepEmpty: true },
            samples: testResults.use_manip.samples,
            result: testResults.use_manip.keep_empty_true,
          }, {
            msg: 'drop empties',
            param: { keepEmpty: false },
            samples: testResults.use_manip.samples,
            result: testResults.use_manip.keep_empty_false,
          },
        ],
      },
    ])('$msg', ({ manip, result }) => {
      const options = testResults.base_options;
      it.each(result)('$msg', ({ param, samples, result }) => {
        options.keepEmpty = param.keepEmpty;
        options.unfoldArrayrr = false;
        options.userFn = {};
        options.userFn.checkItemFn = manip.__user_item_checker;
        options.userFn.readItemFn = manip.__user_item_getter;
        expect(testFn(options, ...samples)).toStrictEqual(result);
      });
    });
  });
});

describe.each([
  {
    msg: 'function: readAsListE()',
    method: 'readAsListE',
  },
])('$msg', ({ method }) => {
  const testData = t_Dat[method];
  describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
    describe.each(param)('+ $msg', ({ param }) => {
      it.each(param)('+ $msg $rem', ({ values, status }) => {
        const { ops, assertQty } = status;
        let result = undefined; let isERR = false;
        try {
          result = runTestFn({ testInst: t_Mod, method }, values);
          //console.log('CHECK: => NO_ERR');
        } catch (err) {
          //console.log('CHECK: IS_ERR => '+err);
          isERR = true;
          result = err;
        } finally {
          expect.assertions(assertQty);
          expect(isERR).toBe(ops.isERR);
          if (isERR) {
            const { errType, errCode } = ops;
            expect(result instanceof errType).toBe(true);
            expect(result.code).toStrictEqual(errCode);
          } else {
            const { className, value } = ops;
            switch (typeof className) {
              case 'string' : {
                expect(result instanceof globalThis[className]).toBe(true);
                if (className !== 'HTMLElement') break;
              }
              default : {
                expect(result).toStrictEqual(value);
                break;
              }
            };
          };
        };
      });
    });
  });
});
