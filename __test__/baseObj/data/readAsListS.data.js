// [v0.1.005-20230908]

/* module:   `baseObj`
 * function: readAsListS
 */

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '(defaults is used)',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: [ undefined, undefined ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'some args are given',
      param: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: [ undefined, null ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a boolean',
          rem: '',
          values: [ undefined, true ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: [ undefined, 21 ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a string',
          rem: '',
          values: [ undefined, 'abc' ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'abc' ],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: [ undefined, TEST_FUNC ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is an array',
          rem: '',
          values: [ undefined, [] ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is an object',
          rem: '',
          values: [ undefined, [] ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'some args are given (a few)',
      param: [
        {
          msg: 'all values are a strings',
          rem: '',
          values: [ undefined, 'a1', 'a2', 'a3'  ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', 'a2', 'a3' ],
            },
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const optionTestDescr = {
  msg: 'run tests against "options" param',
  rem: '',
  param: [
    {
      msg: 'options: boolean',
      param: [
        {
          msg: 'value is "false"',
          rem: '(must be treat as "useTrim")',
          values: [
            false,
            'a1', ' a2 ', 'a3',
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', ' a2 ', 'a3' ],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is "true"',
          rem: '(must be treat as "useTrim")',
          values: [
            true,
            'a1', ' a2 ', 'a3',
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', 'a2', 'a3' ],
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'options: a string',
      param: [
        {
          msg: 'value must be treat as "defValue"',
          rem: '',
          values: [
            '__opt__',
            'a1', ' a2 ', null, 31,
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', ' a2 ', '__opt__', '__opt__' ],
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'options: object',
      param: [
        {
          msg: 'an empty object is passed',
          rem: '(defaults will be used)',
          values: [
            {},
            'a1', null, ' a2 ', true, 'a3', 127,
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', ' a2 ', 'a3' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"defValue" value is defined',
          rem: '',
          values: [
            { defValue: '__opt__' },
            'a1', null, ' a2 ', true, 'a3', 127,
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', '__opt__', ' a2 ', '__opt__', 'a3', '__opt__' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"useTrim" value is "false"',
          rem: '',
          values: [
            { useTrim: false },
            'a1', ' a2 ', 'a3',
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', ' a2 ', 'a3' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"useTrim" value is "true"',
          rem: '',
          values: [
            { useTrim: true },
            'a1', ' a2 ', 'a3',
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', 'a2', 'a3' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"numberToString" value is "false"',
          rem: '',
          values: [
            { numberToString: false },
            'a1', ' a2 ', 'a3', 15,
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', ' a2 ', 'a3' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"numberToString" value is "true"',
          rem: '',
          values: [
            { numberToString: true },
            'a1', ' a2 ', 'a3', 25,
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', ' a2 ', 'a3', '25' ],
            },
            assertQty: 2,
          },
          msg: '"boolToString" value is "false"',
          rem: '',
          values: [
            { boolToString: false },
            'a1', ' a2 ', 'a3', false,
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', ' a2 ', 'a3' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"boolToString" value is "true"',
          rem: '',
          values: [
            { boolToString: true },
            'a1', ' a2 ', 'a3', true,
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', ' a2 ', 'a3', 'true' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"keepEmpty" value is "false"',
          rem: '',
          values: [
            { keepEmpty: false },
            'a1', null, ' a2 ', true, 'a3', 127,
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', ' a2 ', 'a3' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"keepEmpty" value is "true"',
          rem: '',
          values: [
            { keepEmpty: true },
            'a1', null, ' a2 ', true, 'a3', 127,
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', '', ' a2 ', '', 'a3', '' ],
            },
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const unfoldTestDescr = {
  msg: 'run tests against "options" param',
  rem: '(case with "unfoldArray" settings)',
  param: [
    {
      msg: 'no settings are specified',
      param: [
        {
          msg: 'perform ops',
          rem: '(defaults will be used)',
          values: [
            { defValue: '__opt__' },
            'a1', [ 'b1', [ 'c1', [ 'd1' ] ] ], ' a2 ',
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', '__opt__', ' a2 ' ],
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'settings are specified',
      param: [
        {
          msg: '"unfoldArray" value is "false"',
          rem: '(use default "unfoldLevel" value)',
          values: [
            {
              defValue: '__opt__',
              unfoldArray: false,
            },
            'a1', [ 'b1', [ 'c1', [ 'd1' ] ] ], ' a2 ',
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', '__opt__', ' a2 ' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"unfoldArray" value is "true"',
          rem: '(use default "unfoldLevel" value)',
          values: [
            {
              defValue: '__opt__',
              unfoldArray: true,
            },
            'a1', [ 'b1', [ 'c1', [ 'd1' ] ] ], ' a2 ',
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', 'b1', '__opt__', ' a2 ' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"unfoldArray" value is "true"',
          rem: '(use special "unfoldLevel" value of "-1")',
          values: [
            {
              defValue: '__opt__',
              unfoldArray: true,
              unfoldLevel: -1,
            },
            'a1', [ 'b1', [ 'c1', [ 'd1' ] ] ], ' a2 ',
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', 'b1', 'c1', 'd1', ' a2 ' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"unfoldArray" value is "true"',
          rem: '("unfoldLevel" value equal to "0")',
          values: [
            {
              defValue: '__opt__',
              unfoldArray: true,
              unfoldLevel: 0,
            },
            'a1', [ 'b1', [ 'c1', [ 'd1' ] ] ], ' a2 ',
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', '__opt__', ' a2 ' ],
            },
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const resultTestDescr = {
  msg: 'run tests against result value',
  rem: '',
  param: [
    {
      msg: 'case upon "defValue" settings',
      param: [
        {
          msg: '"defValue" value is a number',
          rem: '',
          values: [
            { defValue: -1 },
            'a1', null, ' a2 ', true, 'a3', 127,
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', ' a2 ', 'a3' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"defValue" value is a boolean',
          rem: '',
          values: [
            { defValue: false },
            'a1', null, ' a2 ', true, 'a3', 127,
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', ' a2 ', 'a3' ],
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'case upon "unfoldLevel" settings',
      param: [
        {
          msg: '"unfoldLevel" value greater than "0"',
          rem: '(but less than max level of nesting elements)',
          values: [
            {
              defValue: '__opt__',
              unfoldArray: true,
              unfoldLevel: 2,
            },
            'a1', [ 'b1', [ 'c1', [ 'd1' ] ] ], ' a2 ',
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', 'b1', 'c1', '__opt__', ' a2 ' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"unfoldLevel" value equal to max level of nesting elements',
          rem: '',
          values: [
            {
              defValue: '__opt__',
              unfoldArray: true,
              unfoldLevel: 3,
            },
            'a1', [ 'b1', [ 'c1', [ 'd1' ] ] ], ' a2 ',
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', 'b1', 'c1', 'd1', ' a2 ' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"unfoldLevel" value greater than max level of nesting elements',
          rem: '',
          values: [
            {
              defValue: '__opt__',
              unfoldArray: true,
              unfoldLevel: 5,
            },
            'a1', [ 'b1', [ 'c1', [ 'd1' ] ] ], ' a2 ',
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', 'b1', 'c1', 'd1', ' a2 ' ],
            },
            assertQty: 2,
          },
        }, {
          msg: '"unfoldLevel" value less than "0"',
          rem: '(but not a "-1")',
          values: [
            {
              defValue: '__opt__',
              unfoldArray: true,
              unfoldLevel: -12,
            },
            'a1', [ 'b1', [ 'c1', [ 'd1' ] ] ], ' a2 ',
          ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 'a1', '__opt__', ' a2 ' ],
            },
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    valueTestDescr,
    optionTestDescr,
    unfoldTestDescr,
    resultTestDescr,
  ],
};
