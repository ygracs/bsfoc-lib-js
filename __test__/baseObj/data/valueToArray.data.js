// [v0.1.001-20220820]

/* module:   `baseObj`
 * function: valueToArray
 */

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests aganst "value" param',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'undefined value is passed',
          value: undefined,
          status: {
            ops: [],
          },
        },
      ],
    }, {
      msg: 'args are given (an empty array is returned)',
      param: [
        {
          msg: 'value is a "null"',
          value: null,
          status: {
            ops: [],
          },
        },
      ],
    }, {
      msg: 'args are given (value is transformed into an array)',
      param: [
        {
          msg: 'value is a boolean',
          value: true,
          status: {
            ops: [ true ],
          },
        }, {
          msg: 'value is a number',
          value: 21,
          status: {
            ops: [ 21 ],
          },
        }, {
          msg: 'value is a string',
          value: 'abc',
          status: {
            ops: [ 'abc' ],
          },
        }, {
          msg: 'value is a function',
          value: TEST_FUNC,
          status: {
            ops: [ TEST_FUNC ],
          },
        }, {
          msg: 'value is an object',
          value: {},
          status: {
            ops: [ {} ],
          },
        }, {
          msg: 'value is an instance of a "Set"',
          value: new Set([ 1, 2, 3 ]),
          status: {
            ops: [ 1, 2, 3 ],
          },
        }, {
          msg: 'value is an instance of a "Map"',
          value: new Map([
            [ 'first', 1 ],
            [ 'second', 2 ],
          ]),
          status: {
            ops: [
              [ 'first', 1 ],
              [ 'second', 2 ],
            ],
          },
        }, {
          msg: 'value is an instance of a "Date"',
          value: new Date('2022-06-22T22:05:00Z'),
          status: {
            ops: [ new Date('2022-06-22T22:05:00Z') ],
          },
        },
      ],
    }, {
      msg: 'args are given (value is returned "AS IS")',
      param: [
        {
          msg: 'value is an array',
          value: [ 1, 2, 3 ],
          status: {
            ops: [ 1, 2, 3 ],
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    valueTestDescr,
  ],
};
