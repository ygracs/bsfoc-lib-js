// [v0.1.001-20230725]

/* module:   `baseObj`
 * function: valueToEntries
 */

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '(defaults is used)',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            value: undefined,
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: TEST_FUNC,
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is an array',
          rem: '[*]',
          values: {
            value: [ 1, 2, 3 ],
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is an object',
          rem: '[*]',
          values: {
            value: { data: [ 1, 2, 3 ] },
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        {
          msg: 'value is a string',
          rem: '',
          values: {
            value: 'abc',
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ [ '', 'abc' ] ],
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'a valid args but not accepted by default',
      param: [
        {
          msg: 'value is a boolean',
          rem: '',
          values: {
            value: true,
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: {
            value: 21,
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
  },
  tests: [
    valueTestDescr,
  ],
};
