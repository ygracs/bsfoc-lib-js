// [v0.1.003-20230903]

/* module:   `baseObj`
 * function: readAsListE
 */

const TEST_FUNC = (value) => { return value; };

const samples_s = [
  'item_10', 'item_11', 'item_12', 'item_13', 'item_14',
  'item_15', 'item_16', 'item_17', 'item_18', 'item_19',
]; // test list of a samples

const samples_n = [
  210, 211, 212, 213, 214, 215, 216, 217, 218, 219,
]; // test list of a samples

const samples_o = [
  { index: 0, name: 'item_310' }, { index: 1, name: 'item_311' },
  { index: 2, name: 'item_312' }, { index: 3, name: 'item_313' },
  { index: 4, name: 'item_314' }, { index: 5, name: 'item_315' },
  { index: 6, name: 'item_316' }, { index: 7, name: 'item_317' },
  { index: 8, name: 'item_318' }, { index: 9, name: 'item_319' },
]; // test list of a samples

const samples_ao = [
  [ { index: 0, name: 'item_410' }, { index: 1, name: 'item_411' } ],
  [ { index: 2, name: 'item_412' }, { index: 3, name: 'item_413' } ],
  [ { index: 4, name: 'item_414' }, { index: 5, name: 'item_415' } ],
  [ { index: 6, name: 'item_416' }, { index: 7, name: 'item_417' } ],
  [ { index: 8, name: 'item_418' }, { index: 9, name: 'item_419' } ],
]; // test list of a samples

const items = [ 1, 'a', ' b  ', false ]; // test list of a samples
const val = (value) => {
  switch (value) {
    case 1:
      return { a: 'key_a', b: 'key_b' };
    case 2:
      return [ 'key_a', true, 'key_b', 1 ];
    case 3:
      return [ 'key_c', false, 'key_d', 2 ];
    case 4:
      return [ val(1), val(2), val(3) ];
    case 5:
      return [ val(1), 'key_c', samples_o[5], ...val(3), ...samples_ao[3] ];
    default:
      return;
  };
}; // return test list of a samples

// *** function: readAsListE()
const data_readAsListE = {
  baseTest: {
    options_object: {
      get base_options(){
        return {
          trim: true, num_to_str: true, bool_to_str: true,
          defValue: '',
        };
      },
      /*is_empty: [
        {
          msg: 'an empty object', options: {},
          samples: [ ...items, samples_o[1], samples_ao[2] ],
          result: [ samples_o[1] ],
        },
      ],*/
      /*is_not_empty: [
        {
          msg: 'unfold_arr: "false"',
          result: [
            {
              msg: 'keep_empty: "false"',
              param: {
                unfold_arr: false, keep_empty: false,
              },
              samples: [ ...items, val(1), val(2) ],
              result: [ val(1) ],
            }, {
              msg: 'keep_empty: "true"',
              param: {
                unfold_arr: false, keep_empty: true,
              },
              samples: [ ...items, val(1), val(2) ],
              result: [
                null, null, null, null, // * items *
                val(1), null,
              ],
            },
          ],
        }, {
          msg: 'unfold_arr: "true"',
          result: [
            {
              msg: 'keep_empty: "false"',
              param: {
                unfold_arr: true, keep_empty: false,
              },
              samples: [ ...items, val(1), [ val(2), val(4) ] ],
              result: [ val(1) ],
            }, {
              msg: 'keep_empty: "true"',
              param: {
                unfold_arr: true, keep_empty: true,
              },
              samples: [ ...items, val(1), [ val(2), val(4) ] ],
              result: [
                null, null, null, null, // * items *
                val(1),  null,  null,
              ],
            },
          ],
        },
      ],*/
      use_manip: {
        samples: [ ...items, val(1), ...val(5), ...val(4) ],
        keep_empty_true: [
          '', 'a', 'b', '', '', '',
          'key_c', '', 'key_c', '', 'key_d', '',
          '', '', '', '', '',
        ],
        keep_empty_false: [
          'a', 'b', 'key_c', 'key_c', 'key_d',
        ],
      },
    },
  },
};

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '(defaults is used)',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            opt: undefined,
            value: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'some args are given (only single element is passed)',
      param: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            opt: undefined,
            value: null,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a boolean',
          rem: '',
          values: {
            opt: undefined,
            value: true,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: {
            opt: undefined,
            value: 127,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a "string',
          rem: '',
          values: {
            opt: undefined,
            value: 'some string',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            opt: undefined,
            value: TEST_FUNC,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is an array',
          rem: '(no elements include)',
          values: {
            opt: undefined,
            value: [],
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is an object',
          rem: '(empty object)',
          values: {
            opt: undefined,
            value: {},
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ {} ],
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'some args are given (few elements is passed)',
      param: [
        {
          msg: 'all are valid one',
          rem: '',
          values: {
            opt: undefined,
            value_1: { name: 'value-1' },
            value_2: { name: 'value-2' },
            value_3: { name: 'value-3' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [
                { name: 'value-1' },
                { name: 'value-2' },
                { name: 'value-3' },
              ],
            },
            assertQty: 2,
          },
        }, {
          msg: 'some are non-valid',
          rem: '',
          values: {
            opt: undefined,
            value_1: null,
            value_2: 'value-2',
            value_3: { name: 'value-3' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ { name: 'value-3' } ],
            },
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const optionsTestDescr = {
  msg: 'run tests against "options" param',
  rem: '',
  param: [
    {
      msg: 'options is boolean',
      param: [
        {
          msg: 'value is "true"',
          rem: '(empty elements must be kept)',
          values: {
            opt: true,
            value_1: null,
            value_2: 'value-2',
            value_3: { name: 'value-3' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [
                null, /* value_1 */
                null, /* value_2 */
                { name: 'value-3' }, /* value_3 */
              ],
            },
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'options is an object',
      param: [
        {
          msg: '[options.keepEmpty] is "false"',
          rem: '(empty elements must be skipped)',
          values: {
            opt: {
              keepEmpty: false,
            },
            value_1: null,
            value_2: 'value-2',
            value_3: { name: 'value-3' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [
                /* value_1 must be skipped */
                /* value_2 must be skipped */
                { name: 'value-3' }, /* value_3 is kept */
              ],
            },
            assertQty: 2,
          },
        }, {
          msg: '[options.keepEmpty] is "true"',
          rem: '(empty elements must be kept)',
          values: {
            opt: {
              keepEmpty: true,
            },
            value_1: null,
            value_2: 'value-2',
            value_3: { name: 'value-3' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [
                null, /* value_1 */
                null, /* value_2 */
                { name: 'value-3' }, /* value_3 */
              ],
            },
            assertQty: 2,
          },
        }, {
          msg: '[options.unfoldArray] is not set',
          rem: '(in case of [options.keepEmpty] is "true")',
          values: {
            opt: {
              keepEmpty: true,
              unfoldArray: undefined,
            },
            value_1: [ { name: 'value-1' } ],
            value_2: [ { name: 'value-2-1' }, [ { name: 'value-2-2' } ] ],
            value_3: { name: 'value-3' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [
                null, /* value_1 */
                null, /* value_2 */
                { name: 'value-3' }, /* value_3 */
              ],
            },
            assertQty: 2,
          },
        }, {
          msg: '[options.unfoldArray] is "true"',
          rem: '(in case of [options.keepEmpty] is "true")',
          values: {
            opt: {
              keepEmpty: true,
              unfoldArray: true,
            },
            value_1: [ { name: 'value-1' } ],
            value_2: [ { name: 'value-2-1' }, [ { name: 'value-2-2' } ] ],
            value_3: { name: 'value-3' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [
                { name: 'value-1' }, /* value_1 */
                { name: 'value-2-1' }, null, /* value_2 */
                { name: 'value-3' }, /* value_3 */
              ],
            },
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

module.exports = {
  samples: {
    samples_s: samples_s,
    samples_n: samples_n,
    samples_o: samples_o,
    samples_ao: samples_ao,
  },
  data_readAsListE: data_readAsListE,
  descr: {
    valueTestDescr,
    optionsTestDescr,
  },
  tests: [
    valueTestDescr,
    optionsTestDescr,
  ],
};
