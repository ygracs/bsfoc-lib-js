// [v0.1.006-20240623]

/* module:   `baseObj`
 * function: ---
 */

module.exports = {
  isObject: require('./isObject.data.js'),
  isPlainObject: require('./isPlainObject.data.js'),
  isDateObject: require('./isDateObject.data.js'),
  isPropertyExists: require('./isPropertyExists.data.js'),
  isMethodExists: require('./isMethodExists.data.js'),
  valueToArray: require('./valueToArray.data.js'),
  valueToArrayEx: require('./valueToArrayEx.data.js'),
  valueToEntry: require('./valueToEntry.data.js'),
  valueToEntries: require('./valueToEntries.data.js'),
  readAsListE: require('./readAsListE.data.js'),
  readAsListS: require('./readAsListS.data.js'),
};
