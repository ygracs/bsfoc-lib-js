// [v0.1.001-20220820]

/* module:   `baseObj`
 * function: isPlainObject
 */

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests aganst "value" param',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'undefined value is passed',
          value: undefined,
          status: {
            ops: false,
          },
        },
      ],
    }, {
      msg: 'non-valid args are given (values must not be treated as an object)',
      param: [
        {
          msg: 'value is a "null"',
          value: null,
          status: {
            ops: false,
          },
        }, {
          msg: 'value is a boolean',
          value: true,
          status: {
            ops: false,
          },
        }, {
          msg: 'value is a number',
          value: 21,
          status: {
            ops: false,
          },
        }, {
          msg: 'value is a string',
          value: 'abc',
          status: {
            ops: false,
          },
        }, {
          msg: 'value is a function',
          value: TEST_FUNC,
          status: {
            ops: false,
          },
        },
      ],
    }, {
      msg: 'non-valid args are given (object that is not consider to be plain)',
      param: [
        {
          msg: 'value is an array',
          value: [],
          status: {
            ops: false,
          },
        },
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        {
          msg: 'value is an object',
          value: {},
          status: {
            ops: true,
          },
        }, {
          msg: 'value is an instance of a "Set"',
          value: new Set(),
          status: {
            ops: true,
          },
        }, {
          msg: 'value is an instance of a "Map"',
          value: new Map(),
          status: {
            ops: true,
          },
        }, {
          msg: 'value is an instance of a "Date"',
          value: new Date(),
          status: {
            ops: true,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    valueTestDescr,
  ],
};
