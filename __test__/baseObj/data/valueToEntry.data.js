// [v0.1.001-20240623]

/* module:   `baseObj`
 * function: valueToEntry
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '(defaults is used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
    {
      msg: 'a valid args are given',
      param: [
        {
          msg: 'value is a boolean',
          rem: '',
          values: {
            value: true,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ '', true ],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: {
            value: 21,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ '', 21 ],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: TEST_FUNC,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ '', TEST_FUNC ],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is a string',
          rem: '',
          values: {
            value: 'abc',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ '', 'abc' ],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is an array',
          rem: '[*]',
          values: {
            value: [ 1, 2, 3 ],
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ 1, 2 ],
            },
            assertQty: 2,
          },
        }, {
          msg: 'value is an object',
          rem: '[*]',
          values: {
            value: { data: [ 1, 2, 3 ] },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [ '', { data: [ 1, 2, 3 ] } ],
            },
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
  },
  tests: [
    valueTestDescr,
  ],
};
