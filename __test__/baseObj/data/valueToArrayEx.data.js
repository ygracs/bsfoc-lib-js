// [v0.1.001-20220820]

/* module:   `baseObj`
 * function: valueToArrayEx
 */

const TEST_FUNC = (value) => { return value; };

const valueTestDescr = {
  msg: 'run tests aganst "value" param',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'undefined value is passed',
          value: {
            main: undefined,
            cb: undefined,
          },
          status: {
            ops: [],
          },
        },
      ],
    }, {
      msg: 'args are given (value is transformed into an array)',
      param: [
        {
          msg: 'value is a "null"',
          value: {
            main: null,
            cb: undefined,
          },
          status: {
            ops: [ null ],
          },
        }, {
          msg: 'value is a boolean',
          value: {
            main: true,
            cb: undefined,
          },
          status: {
            ops: [ true ],
          },
        }, {
          msg: 'value is a number',
          value: {
            main: 21,
            cb: undefined,
          },
          status: {
            ops: [ 21 ],
          },
        }, {
          msg: 'value is a string',
          value: {
            main: 'abc',
            cb: undefined,
          },
          status: {
            ops: [ 'abc' ],
          },
        }, {
          msg: 'value is a function',
          value: {
            main: TEST_FUNC,
            cb: undefined,
          },
          status: {
            ops: [ TEST_FUNC ],
          },
        }, {
          msg: 'value is an object',
          value: {
            main: {},
            cb: undefined,
          },
          status: {
            ops: [ {} ],
          },
        }, {
          msg: 'value is an instance of a "Set"',
          value: {
            main: new Set([ 1, 2, 3 ]),
            cb: undefined,
          },
          status: {
            ops: [ 1, 2, 3 ],
          },
        }, {
          msg: 'value is an instance of a "Map"',
          value: {
            main: new Map([
              [ 'first', 1 ],
              [ 'second', 2 ],
            ]),
            cb: undefined,
          },
          status: {
            ops: [
              [ 'first', 1 ],
              [ 'second', 2 ],
            ],
          },
        }, {
          msg: 'value is an instance of a "Date"',
          value: {
            main: new Date('2022-06-22T22:05:00Z'),
            cb: undefined,
          },
          status: {
            ops: [ new Date('2022-06-22T22:05:00Z') ],
          },
        },
      ],
    }, {
      msg: 'args are given (value is returned "AS IS")',
      param: [
        {
          msg: 'value is an array',
          value: {
            main: [ 1, 2, 3 ],
            cb: undefined,
          },
          status: {
            ops: [ 1, 2, 3 ],
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    valueTestDescr,
  ],
};
