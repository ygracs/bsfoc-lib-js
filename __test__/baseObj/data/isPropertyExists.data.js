// [v0.1.001-20220820]

/* module:   `baseObj`
 * function: isPropertyExists
 */

const TEST_FUNC = (value) => { return value; };

const valueTestDescr_1 = {
  msg: 'run tests aganst "obj" param',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'undefined value is passed',
          value: {
            obj: undefined,
            name: undefined,
          },
          status: {
            ops: false,
          },
        },
      ],
    }, {
      msg: 'non-valid args are given (values must not be treated as an object)',
      param: [
        {
          msg: 'value is a "null"',
          value: {
            obj: null,
            name: undefined,
          },
          status: {
            ops: false,
          },
        }, {
          msg: 'value is a boolean',
          value: {
            obj: true,
            name: undefined,
          },
          status: {
            ops: false,
          },
        }, {
          msg: 'value is a number',
          value: {
            obj: 21,
            name: undefined,
          },
          status: {
            ops: false,
          },
        }, {
          msg: 'value is a string',
          value: {
            obj: 'some value',
            name: undefined,
          },
          status: {
            ops: false,
          },
        }, {
          msg: 'value is a function',
          value: {
            obj: TEST_FUNC,
            name: undefined,
          },
          status: {
            ops: false,
          },
        },
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        {
          msg: 'value is an array',
          value: {
            obj: [],
            name: 'length',
          },
          status: {
            ops: true,
          },
        }, {
          msg: 'value is an object',
          value: {
            obj: { __text: 'some value' },
            name: '__text',
          },
          status: {
            ops: true,
          },
        }, {
          msg: 'value is an instance of a "Set"',
          value: {
            obj: new Set(),
            name: 'size',
          },
          status: {
            ops: true,
          },
        }, {
          msg: 'value is an instance of a "Map"',
          value: {
            obj: new Map(),
            name: 'size',
          },
          status: {
            ops: true,
          },
        },
      ],
    },
  ],
};

const valueTestDescr_2 = {
  msg: 'run tests aganst "name" param',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'undefined value is passed',
          value: {
            obj: { __text: 'some value' },
            name: undefined,
          },
          status: {
            ops: false,
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        {
          msg: 'value is a "null"',
          value: {
            obj: { __text: 'some value' },
            name: null,
          },
          status: {
            ops: false,
          },
        }, {
          msg: 'value is a boolean',
          value: {
            obj: { __text: 'some value' },
            name: true,
          },
          status: {
            ops: false,
          },
        }, {
          msg: 'value is a number',
          value: {
            obj: { __text: 'some value' },
            name: 21,
          },
          status: {
            ops: false,
          },
        }, {
          msg: 'value is an empty string',
          value: {
            obj: { __text: 'some value' },
            name: '',
          },
          status: {
            ops: false,
          },
        }, {
          msg: 'value is a function',
          value: {
            obj: { __text: 'some value' },
            name: TEST_FUNC,
          },
          status: {
            ops: false,
          },
        }, {
          msg: 'value is an array',
          value: {
            obj: { __text: 'some value' },
            name: [],
          },
          status: {
            ops: false,
          },
        }, {
          msg: 'value is an object',
          value: {
            obj: { __text: 'some value' },
            name: {},
          },
          status: {
            ops: false,
          },
        },
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        {
          msg: 'value is a non-empty string',
          value: {
            obj: { __text: 'some value' },
            name: '__text',
          },
          status: {
            ops: true,
          },
        },
      ],
    },
  ],
};

const resultTestDescr_1 = {
  msg: 'run tests aganst a received value of an object property',
  param: [
    {
      msg: 'values that must not be treated as a property',
      param: [
        {
          msg: 'value is an "undefined"',
          value: {
            obj: { __text: undefined },
            name: '__text',
          },
          status: {
            ops: false,
          },
        }, {
          msg: 'value is a function (aka method)',
          value: {
            obj: { __text: TEST_FUNC },
            name: '__text',
          },
          status: {
            ops: false,
          },
        },
      ],
    }, {
      msg: 'values that must be treated as a property',
      param: [
        {
          msg: 'value is a "null"',
          value: {
            obj: { __text: null },
            name: '__text',
          },
          status: {
            ops: true,
          },
        }, {
          msg: 'value is a boolean',
          value: {
            obj: { __text: true },
            name: '__text',
          },
          status: {
            ops: true,
          },
        }, {
          msg: 'value is a boolean',
          value: {
            obj: { __text: true },
            name: '__text',
          },
          status: {
            ops: true,
          },
        }, {
          msg: 'value is a number',
          value: {
            obj: { __text: 21 },
            name: '__text',
          },
          status: {
            ops: true,
          },
        }, {
          msg: 'value is a string',
          value: {
            obj: { __text: 'abc' },
            name: '__text',
          },
          status: {
            ops: true,
          },
        }, {
          msg: 'value is an array',
          value: {
            obj: { __text: [] },
            name: '__text',
          },
          status: {
            ops: true,
          },
        }, {
          msg: 'value is an object',
          value: {
            obj: { __text: {} },
            name: '__text',
          },
          status: {
            ops: true,
          },
        },
      ],
    },
  ],
};

const resultTestDescr_2 = {
  msg: 'run tests aganst special cases',
  param: [
    {
      msg: 'case when "name" parameter is valid but property is not exists',
      param: [
        {
          msg: 'method must return "false"',
          value: {
            obj: { __text: 'some value' },
            name: '__attr',
          },
          status: {
            ops: false,
          },
        },
      ],
    }, {
      msg: 'case when "name" parameter is a string padded with spaces',
      param: [
        {
          msg: 'ensure that a "name" value is trimmed',
          value: {
            obj: { __text: 'some value' },
            name: '   __text  ',
          },
          status: {
            ops: true,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    valueTestDescr_1,
    valueTestDescr_2,
    resultTestDescr_1,
    resultTestDescr_2,
  ],
};
