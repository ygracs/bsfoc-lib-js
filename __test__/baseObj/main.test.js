// [v0.1.009-20230620]

const bso = require('#lib/baseObj.js');

const t_Dat = require('./data/index.data.js');

const test_func = (value) => { return value; };

const test_obj = {
  prop1: undefined,
  prop2: null,
  prop3: false,
  prop4: 0,
  prop5: 'string',
  prop6: [ 1, 2, 3 ],
  prop7: { prop1: 1, prop2: 2, prop3: 3 },
  prop8: test_func,
  prop_undef: undefined,
  prop_null: null,
  prop_bool: false,
  prop_num: 0,
  prop_str: 'string',
  prop_arr: [ 1, 2, 3 ],
  prop_set: new Set([ 1, 2, 3 ]),
  prop_obj: { prop1: 1, prop2: 2, prop3: 3 },
  prop_map: new Map([ [ 'prop1', 1 ], [ 'prop2', 2 ], [ 'prop3', 3 ] ]),
  prop_dt: new Date(),
  prop_fn: test_func,
};

// *** function: isObject()
describe('function: isObject()', () => {
  const testFn = (...args) => { return bso.isObject(...args); };
  const testData = t_Dat.isObject;
  describe.each(testData.descr)('$msg', ({ param }) => {
    describe.each(param)('$msg', ({ param }) => {
      it.each(param)('$msg', ({ value, status }) => {
        let result = undefined;
        expect(() => { result = testFn(value); }).not.toThrow(Error);
        expect(result).toStrictEqual(status.ops);
      });
    });
  });
});

// *** function: isPlainObject()
describe('function: isPlainObject()', () => {
  const testFn = (...args) => { return bso.isPlainObject(...args); };
  const testData = t_Dat.isPlainObject;
  describe.each(testData.descr)('$msg', ({ param }) => {
    describe.each(param)('$msg', ({ param }) => {
      it.each(param)('$msg', ({ value, status }) => {
        let result = undefined;
        expect(() => { result = testFn(value); }).not.toThrow(Error);
        expect(result).toStrictEqual(status.ops);
      });
    });
  });
});

// *** function: isDateObject()
describe('function: isDateObject()', () => {
  const testFn = (...args) => { return bso.isDateObject(...args); };
  const testData = t_Dat.isDateObject;
  describe.each(testData.descr)('$msg', ({ param }) => {
    describe.each(param)('$msg', ({ param }) => {
      it.each(param)('$msg', ({ value, status }) => {
        let result = undefined;
        expect(() => { result = testFn(value); }).not.toThrow(Error);
        expect(result).toStrictEqual(status.ops);
      });
    });
  });
});

// *** function: isPropertyExists()
describe('function: isPropertyExists()', () => {
  const testFn = (...args) => { return bso.isPropertyExists(...args); };
  const testData = t_Dat.isPropertyExists;
  describe.each(testData.descr)('$msg', ({ param }) => {
    describe.each(param)('$msg', ({ param }) => {
      it.each(param)('$msg', ({ value, status }) => {
        const { obj, name } = value;
        let result = undefined;
        expect(() => { result = testFn(obj, name); }).not.toThrow(Error);
        expect(result).toStrictEqual(status.ops);
      });
    });
  });
});

// *** function: isMethodExists()
describe('function: isMethodExists()', () => {
  const testFn = (...args) => { return bso.isMethodExists(...args); };
  const testData = t_Dat.isMethodExists;
  describe.each(testData.descr)('$msg', ({ param }) => {
    describe.each(param)('$msg', ({ param }) => {
      it.each(param)('$msg', ({ value, status }) => {
        const { obj, name } = value;
        let result = undefined;
        expect(() => { result = testFn(obj, name); }).not.toThrow(Error);
        expect(result).toStrictEqual(status.ops);
      });
    });
  });
});

// *** function: valueToArray()
describe('function: valueToArray()', () => {
  const testFn = (...args) => { return bso.valueToArray(...args); };
  const testData = t_Dat.valueToArray;
  describe.each(testData.descr)('$msg', ({ param }) => {
    describe.each(param)('$msg', ({ param }) => {
      it.each(param)('$msg', ({ value, status }) => {
        let result = undefined;
        expect(() => { result = testFn(value); }).not.toThrow(Error);
        expect(result).toStrictEqual(status.ops);
      });
    });
  });
});

// *** function: valueToArrayEx()
describe('function: valueToArrayEx()', () => {
  const testFn = (...args) => { return bso.valueToArrayEx(...args); };
  const testData = t_Dat.valueToArrayEx;
  describe.each(testData.descr)('$msg', ({ param }) => {
    describe.each(param)('$msg', ({ param }) => {
      it.each(param)('$msg', ({ value, status }) => {
        const { main, cb } = value;
        let result = undefined;
        expect(() => { result = testFn(main, cb); }).not.toThrow(Error);
        expect(result).toStrictEqual(status.ops);
      });
    });
  });
});

// *** function: doMethodIfExists()
describe('function: doMethodIfExists()', () => {
  const testFn = (...args) => { return bso.doMethodIfExists(...args); };
  it('with VALID ARGS', () => {
    // with VALID ARGS
    expect(testFn(test_obj, 'prop1', 'result')).toBe(undefined);
    expect(testFn(test_obj, 'prop2', 'result')).toBe(undefined);
    expect(testFn(test_obj, 'prop8', 'result')).toBe('result');
    expect(testFn(test_obj, 'prop10', 'result')).toBe(undefined);
  });
  it('with non-VALID ARGS', () => {
    // with non-VALID ARGS
    expect(testFn(null, 'prop1', 'result')).toBe(undefined);
    expect(testFn(test_obj, 10, 'result')).toBe(undefined);
  });
});
