// [v0.1.007-20230729]

/* module:   `baseClass`
 * class:    TItemsICollection
 * method:   ---
 * property: ---
 */

module.exports = {
  main: {
    addCategory: require('./addCategory.data.js'),
    addItem: require('./addItem.data.js'),
    addItemToCategory: require('./addItemToCategory.data.js'),
    delItem: require('./delItem.data.js'),
    delItemFromCategory: require('./delItemFromCategory.data.js'),
    renameItem: require('./renameItem.data.js'),
    replaceItem: require('./replaceItem.data.js'),
    delCategory: require('./delCategory.data.js'),
    renameCategory: require('./renameCategory.data.js'),
    mergeCategories: require('./mergeCategories.data.js'),
  },
  parent: undefined,
  getProp: {},
  setProp: {},
};
