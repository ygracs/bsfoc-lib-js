// [v0.1.007-20230729]

/* module:   `baseClass`
 * class:    TItemsICollection
 * method:   delItemFromCategory
 * property: ---
 */

// a set of categories for tests
const cats = [
  'comedy', 'fantastic', 'news', 'films', 'musics',
];

// a set of items for tests
const items = [
  {
    name: 'green apple',
    item: {
      obj: 'apple', color: 'green', type: 'fruit',
    },
  }, {
    name: 'purple plumber',
    item: {
      obj: 'plumber', color: 'purple', type: 'berry',
    },
  }, {
    name: 'yellow pear',
    item: {
      obj: 'pear', color: 'yellow', type: 'fruit',
    },
  }, {
    name: 'red pear',
    item: {
      obj: 'pear', color: 'red', type: 'fruit',
    },
  }, {
    name: 'blue bubble',
    item: {
      obj: 'bubble', color: 'blue', type: 'other stuffs',
    },
  },
];

/*module.exports.samples = {
  items: items,
  cats: cats,
};*/

// *** class: TItemsICollection()
// *** method: delItemFromCategory()
const descr = {
  samples: {
    cats: [ cats[0], cats[1], cats[2] ],
    items: [ items[0], items[1], items[2] ],
    index: [
      {
        name: items[0].name,
        cats: [ cats[0], cats[1] ],
      }, {
        name: items[2].name,
        cats: [ cats[0], cats[2] ],
      },
    ],
  },
  descr: [
    {
      msg: 'one category',
      values: [ items[0], [ cats[0] ] ],
      result: true,
      status: {
        before: {
          catNames: [ cats[0], cats[1], cats[2] ],
          itemNames: [ items[0].name, items[1].name, items[2].name ],
        },
        after: {
          index: {
            [items[0].name]: [ cats[1] ],
            [items[1].name]: [],
            [items[2].name]: [ cats[0], cats[2] ],
          },
          c_index: {
            [cats[0]]: [ items[2].item ],
            [cats[1]]: [],
            [cats[2]]: [ items[2].item ],
          },
        },
      },
    }, {
      msg: 'few categories',
      values: [ items[0], [ cats[0], cats[1], cats[2] ] ],
      result: true,
      status: {
        before: {
          catNames: [ cats[0], cats[1], cats[2] ],
          itemNames: [ items[0].name, items[1].name, items[2].name ],
        },
        after: {
          index: {
            [items[0].name]: [],
            [items[1].name]: [],
            [items[2].name]: [ cats[0], cats[2] ],
          },
          c_index: {
            [cats[0]]: [ items[2].item ],
            [cats[1]]: [],
            [cats[2]]: [ items[2].item ],
          },
        },
      },
    },
  ],
};

module.exports = descr;
