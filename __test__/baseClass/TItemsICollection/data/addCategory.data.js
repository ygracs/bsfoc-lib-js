// [v0.1.007-20230729]

/* module:   `baseClass`
 * class:    addCategory
 * method:   ---
 * property: ---
 */

// a set of categories for tests
const cats = [
  'comedy', 'fantastic', 'news', 'films', 'musics',
];

// a set of items for tests
const items = [
  {
    name: 'green apple',
    item: {
      obj: 'apple', color: 'green', type: 'fruit',
    },
  }, {
    name: 'purple plumber',
    item: {
      obj: 'plumber', color: 'purple', type: 'berry',
    },
  }, {
    name: 'yellow pear',
    item: {
      obj: 'pear', color: 'yellow', type: 'fruit',
    },
  }, {
    name: 'red pear',
    item: {
      obj: 'pear', color: 'red', type: 'fruit',
    },
  }, {
    name: 'blue bubble',
    item: {
      obj: 'bubble', color: 'blue', type: 'other stuffs',
    },
  },
];

// *** class: TItemsICollection()
// *** method: addCategory()
const descr = [
  {
    msg: 'a new cat',
    value: cats[0], result: true,
    status: {
      before: {
        hasCat: false,
        catNames: [],
        itemNames: [],
      },
      after: {
        hasCat: true, isSet: true,
        items: [],
        catNames: [ cats[0] ],
        itemNames: [],
      },
    },
  }, {
    msg: 'the same cat [*]',
    value: cats[0], result: true,
    status: {
      before: {
        hasCat: true,
        catNames: [ cats[0] ],
        itemNames: [],
      },
      after: {
        hasCat: true, isSet: true,
        items: [],
        catNames: [ cats[0] ],
        itemNames: [],
      },
    },
  }, {
    msg: 'another cat',
    value: cats[1], result: true,
    status: {
      before: {
        hasCat: false,
        catNames: [ cats[0] ],
        itemNames: [],
      },
      after: {
        hasCat: true, isSet: true,
        items: [],
        catNames: [ cats[0], cats[1] ],
        itemNames: [],
      },
    },
  },
];

module.exports = descr;
