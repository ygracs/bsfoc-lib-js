// [v0.1.007-20230729]

/* module:   `baseClass`
 * class:    TItemsICollection
 * method:   addItem
 * property: ---
 */

// a set of categories for tests
const cats = [
  'comedy', 'fantastic', 'news', 'films', 'musics',
];

// a set of items for tests
const items = [
  {
    name: 'green apple',
    item: {
      obj: 'apple', color: 'green', type: 'fruit',
    },
  }, {
    name: 'purple plumber',
    item: {
      obj: 'plumber', color: 'purple', type: 'berry',
    },
  }, {
    name: 'yellow pear',
    item: {
      obj: 'pear', color: 'yellow', type: 'fruit',
    },
  }, {
    name: 'red pear',
    item: {
      obj: 'pear', color: 'red', type: 'fruit',
    },
  }, {
    name: 'blue bubble',
    item: {
      obj: 'bubble', color: 'blue', type: 'other stuffs',
    },
  },
];

/*module.exports.samples = {
  items: items,
  cats: cats,
};*/

// *** class: TItemsICollection()
// *** method: addItem()
const descr = {
  samples: {
    cats: [ cats[0], cats[1] ],
    items: [],
    index: [],
  },
  descr: [
    {
      msg: 'no cats set before method is called',
      setCats: false,
      data: [
        {
          msg: 'a new item',
          value: items[0], result: true,
          status: {
            before: {
              hasItem: false,
              catNames: [],
              itemNames: [],
            },
            after: {
              hasItem: true,
              catNames: [],
              itemNames: [ items[0].name ],
            },
          },
        }, {
          msg: 'the same item [*]',
          value: items[0], result: true,
          status: {
            before: {
              hasItem: true,
              catNames: [],
              itemNames: [ items[0].name ],
            },
            after: {
              hasItem: true,
              catNames: [],
              itemNames: [ items[0].name ],
            },
          },
        }, {
          msg: 'another item',
          value: items[1], result: true,
          status: {
            before: {
              hasItem: false,
              catNames: [],
              itemNames: [ items[0].name ],
            },
            after: {
              hasItem: true,
              catNames: [],
              itemNames: [ items[0].name, items[1].name ],
            },
          },
        },
      ],
    },
  ],
};

module.exports = descr;
