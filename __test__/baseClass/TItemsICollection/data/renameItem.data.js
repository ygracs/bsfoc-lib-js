// [v0.1.007-20230729]

/* module:   `baseClass`
 * class:    TItemsICollection
 * method:   renameItem
 * property: ---
 */

// a set of categories for tests
const cats = [
  'comedy', 'fantastic', 'news', 'films', 'musics',
];

// a set of items for tests
const items = [
  {
    name: 'green apple',
    item: {
      obj: 'apple', color: 'green', type: 'fruit',
    },
  }, {
    name: 'purple plumber',
    item: {
      obj: 'plumber', color: 'purple', type: 'berry',
    },
  }, {
    name: 'yellow pear',
    item: {
      obj: 'pear', color: 'yellow', type: 'fruit',
    },
  }, {
    name: 'red pear',
    item: {
      obj: 'pear', color: 'red', type: 'fruit',
    },
  }, {
    name: 'blue bubble',
    item: {
      obj: 'bubble', color: 'blue', type: 'other stuffs',
    },
  },
];

/*module.exports.samples = {
  items: items,
  cats: cats,
};*/

// *** class: TItemsICollection()
// *** method: renameItem()
const descr = {
  samples: {
    cats: [ cats[0], cats[1], cats[2] ],
    items: [ items[0], items[1], items[2] ],
    index: [
      {
        name: items[0].name,
        cats: [ cats[0], cats[1] ],
      }, {
        name: items[2].name,
        cats: [ cats[0], cats[2] ],
      },
    ],
  },
  descr: [
    {
      msg: 'not exists',
      result: [
        {
          msg: 'done',
          values: [ items[3], items[2] ],
          result: false,
          status: {
            before: {
              catNames: [ cats[0], cats[1], cats[2] ],
              itemNames: [ items[0].name, items[1].name, items[2].name ],
            },
            after: {
              index: {
                [items[2].name]: [ cats[0], cats[2] ],
                [items[3].name]: [],
              },
            },
          },
        },
      ],
    }, {
      msg: 'exists',
      result: [
        {
          msg: 'new item name is not exists',
          values: [ items[2], items[3] ],
          result: true,
          status: {
            before: {
              catNames: [ cats[0], cats[1], cats[2] ],
              itemNames: [ items[0].name, items[1].name, items[2].name ],
            },
            after: {
              index: {
                [items[2].name]: [],
                [items[3].name]: [ cats[0], cats[2] ],
              },
            },
          },
        }, {
          msg: 'new item name is the same',
          values: [ items[2], items[2] ],
          result: true,
          status: {
            before: {
              catNames: [ cats[0], cats[1], cats[2] ],
              itemNames: [ items[0].name, items[1].name, items[2].name ],
            },
            after: {
              index: {
                [items[2].name]: [ cats[0], cats[2] ],
              },
            },
          },
        }, {
          msg: 'new item name is exists and not the same',
          values: [ items[2], items[0] ],
          result: false,
          status: {
            before: {
              catNames: [ cats[0], cats[1], cats[2] ],
              itemNames: [ items[0].name, items[1].name, items[2].name ],
            },
            after: {
              index: {
                [items[0].name]: [ cats[0], cats[1] ],
                [items[2].name]: [ cats[0], cats[2] ],
              },
            },
          },
        },
      ],
    },
  ],
};

module.exports = descr;
