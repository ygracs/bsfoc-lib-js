// [v0.1.007-20230729]

/* module:   `baseClass`
 * class:    TItemsICollection
 * method:   addItemToCategory
 * property: ---
 */

// a set of categories for tests
const cats = [
  'comedy', 'fantastic', 'news', 'films', 'musics',
];

// a set of items for tests
const items = [
  {
    name: 'green apple',
    item: {
      obj: 'apple', color: 'green', type: 'fruit',
    },
  }, {
    name: 'purple plumber',
    item: {
      obj: 'plumber', color: 'purple', type: 'berry',
    },
  }, {
    name: 'yellow pear',
    item: {
      obj: 'pear', color: 'yellow', type: 'fruit',
    },
  }, {
    name: 'red pear',
    item: {
      obj: 'pear', color: 'red', type: 'fruit',
    },
  }, {
    name: 'blue bubble',
    item: {
      obj: 'bubble', color: 'blue', type: 'other stuffs',
    },
  },
];

/*module.exports.samples = {
  items: items,
  cats: cats,
};*/

// *** class: TItemsICollection()
// *** method: addItemToCategory()
const descr = {
  samples: {
    cats: [ cats[0], cats[1] ],
    items: [ items[0], items[1], items[2] ],
    index: [],
  },
  descr: [
    {
      msg: 'one item into category',
      values: [ items[0], [ cats[0] ] ],
      result: true,
      status: {
        before: {
          catNames: [ cats[0], cats[1] ],
          itemNames: [ items[0].name, items[1].name, items[2].name ],
        },
        after: {
          index: {
            [items[0].name]: [ cats[0] ],
            [items[1].name]: [],
            [items[2].name]: [],
          },
          c_index: {
            [cats[0]]: [ items[0].item ],
            [cats[1]]: [],
          },
        },
      },
    }, {
      msg: 'the same item into category',
      values: [ items[0], [ cats[0] ] ],
      result: true,
      status: {
        before: {
          catNames: [ cats[0], cats[1] ],
          itemNames: [ items[0].name, items[1].name, items[2].name ],
        },
        after: {
          index: {
            [items[0].name]: [ cats[0] ],
            [items[1].name]: [],
            [items[2].name]: [],
          },
          c_index: {
            [cats[0]]: [ items[0].item ],
            [cats[1]]: [],
          },
        },
      },
    }, {
      msg: 'another item into category',
      values: [ items[1], [ cats[0] ] ],
      result: true,
      status: {
        before: {
          catNames: [ cats[0], cats[1] ],
          itemNames: [ items[0].name, items[1].name, items[2].name ],
        },
        after: {
          index: {
            [items[0].name]: [ cats[0] ],
            [items[1].name]: [ cats[0] ],
            [items[2].name]: [],
          },
          c_index: {
            [cats[0]]: [ items[0].item, items[1].item ],
            [cats[1]]: [],
          },
        },
      },
    }, {
      msg: 'anoter item into few categories',
      values: [ items[2], [ cats[0], cats[1] ] ],
      result: true,
      status: {
        before: {
          catNames: [ cats[0], cats[1] ],
          itemNames: [ items[0].name, items[1].name, items[2].name ],
        },
        after: {
          index: {
            [items[0].name]: [ cats[0] ],
            [items[1].name]: [ cats[0] ],
            [items[2].name]: [ cats[0], cats[1] ],
          },
          c_index: {
            [cats[0]]: [ items[0].item, items[1].item, items[2].item ],
            [cats[1]]: [ items[2].item ],
          },
        },
      },
    },
  ],
};

module.exports = descr;
