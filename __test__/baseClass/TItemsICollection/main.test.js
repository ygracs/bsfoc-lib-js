// [v0.0.012-20230729]

const { TItemsICollection } = require("#lib/baseClass.js");

let obj = null; // test object

const t_Dat = require('./data/index.data.js');

function value_to_array(value){
  if (value === undefined) return [];
  if (value === null) return [];
  if (value instanceof Set) return [ ...value.values() ];
  if (!Array.isArray(value)) return [ value ];
  return value;
};

function is_array_of_sets(value){
  let isSUCCEED = Array.isArray(value);
  if (isSUCCEED && value.length > 0) {
    for (let element of value) {
      if (!(element instanceof Set)) {
        isSUCCEED = false; break;
      };
    };
  };
  return isSUCCEED;
}

function preloads(obj, samples){
  for (let { name, item } of samples.items) {
    obj.addItem(name, item);
  };
  for (let cat of samples.cats) {
    obj.addCategory(cat);
  };
  for (let { name, cats } of samples.index) {
    obj.addItemToCategory(name, ...cats);
  };
}

// *** class: TItemsICollection()
describe('class: TItemsICollection()', () => {
  describe('1. create new instance', () => {
    let testInst = null;
    it('done', () => {
      expect(() => { testInst = new TItemsICollection(); }).not.toThrow();
      expect(testInst instanceof TItemsICollection).toBe(true);
    });
    it('check instance status', () => {
      expect(testInst instanceof TItemsICollection).toBe(true);
      expect(testInst.ItemNames).toStrictEqual([]);
      expect(testInst.CategoryNames).toStrictEqual([]);
    });
  });

  describe('2. add items to the instance', () => {
    describe('2.1 - method: addCategory()', () => {
      const testFn = (...args) => { return testInst.addCategory(...args); };
      const testData = t_Dat.main.addCategory;
      let testInst = null;
      beforeAll(() => { testInst = new TItemsICollection(); });
      describe.each(testData)('- add $msg', ({ value, result, status }) => {
        const stat_b = status.before; const stat_a = status.after;
        it('check instance status (before)', () => {
          expect(testInst instanceof TItemsICollection).toBe(true);
          expect(testInst.hasCategory(value)).toBe(stat_b.hasCat);
          expect(testInst.ItemNames).toStrictEqual(stat_b.itemNames);
          expect(testInst.CategoryNames).toStrictEqual(stat_b.catNames);
        });
        it('done', () => {
          expect(testFn(value)).toBe(result);
        });
        it('check instance status (after)', () => {
          expect(testInst.ItemNames).toStrictEqual(stat_a.itemNames);
          expect(testInst.hasCategory(value)).toBe(stat_a.hasCat);
          expect(testInst.getCategory(value) instanceof Set).toBe(stat_a.isSet);
          let cat_items = value_to_array(testInst.getCategory(value));
          expect(cat_items).toStrictEqual(stat_a.items);
          expect(testInst.CategoryNames).toStrictEqual(stat_a.catNames);
        });
      });
    });

    describe('2.2 - method: addItem()', () => {
      const testFn = (...args) => { return testInst.addItem(...args); };
      const testData = t_Dat.main.addItem;
      const samples = testData.samples;
      let testInst = null;
      describe.each(testData.descr)('$msg', ({ setCats, data }) => {
        beforeAll(() => {
          testInst = new TItemsICollection();
          if (setCats) preloads(testInst, samples);
        });
        describe.each(data)('- add $msg', ({ value, result, status }) => {
          const stat_b = status.before; const stat_a = status.after;
          it('check instance status (before)', () => {
            expect(testInst instanceof TItemsICollection).toBe(true);
            expect(testInst.hasItem(value.name)).toBe(stat_b.hasItem);
            expect(testInst.ItemNames).toStrictEqual(stat_b.itemNames);
            expect(testInst.CategoryNames).toStrictEqual(stat_b.catNames);
          });
          it('done', () => {
            expect(testFn(value.name, value.item)).toBe(result);
          });
          it('check instance status (after)', () => {
            expect(testInst.hasItem(value.name)).toBe(stat_a.hasItem);
            expect(testInst.getItem(value.name)).toStrictEqual(value.item);
            expect(testInst.ItemNames).toStrictEqual(stat_a.itemNames);
            expect(testInst.CategoryNames).toStrictEqual(stat_a.catNames);
          });
        });
      });
    });

    describe('2.3. method: addItemToCategory()', () => {
      const testFn = (...args) => {
        return testInst.addItemToCategory(...args);
      };
      const testData = t_Dat.main.addItemToCategory;
      const samples = testData.samples;
      let testInst = null;
      beforeAll(() => {
        testInst = new TItemsICollection(); preloads(testInst, samples);
      });
      describe.each(testData.descr)('- push $msg', ({
        values, result, status,
      }) => {
        const stat_b = status.before; const stat_a = status.after;
        it('check instance status (before)', () => {
          expect(testInst instanceof TItemsICollection).toBe(true);
          expect(testInst.ItemNames).toStrictEqual(stat_b.itemNames);
          expect(testInst.CategoryNames).toStrictEqual(stat_b.catNames);
        });
        it('done', () => {
          let name = values[0].name; let cats = values[1];
          expect(testFn(name, ...cats)).toBe(result);
        });
        it('check instance status (after)', () => {
          expect(
            testInst.getItemCategories(values[0].name)
          ).toStrictEqual(stat_a.index[values[0].name]);
          for (let i = 0; i < values[1].length; i++) {
            expect(
              [...testInst.getCategory(values[1][i])]
            ).toStrictEqual(stat_a.c_index[values[1][i]]);
          };
        });
      });
    });
  });

  describe('3. delete items from the instance', () => {
    describe('3.1 - method: delItemFromCategory()', () => {
      const testFn = (...args) => {
        return testInst.delItemFromCategory(...args);
      };
      const testData = t_Dat.main.delItemFromCategory;
      const samples = testData.samples;
      let testInst = null;
      describe.each(testData.descr)('- delete item from $msg', ({
        values, result, status,
      }) => {
        const stat_b = status.before; const stat_a = status.after;
        beforeAll(() => {
          testInst = new TItemsICollection(); preloads(testInst, samples);
        });
        it('check instance status (before)', () => {
          expect(testInst instanceof TItemsICollection).toBe(true);
          expect(testInst.ItemNames).toStrictEqual(stat_b.itemNames);
          expect(testInst.CategoryNames).toStrictEqual(stat_b.catNames);
        });
        it('done', () => {
          let name = values[0].name; let cats = values[1];
          expect(testFn(name, ...cats)).toBe(result);
        });
        it('check instance status (after)', () => {
          expect(
            testInst.getItemCategories(values[0].name)
          ).toStrictEqual(stat_a.index[values[0].name]);
          for (let i = 0; i < values[1].length; i++) {
            expect(
              [...testInst.getCategory(values[1][i])]
            ).toStrictEqual(stat_a.c_index[values[1][i]]);
          };
        });
      });
    });

    describe('3.2 - method: delItem()', () => {
      const testFn = (value) => { return testInst.delItem(value); };
      const testData = t_Dat.main.delItem;
      const samples = testData.samples;
      let testInst = null;
      beforeAll(() => {
        testInst = new TItemsICollection(); preloads(testInst, samples);
      });
      describe.each(testData.descr)('- item is $msg', ({
        value, result, status,
      }) => {
        const stat_b = status.before; const stat_a = status.after;
        it('check instance status (before)', () => {
          expect(testInst instanceof TItemsICollection).toBe(true);
          expect(testInst.ItemNames).toStrictEqual(stat_b.itemNames);
          expect(testInst.CategoryNames).toStrictEqual(stat_b.catNames);
        });
        it('done', () => {
          expect(testFn(value.name)).toBe(result);
        });
        it('check instance status (after)', () => {
          expect(testInst.ItemNames).toStrictEqual(stat_a.itemNames);
          expect(
            testInst.getItemCategories(value.name)
          ).toStrictEqual(value_to_array(stat_a.index[value.name]));
          for (let i = 0; i < stat_a.catNames.length; i++) {
            expect(
              [...testInst.getCategory(stat_a.catNames[i])]
            ).toStrictEqual(stat_a.c_index[stat_a.catNames[i]]);
          };
        });
      });
    });

    describe('3.3 - method: delCategory()', () => {
      const testFn = (value) => { return testInst.delCategory(value); };
      const testData = t_Dat.main.delCategory;
      const samples = testData.samples;
      let testInst = null;
      describe.each(testData.descr)('- category is $msg', ({
        value, result, status,
      }) => {
        const stat_b = status.before; const stat_a = status.after;
        beforeAll(() => {
          testInst = new TItemsICollection(); preloads(testInst, samples);
        });
        it('check instance status (before)', () => {
          expect(testInst instanceof TItemsICollection).toBe(true);
          expect(testInst.ItemNames).toStrictEqual(stat_b.itemNames);
          expect(testInst.CategoryNames).toStrictEqual(stat_b.catNames);
        });
        it('done', () => {
          expect(testFn(value)).toBe(result);
        });
        it('check instance status (after)', () => {
          expect(
            value_to_array(testInst.getCategory(value))
          ).toStrictEqual(stat_a.c_index[value]);
          for (let i = 0; i < stat_b.itemNames.length; i++) {
            expect(
              [...testInst.getItemCategories(stat_b.itemNames[i])]
            ).toStrictEqual(stat_a.index[stat_b.itemNames[i]]);
          };
        });
      });
    });
  });

  describe('4. other ops with items', () => {
    describe('4.1 - method: renameItem()', () => {
      const testFn = (...args) => { return testInst.renameItem(...args); };
      const testData = t_Dat.main.renameItem;
      const samples = testData.samples;
      let testInst = null;
      describe.each(testData.descr)('old item name is $msg', ({ result }) => {
        describe.each(result)('- $msg', ({ values, result, status }) => {
          const stat_b = status.before; const stat_a = status.after;
          beforeAll(() => {
            testInst = new TItemsICollection(); preloads(testInst, samples);
          });
          it('check instance status (before)', () => {
            expect(testInst instanceof TItemsICollection).toBe(true);
            expect(testInst.ItemNames).toStrictEqual(stat_b.itemNames);
            expect(testInst.CategoryNames).toStrictEqual(stat_b.catNames);
          });
          it('done', () => {
            expect(testFn(values[0].name, values[1].name)).toBe(result);
          });
          it('check instance status (after)', () => {
            expect(
              testInst.getItemCategories(values[0].name)
            ).toStrictEqual(stat_a.index[values[0].name]);
            expect(
              testInst.getItemCategories(values[1].name)
            ).toStrictEqual(stat_a.index[values[1].name]);
          });
        });
      });
    })

    describe('4.2 - method: replaceItem()', () => {
      const testFn = (...args) => { return testInst.replaceItem(...args); };
      const testData = t_Dat.main.replaceItem;
      const samples = testData.samples;
      let testInst = null;
      describe.each(testData.descr)('item name is $msg', ({ result }) => {
        describe.each(result)('- new item is $msg', ({
          values, result, status,
        }) => {
          const stat_b = status.before; const stat_a = status.after;
          beforeAll(() => {
            testInst = new TItemsICollection(); preloads(testInst, samples);
          });
          it('check instance status (before)', () => {
            expect(testInst instanceof TItemsICollection).toBe(true);
            expect(testInst.ItemNames).toStrictEqual(stat_b.itemNames);
            expect(testInst.CategoryNames).toStrictEqual(stat_b.catNames);
          });
          it('done', () => {
            expect(testFn(values[0].name, values[1].item)).toBe(result);
          });
          it('check instance status (after)', () => {
            expect(
              testInst.getItemCategories(values[0].name)
            ).toStrictEqual(stat_a.index[values[0].name]);
            for (let i = 0; i < stat_b.catNames.length; i++) {
              expect(
                [...testInst.getCategory(stat_b.catNames[i])]
              ).toStrictEqual(stat_a.c_index[stat_b.catNames[i]]);
            };
          });
        });
      });
    });
  });

  describe('5. other ops with items categories', () => {
    describe('5.1 - method: renameCategory()', () => {
      const testFn = (...args) => { return testInst.renameCategory(...args); };
      const testData = t_Dat.main.renameCategory;
      const samples = testData.samples;
      let testInst = null;
      describe.each(testData.descr)('old category name is $msg', ({ result }) => {
        describe.each(result)('- $msg', ({ values, result, status }) => {
          const stat_b = status.before; const stat_a = status.after;
          beforeAll(() => {
            testInst = new TItemsICollection(); preloads(testInst, samples);
          });
          it('check instance status (before)', () => {
            expect(testInst instanceof TItemsICollection).toBe(true);
            expect(testInst.ItemNames).toStrictEqual(stat_b.itemNames);
            expect(testInst.CategoryNames).toStrictEqual(stat_b.catNames);
          });
          it('done', () => {
            expect(testFn(...values)).toBe(result);
          });
          it('check instance status (after)', () => {
            expect(
              value_to_array(testInst.getCategory(values[0]))
            ).toStrictEqual(stat_a.c_index[values[0]]);
            expect(
              value_to_array(testInst.getCategory(values[1]))
            ).toStrictEqual(stat_a.c_index[values[1]]);
            for (let i = 0; i < stat_b.itemNames.length; i++) {
              expect(
                [...testInst.getItemCategories(stat_b.itemNames[i])]
              ).toStrictEqual(stat_a.index[stat_b.itemNames[i]]);
            };
          });
        });
      });
    });

    describe('5.1 - method: mergeCategories()', () => {
      const testFn = (...args) => { return testInst.mergeCategories(...args); };
      const testData = t_Dat.main.mergeCategories;
      const samples = testData.samples;
      let testInst = null;
      describe.each(testData.descr)('$msg', ({ result }) => {
        describe.each(result)('- $msg', ({ values, result, status }) => {
          const stat_b = status.before; const stat_a = status.after;
          beforeAll(() => {
            testInst = new TItemsICollection(); preloads(testInst, samples);
          });
          it('check instance status (before)', () => {
            expect(testInst instanceof TItemsICollection).toBe(true);
            expect(testInst.ItemNames).toStrictEqual(stat_b.itemNames);
            expect(testInst.CategoryNames).toStrictEqual(stat_b.catNames);
          });
          it('done', () => {
            expect(testFn(...values)).toBe(result);
          });
          it('check instance status (after)', () => {
            for (let i = 0; i < values.length; i++) {
              expect(
                value_to_array(testInst.getCategory(values[i]))
              ).toStrictEqual(stat_a.c_index[values[i]]);
            };
            for (let i = 0; i < stat_b.itemNames.length; i++) {
              expect(
                [...testInst.getItemCategories(stat_b.itemNames[i])]
              ).toStrictEqual(stat_a.index[stat_b.itemNames[i]]);
            };
          });
        });
      });
    });
  });
});
