// [v0.1.001-20220831]

/* module:   `baseClass`
 * class:    TItemsListEx
 * method:   srchNextIndex
 * property: ---
 */

const TEST_FUNC = (value) => { return value; };

const samples_1 = [
  'sam_t0', 'sam_t1', 'sam_t2', 'sam_t3', 'sam_t4', 'sam_t5', 'sam_t6',
];

const samples_2 = [
  'sam_t0', null, true, 234, 'sam_t4', TEST_FUNC,
  [ 'sam_t5' ], { name: 'sam_t6' },
];

//const samples_3 = [ ...samples_1, ...samples_1 ];

const preloads = [
  {
    items: [ ...samples_2, ...samples_2, ...samples_1 ],
    curIndex: -1,
    status: {
      count: 23,
      curIndex: -1,
      minIndex: 0,
      maxIndex: 22,
      prevIndex: -1,
      nextIndex: -1,
      isEmpty: false,
    },
  }, {
    items: [ ...samples_1, ...samples_1 ],
    curIndex: -1,
    status: {
      count: 14,
      curIndex: -1,
      minIndex: 0,
      maxIndex: 13,
      prevIndex: -1,
      nextIndex: -1,
      isEmpty: false,
    },
  }, {
    items: samples_1,
    curIndex: -1,
    status: {
      count: 7,
      curIndex: -1,
      minIndex: 0,
      maxIndex: 6,
      prevIndex: -1,
      nextIndex: -1,
      isEmpty: false,
    },
  },
];

const valueTestDescr = {
  msg: 'run tests aganst "item" param',
  param: [
    {
      msg: 'no args are given',
      preloads: preloads[0],
      param: [
        {
          msg: 'undefined value is passed',
          value: undefined,
          status: {
            ops: {
              seekFirst: true,
              cycles: [ -1, -1, -1 ],
              assertQty: 2,
            },
            get before(){
              return valueTestDescr.param[0].preloads.status;
            },
            after: undefined,
          },
        },
      ],
    }, {
      msg: 'args are given',
      preloads: preloads[0],
      param: [
        {
          msg: 'value is a "null"',
          value: null,
          status: {
            ops: {
              seekFirst: true,
              cycles: [ 1, 9, -1 ],
              assertQty: 6,
            },
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            after: undefined,
          },
        }, {
          msg: 'value is a boolean',
          value: true,
          status: {
            ops: {
              seekFirst: true,
              cycles: [ 2, 10, -1 ],
              assertQty: 6,
            },
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            after: undefined,
          },
        }, {
          msg: 'value is a number',
          value: 234,
          status: {
            ops: {
              seekFirst: true,
              cycles: [ 3, 11, -1 ],
              assertQty: 6,
            },
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            after: undefined,
          },
        }, {
          msg: 'value is a string',
          value: 'sam_t4',
          status: {
            ops: {
              seekFirst: true,
              cycles: [ 4, 12, 20, -1 ],
              assertQty: 8,
            },
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            after: undefined,
          },
        },
      ],
    },
  ],
};

const resultTestDescr = {
  msg: 'run tests aganst result values',
  param: [
    {
      msg: 'target element was not searched early',
      preloads: preloads[1],
      param: [
        {
          msg: 'run test',
          value: 'sam_t4',
          status: {
            ops: {
              seekFirst: false,
              cycles: [ -1, -1, -1 ],
              assertQty: 2,
            },
            get before(){
              return resultTestDescr.param[0].preloads.status;
            },
            after: undefined,
          },
        },
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    valueTestDescr,
    resultTestDescr,
  ],
};
