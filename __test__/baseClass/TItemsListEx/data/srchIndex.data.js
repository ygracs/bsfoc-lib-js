// [v0.1.001-20220831]

/* module:   `baseClass`
 * class:    TItemsListEx
 * method:   srchIndex
 * property: ---
 */

const TEST_FUNC = (value) => { return value; };

const samples_1 = [
  'sam_t0', 'sam_t1', 'sam_t2', 'sam_t3', 'sam_t4', 'sam_t5', 'sam_t6',
];

const samples_2 = [
  'sam_t0', null, true, 234, 'sam_t4', TEST_FUNC,
  [ 'sam_t5' ], { name: 'sam_t6' },
];

//const samples_3 = [ ...samples_1, ...samples_1 ];

const preloads = [
  {
    items: samples_2,
    curIndex: -1,
    status: {
      count: 8,
      curIndex: -1,
      minIndex: 0,
      maxIndex: 7,
      prevIndex: -1,
      nextIndex: -1,
      isEmpty: false,
    },
  }, {
    items: [ ...samples_1, ...samples_1 ],
    curIndex: -1,
    status: {
      count: 14,
      curIndex: -1,
      minIndex: 0,
      maxIndex: 13,
      prevIndex: -1,
      nextIndex: -1,
      isEmpty: false,
    },
  }, {
    items: samples_1,
    curIndex: -1,
    status: {
      count: 7,
      curIndex: -1,
      minIndex: 0,
      maxIndex: 6,
      prevIndex: -1,
      nextIndex: -1,
      isEmpty: false,
    },
  },
];

const valueTestDescr_1 = {
  msg: 'run tests aganst "item" param',
  param: [
    {
      msg: 'no args are given',
      preloads: preloads[0],
      param: [
        {
          msg: 'undefined value is passed',
          value: {
            index: undefined,
            item: undefined,
          },
          status: {
            ops: -1,
            get before(){
              return valueTestDescr_1.param[0].preloads.status;
            },
            after: undefined,
          },
        },
      ],
    }, {
      msg: 'args are given',
      preloads: preloads[0],
      param: [
        {
          msg: 'value is a "null"',
          value: {
            index: undefined,
            item: null,
          },
          status: {
            ops: 1,
            get before(){
              return valueTestDescr_1.param[1].preloads.status;
            },
            after: undefined,
          },
        }, {
          msg: 'value is a boolean',
          value: {
            index: undefined,
            item: true,
          },
          status: {
            ops: 2,
            get before(){
              return valueTestDescr_1.param[1].preloads.status;
            },
            after: undefined,
          },
        }, {
          msg: 'value is a number',
          value: {
            index: undefined,
            item: 234,
          },
          status: {
            ops: 3,
            get before(){
              return valueTestDescr_1.param[1].preloads.status;
            },
            after: undefined,
          },
        }, {
          msg: 'value is a string',
          value: {
            index: undefined,
            item: 'sam_t4',
          },
          status: {
            ops: 4,
            get before(){
              return valueTestDescr_1.param[1].preloads.status;
            },
            after: undefined,
          },
        },
      ],
    },
  ],
};

const valueTestDescr_2 = {
  msg: 'run tests aganst "index" param',
  param: [
    {
      msg: 'no args are given',
      preloads: preloads[1],
      param: [
        {
          msg: 'undefined value is passed',
          value: {
            index: undefined,
            item: 'sam_t4',
          },
          status: {
            ops: 4,
            get before(){
              return valueTestDescr_2.param[0].preloads.status;
            },
            after: undefined,
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      preloads: preloads[1],
      param: [
        {
          msg: 'value is a "null"',
          value: {
            index: null,
            item: 'sam_t4',
          },
          status: {
            ops: 4,
            get before(){
              return valueTestDescr_2.param[1].preloads.status;
            },
            after: undefined,
          },
        },
      ],
    }, {
      msg: 'valid args are given',
      preloads: preloads[1],
      param: [
        {
          msg: 'value is a positive number',
          value: {
            index: 5,
            item: 'sam_t4',
          },
          status: {
            ops: 11,
            get before(){
              return valueTestDescr_2.param[2].preloads.status;
            },
            after: undefined,
          },
        }, {
          msg: 'value is a string that represents a positive number',
          value: {
            index: '5',
            item: 'sam_t4',
          },
          status: {
            ops: 11,
            get before(){
              return valueTestDescr_2.param[2].preloads.status;
            },
            after: undefined,
          },
        },
      ],
    },
  ],
};

const rangeTestDescr = {
  msg: 'run tests aganst "index" range',
  param: [
    {
      msg: 'index value is out of range',
      preloads: preloads[2],
      param: [
        {
          msg: 'value is less than "0"',
          value: {
            index: -15,
            item: 'sam_t4',
          },
          status: {
            ops: 4,
            get before(){
              return rangeTestDescr.param[0].preloads.status;
            },
            after: undefined,
          },
        }, {
          msg: 'value is greater than "maxIndex"',
          value: {
            index: 15,
            item: 'sam_t4',
          },
          status: {
            ops: -1,
            get before(){
              return rangeTestDescr.param[0].preloads.status;
            },
            after: undefined,
          },
        },
      ],
    }, {
      msg: 'index is in range',
      preloads: preloads[2],
      param: [
        {
          msg: 'run test',
          value: {
            index: 2,
            item: 'sam_t4',
          },
          status: {
            ops: 4,
            get before(){
              return rangeTestDescr.param[1].preloads.status;
            },
            after: undefined,
          },
        },
      ],
    },
  ],
};

const resultTestDescr = {
  msg: 'run tests aganst result values',
  param: [
    {
      msg: 'target element is not exists',
      preloads: preloads[2],
      param: [
        {
          msg: 'run test',
          value: {
            index: undefined,
            item: 'sam_t8',
          },
          status: {
            ops: -1,
            get before(){
              return resultTestDescr.param[0].preloads.status;
            },
            after: undefined,
          },
        },
      ],
    }, {
      msg: 'target element is exists',
      preloads: preloads[2],
      param: [
        {
          msg: 'given index is ahead of the searched element',
          value: {
            index: 2,
            item: 'sam_t4',
          },
          status: {
            ops: 4,
            get before(){
              return resultTestDescr.param[1].preloads.status;
            },
            after: undefined,
          },
        }, {
          msg: 'given index is behind the searched element',
          value: {
            index: 5,
            item: 'sam_t4',
          },
          status: {
            ops: -1,
            get before(){
              return resultTestDescr.param[1].preloads.status;
            },
            after: undefined,
          },
        },
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    valueTestDescr_1,
    valueTestDescr_2,
    rangeTestDescr,
    resultTestDescr,
  ],
};
