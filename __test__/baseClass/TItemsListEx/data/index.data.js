// [v0.1.002-20230624]

/* module:   `baseClass`
 * class:    TItemsListEx
 * method:   ---
 * property: ---
 */

module.exports = {
  main: {
    //setItem: require('./setItem.data.js'),
    //delItem: require('./delItem.data.js'),
    //delItemEx: require('./delItemEx.data.js'),
    //clear: require('./clear.data.js'),
    srchIndex: require('./srchIndex.data.js'),
    srchNextIndex: require('./srchNextIndex.data.js'),
  },
  parent: require('../../TItemsList/data/index.data.js'),
  getProp: {},
  setProp: {
    //curItem: require('./setProp.curItem.data.js'),
  },
};
