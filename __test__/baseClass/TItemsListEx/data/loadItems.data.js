// [v0.1.001-20220831]

/* module:   `baseClass`
 * class:    TItemsListEx
 * method:   loadItems
 * property: ---
 */

const TEST_FUNC = (value) => { return value; };

const samples = [
  'sam_t0', 'sam_t1', 'sam_t2', 'sam_t3', 'sam_t4', 'sam_t5', 'sam_t6',
];

const preloads = [
  {
    items: [],
    curIndex: -1,
    status: {
      count: 0,
      curIndex: -1,
      minIndex: -1,
      maxIndex: -1,
      prevIndex: -1,
      nextIndex: -1,
      isEmpty: true,
    },
  }, {
    items: samples,
    curIndex: -1,
    status: {
      count: 7,
      curIndex: -1,
      minIndex: 0,
      maxIndex: 6,
      prevIndex: -1,
      nextIndex: -1,
      isEmpty: false,
    },
  }, {
    items: samples,
    curIndex: 2,
    status: {
      count: 7,
      curIndex: 2,
      minIndex: 0,
      maxIndex: 6,
      prevIndex: 1,
      nextIndex: 3,
      isEmpty: false,
    },
  },
];

const valueTestDescr = {
  msg: 'run tests aganst "list" param (defaults is used)',
  param: [
    {
      msg: 'no args are given',
      preloads: preloads[0],
      param: [
        {
          msg: 'undefined value is passed',
          value: {
            items: undefined,
            options: undefined,
          },
          status: {
            ops: 0,
            get before(){
              return valueTestDescr.param[0].preloads.status;
            },
            get after(){
              return valueTestDescr.param[0].preloads.status;
            },
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      preloads: preloads[0],
      param: [
        {
          msg: 'value is a "null"',
          value: {
            items: null,
            options: undefined,
          },
          status: {
            ops: 0,
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            get after(){
              return valueTestDescr.param[1].preloads.status;
            },
          },
        },
      ],
    }, {
      msg: 'valid args are given',
      preloads: preloads[0],
      param: [
        {
          msg: 'value is a boolean',
          value: {
            items: false,
            options: undefined,
          },
          status: {
            ops: 1,
            get before(){
              return valueTestDescr.param[2].preloads.status;
            },
            after: {
              count: 1,
              curIndex: -1,
              minIndex: 0,
              maxIndex: 0,
              prevIndex: -1,
              nextIndex: -1,
              isEmpty: false,
            },
          },
        }, {
          msg: 'value is a number',
          value: {
            items: 123,
            options: undefined,
          },
          status: {
            ops: 1,
            get before(){
              return valueTestDescr.param[2].preloads.status;
            },
            after: {
              count: 1,
              curIndex: -1,
              minIndex: 0,
              maxIndex: 0,
              prevIndex: -1,
              nextIndex: -1,
              isEmpty: false,
            },
          },
        }, {
          msg: 'value is a string',
          value: {
            items: 'false',
            options: undefined,
          },
          status: {
            ops: 1,
            get before(){
              return valueTestDescr.param[2].preloads.status;
            },
            after: {
              count: 1,
              curIndex: -1,
              minIndex: 0,
              maxIndex: 0,
              prevIndex: -1,
              nextIndex: -1,
              isEmpty: false,
            },
          },
        }, {
          msg: 'value is a function',
          value: {
            items: TEST_FUNC,
            options: undefined,
          },
          status: {
            ops: 1,
            get before(){
              return valueTestDescr.param[2].preloads.status;
            },
            after: {
              count: 1,
              curIndex: -1,
              minIndex: 0,
              maxIndex: 0,
              prevIndex: -1,
              nextIndex: -1,
              isEmpty: false,
            },
          },
        }, {
          msg: 'value is an array',
          value: {
            items: [ 1, 2, 3 ],
            options: undefined,
          },
          status: {
            ops: 3,
            get before(){
              return valueTestDescr.param[2].preloads.status;
            },
            after: {
              count: 3,
              curIndex: -1,
              minIndex: 0,
              maxIndex: 2,
              prevIndex: -1,
              nextIndex: -1,
              isEmpty: false,
            },
          },
        }, {
          msg: 'value is an object',
          value: {
            items: {},
            options: undefined,
          },
          status: {
            ops: 1,
            get before(){
              return valueTestDescr.param[2].preloads.status;
            },
            after: {
              count: 1,
              curIndex: -1,
              minIndex: 0,
              maxIndex: 0,
              prevIndex: -1,
              nextIndex: -1,
              isEmpty: false,
            },
          },
        }, {
          msg: 'value is an instance of "Set"',
          value: {
            items: new Set([ 1, 2, 3 ]),
            options: undefined,
          },
          status: {
            ops: 3,
            get before(){
              return valueTestDescr.param[2].preloads.status;
            },
            after: {
              count: 3,
              curIndex: -1,
              minIndex: 0,
              maxIndex: 2,
              prevIndex: -1,
              nextIndex: -1,
              isEmpty: false,
            },
          },
        }, {
          msg: 'value is an instance of "Map"',
          value: {
            items: new Map([
              [ 'a', 1 ],
              [ 'b', 2 ],
              [ 'c', 3 ],
            ]),
            options: undefined,
          },
          status: {
            ops: 3,
            get before(){
              return valueTestDescr.param[2].preloads.status;
            },
            after: {
              count: 3,
              curIndex: -1,
              minIndex: 0,
              maxIndex: 2,
              prevIndex: -1,
              nextIndex: -1,
              isEmpty: false,
            },
          },
        },
      ],
    },
  ],
};

const optionTestDescr = {
  msg: 'run tests aganst "options" param',
  param: [
    {
      msg: 'non-valid args are given',
      preloads: preloads[2],
      param: [
        {
          msg: 'value is a "null"',
          value: {
            items: [ samples[0], samples[1] ],
            options: null,
          },
          status: {
            ops: 2,
            get before(){
              return optionTestDescr.param[0].preloads.status;
            },
            after: {
              count: 2,
              curIndex: -1,
              minIndex: 0,
              maxIndex: 1,
              prevIndex: -1,
              nextIndex: -1,
              isEmpty: false,
            },
          },
        },
      ],
    }, {
      msg: 'a valid args are given',
      preloads: preloads[2],
      param: [
        {
          msg: 'value is a boolean (options: false)',
          value: {
            items: [ samples[0], samples[1] ],
            options: false,
          },
          status: {
            ops: 2,
            get before(){
              return optionTestDescr.param[1].preloads.status;
            },
            after: {
              count: 9,
              curIndex: 2,
              minIndex: 0,
              maxIndex: 8,
              prevIndex: 1,
              nextIndex: 3,
              isEmpty: false,
            },
          },
        }, {
          msg: 'value is a boolean (options: true)',
          value: {
            items: [ samples[0], samples[1] ],
            options: true,
          },
          status: {
            ops: 2,
            get before(){
              return optionTestDescr.param[1].preloads.status;
            },
            after: {
              count: 2,
              curIndex: -1,
              minIndex: 0,
              maxIndex: 1,
              prevIndex: -1,
              nextIndex: -1,
              isEmpty: false,
            },
          },
        }, {
          msg: 'value is an object (options: "useClear" => false)',
          value: {
            items: [ samples[0], samples[1] ],
            options: {
              useClear: false,
            },
          },
          status: {
            ops: 2,
            get before(){
              return optionTestDescr.param[1].preloads.status;
            },
            after: {
              count: 9,
              curIndex: 2,
              minIndex: 0,
              maxIndex: 8,
              prevIndex: 1,
              nextIndex: 3,
              isEmpty: false,
            },
          },
        }, {
          msg: 'value is an object (options: "useClear" => true)',
          value: {
            items: [ samples[0], samples[1] ],
            options: {
              useClear: true,
            },
          },
          status: {
            ops: 2,
            get before(){
              return optionTestDescr.param[1].preloads.status;
            },
            after: {
              count: 2,
              curIndex: -1,
              minIndex: 0,
              maxIndex: 1,
              prevIndex: -1,
              nextIndex: -1,
              isEmpty: false,
            },
          },
        },
      ],
    }, {
      msg: 'check if options appied',
      preloads: preloads[2],
      param: [
        {
          msg: 'options: "useClear" => false && "resetIndex" => true',
          value: {
            items: [ samples[0], samples[1] ],
            options: {
              useClear: false,
              resetIndex: true,
            },
          },
          status: {
            ops: 2,
            get before(){
              return optionTestDescr.param[2].preloads.status;
            },
            after: {
              count: 9,
              curIndex: -1,
              minIndex: 0,
              maxIndex: 8,
              prevIndex: -1,
              nextIndex: -1,
              isEmpty: false,
            },
          },
        }, {
          msg: 'options: "useClear" => false && "curIndex" => <value>',
          value: {
            items: [ samples[0], samples[1] ],
            options: {
              useClear: false,
              curIndex: 4,
            },
          },
          status: {
            ops: 2,
            get before(){
              return optionTestDescr.param[2].preloads.status;
            },
            after: {
              count: 9,
              curIndex: 4,
              minIndex: 0,
              maxIndex: 8,
              prevIndex: 3,
              nextIndex: 5,
              isEmpty: false,
            },
          },
        }, {
          msg: 'options: "useClear" => false && "resetIndex" => true && "curIndex" => <value>',
          value: {
            items: [ samples[0], samples[1] ],
            options: {
              useClear: false,
              resetIndex: true,
              curIndex: 4,
            },
          },
          status: {
            ops: 2,
            get before(){
              return optionTestDescr.param[2].preloads.status;
            },
            after: {
              count: 9,
              curIndex: 4,
              minIndex: 0,
              maxIndex: 8,
              prevIndex: 3,
              nextIndex: 5,
              isEmpty: false,
            },
          },
        }, {
          msg: 'options: "useClear" => true && "curIndex" => <value>',
          value: {
            items: [ samples[0], samples[1] ],
            options: {
              useClear: true,
              curIndex: 1,
            },
          },
          status: {
            ops: 2,
            get before(){
              return optionTestDescr.param[2].preloads.status;
            },
            after: {
              count: 2,
              curIndex: 1,
              minIndex: 0,
              maxIndex: 1,
              prevIndex: 0,
              nextIndex: 1,
              isEmpty: false,
            },
          },
        },
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    valueTestDescr,
    optionTestDescr,
  ],
};
