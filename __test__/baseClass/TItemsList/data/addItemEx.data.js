// [v0.1.003-20230624]

/* module:   `baseClass`
 * class:    TItemsList
 * method:   addItemEx
 * property: ---
 */

const {
  initTest,
} = require('../../../test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

/*const samples = [
  'sam_t0', 'sam_t1', 'sam_t2', 'sam_t3', 'sam_t4', 'sam_t5', 'sam_t6',
];*/

const preloads = {
  EMPTY_CONTAINER: {
    options: undefined,
    items: [],
    curIndex: undefined,
    status: {
      count: 0,
      curIndex: -1,
    },
  },
  /*items: samples,
  curIndex: -1,
  status: {
    count: 7,
    curIndex: -1,
    isEmpty: false,
  },*/
};

const valueTestDescr = {
  msg: 'run tests against "item" param',
  rem: '(defaults is used)',
  param: [
    {
      msg: 'no args are given',
      param: [
        ({
          msg: 'undefined value is passed',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: undefined,
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: -1,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.EMPTY_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'args are given',
      param: [
        ({
          msg: 'value is a "null"',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: null,
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a boolean',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: true,
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a number',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: 21,
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a string',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: 'null',
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a function',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: TEST_FUNC,
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is an array',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: [ 1, 2, 3 ],
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is an object',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: { items: [ 1, 2, 3 ] },
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

const optionTestDescr = {
  msg: 'run tests aganst "options" param',
  rem: '',
  param: [
    {
      msg: 'no args are given',
      param: [
        ({
          msg: 'undefined value is passed',
          rem: '(defaults will be used)',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: 'apple',
            opt: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        ({
          msg: 'value is a "null"',
          rem: '(defaults will be used - value ignored)',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: 'apple',
            opt: null,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        ({
          msg: 'value is a boolean',
          rem: '(options: false)',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: 'apple',
            opt: false,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a boolean',
          rem: '(options: true)',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: 'apple',
            opt: true,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: 0,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
    optionTestDescr,
  },
  tests: [
    valueTestDescr,
    optionTestDescr,
  ],
};
