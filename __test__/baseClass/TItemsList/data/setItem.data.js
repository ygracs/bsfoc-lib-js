// [v0.1.002-20230912]

/* module:   `baseClass`
 * class:    TItemsList
 * method:   setItem
 * property: ---
 */

const {
  initTest,
} = require('../../../test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const preloads = {
  TEST_CONTAINER: {
    options: undefined,
    items: [ 'sam_t0', 'sam_t1', 'sam_t2', 'sam_t3', 'sam_t4' ],
    curIndex: undefined,
    status: {
      count: 5,
      curIndex: -1,
    },
  },
};

const valueTestDescr_1 = {
  msg: 'run tests against "index" param',
  rem: '',
  param: [
    {
      msg: 'no args are given',
      param: [
        ({
          msg: 'undefined value is passed',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: undefined,
            item: 'sam_t5',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        ({
          msg: 'value is a "null"',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: null,
            item: 'sam_t5',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a boolean',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: true,
            item: 'sam_t5',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a string',
          rem: '(not convertable to a number)',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: 'not a number',
            item: 'sam_t5',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'valid args are given',
      param: [
        ({
          msg: 'value is a positive number',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: 3,
            item: 'sam_t5',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a string',
          rem: '(convertable to a positive number)',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: '2',
            item: 'sam_t5',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

const valueTestDescr_2 = {
  msg: 'run tests against "item" param',
  rem: '',
  param: [
    {
      msg: 'no args are given',
      param: [
        ({
          msg: 'undefined value is passed',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: 2,
            item: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'args are given',
      param: [
        ({
          msg: 'value is a "null"',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: 2,
            item: null,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a boolean',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: 2,
            item: true,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a number',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: 3,
            item: 5,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a string',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: 3,
            item: 'some text',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

const rangeTestDescr = {
  msg: 'run tests against "index" range',
  rem: '',
  param: [
    {
      msg: 'index value is out of range',
      param: [
        ({
          msg: 'value is less than "0"',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: -15,
            item: 'some value',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is greater than "maxIndex"',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: 15,
            item: 'some value',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'index is in range',
      param: [
        ({
          msg: 'value is a positive number',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: 2,
            item: 'some value',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

module.exports = {
  descr: {
    valueTestDescr_1,
    rangeTestDescr,
    valueTestDescr_2,
  },
  tests: [
    valueTestDescr_1,
    rangeTestDescr,
    valueTestDescr_2,
  ],
};
