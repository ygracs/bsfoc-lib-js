// [v0.1.003-20230911]

/* module:   `baseClass`
 * class:    TItemsList
 * method:   clear
 * property: ---
 */

const {
  initTest,
} = require('../../../test-hfunc.js');

const preloads = {
  EMPTY_CONTAINER: {
    options: undefined,
    items: [],
    curIndex: undefined,
    status: {
      count: 0,
      curIndex: -1,
    },
  },
  TEST_CONTAINER_1: {
    options: undefined,
    items: [ 'sam_t0', 'sam_t1', 'sam_t2' ],
    curIndex: undefined,
    status: {
      count: 3,
      curIndex: -1,
    },
  },
  TEST_CONTAINER_2: {
    options: undefined,
    items: [ 'sam_t0', 'sam_t1', 'sam_t2', 'sam_t3', 'sam_t4' ],
    curIndex: 2,
    status: {
      count: 5,
      curIndex: 2,
    },
  },
};

resultTestDescr = {
  msg: 'run tests against result value',
  param: [
    {
      msg: 'some elements was loaded',
      param: [
        ({
          msg: 'perform test',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: undefined,
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: undefined,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.EMPTY_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

module.exports = {
  descr: {
    resultTestDescr,
  },
  tests: [
    resultTestDescr,
  ],
};
