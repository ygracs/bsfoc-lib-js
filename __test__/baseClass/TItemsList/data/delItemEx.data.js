// [v0.1.004-20230911]

/* module:   `baseClass`
 * class:    TItemsList
 * method:   delItemEx
 * property: ---
 */

const {
  initTest,
} = require('../../../test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const preloads = {
  TEST_CONTAINER_1: {
    options: undefined,
    items: [ 'sam_t0', 'sam_t1', 'sam_t2' ],
    curIndex: undefined,
    status: {
      count: 3,
      curIndex: -1,
    },
  },
  TEST_CONTAINER_2: {
    options: undefined,
    items: [ 'sam_t0', 'sam_t1', 'sam_t2', 'sam_t3', 'sam_t4' ],
    curIndex: 2,
    status: {
      count: 5,
      curIndex: 2,
    },
  },
  TEST_CONTAINER_3: {
    options: undefined,
    items: [ 'sam_t0', 'sam_t1', 'sam_t2', 'sam_t3', 'sam_t4' ],
    curIndex: 4,
    status: {
      count: 5,
      curIndex: 4,
    },
  },
  TEST_CONTAINER_5: {
    options: undefined,
    items: [ 'sam_t0' ],
    curIndex: undefined,
    status: {
      count: 1,
      curIndex: -1,
    },
  },
};

const valueTestDescr = {
  msg: 'run tests against "index" param',
  rem: '(defaults is used)',
  param: [
    {
      msg: 'no args are given',
      param: [
        ({
          msg: 'undefined value is passed',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            index: undefined,
            options: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: undefined,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER_1.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        ({
          msg: 'value is a "null"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            index: undefined,
            options: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: undefined,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER_1.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        ({
          msg: 'value is a positive number',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            index: 1,
            options: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_1.items[1],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 2,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a string',
          rem: '(convertable to a valid index)',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            index: '3',
            options: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_1.items[3],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 3,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

const rangeTestDescr = {
  msg: 'run tests against "index" range',
  rem: '(defaults is used)',
  param: [
    {
      msg: 'index is out of range',
      param: [
        ({
          msg: 'index is less than "0"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            index: -15,
            options: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: undefined,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER_1.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'index is greater than "maxIndex"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            index: 15,
            options: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: undefined,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER_1.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'index is in range',
      param: [
        ({
          msg: 'perform test',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            index: 2,
            options: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_1.items[2],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 2,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

const optionTestDescr = {
  msg: 'run tests against "options" param',
  rem: '',
  param: [
    {
      msg: 'non-valid args are given',
      param: [
        ({
          msg: 'value is a "null"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            index: 1,
            options: null,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_2.items[1],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 4,
              curIndex: 1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        ({
          msg: 'value is a boolean',
          rem: '(options: false)',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            index: 1,
            options: false,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_2.items[1],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 4,
              curIndex: 2,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a boolean',
          rem: '(options: true)',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            index: 1,
            options: true,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_2.items[1],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 4,
              curIndex: 1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

const resultTestDescr = {
  msg: 'run tests against special cases',
  param: [
    {
      msg: 'delete last element ("curIndex" is equal to "index")',
      param: [
        ({
          msg: 'options: "false"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_3,
          values: {
            index: 4,
            options: false,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_3.items[4],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 4,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'options: "true"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_3,
          values: {
            index: 4,
            options: true,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_3.items[4],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 4,
              curIndex: 3,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'delete element but not the last ("curIndex" is less than "index")',
      param: [
        ({
          msg: 'options: "false"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            index: 1,
            options: false,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_2.items[1],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 4,
              curIndex: 2,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'options: "true"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            index: 1,
            options: true,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_2.items[1],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 4,
              curIndex: 1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'delete element but not the last ("curIndex" is greater than "index")',
      param: [
        ({
          msg: 'options: "false"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            index: 3,
            options: false,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_2.items[3],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 4,
              curIndex: 2,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'options: "true"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            index: 3,
            options: true,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_2.items[3],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 4,
              curIndex: 2,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'delete element but not the last ("curIndex" is equal to "index")',
      param: [
        ({
          msg: 'options: "false"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            index: 2,
            options: false,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_2.items[2],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 4,
              curIndex: 2,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'options: "true"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            index: 2,
            options: true,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_2.items[2],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 4,
              curIndex: 2,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'delete element (instance has only one element)',
      param: [
        ({
          msg: 'options: "false"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_5,
          values: {
            index: 0,
            options: false,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_5.items[0],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 0,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'options: "true"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_5,
          values: {
            index: 0,
            options: true,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: preloads.TEST_CONTAINER_5.items[0],
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 0,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
    rangeTestDescr,
    optionTestDescr,
    resultTestDescr,
  },
  tests: [
    valueTestDescr,
    rangeTestDescr,
    optionTestDescr,
    resultTestDescr,
  ],
};
