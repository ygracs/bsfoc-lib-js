// [v0.1.003-20230912]

/* module:   `baseClass`
 * class:    TItemsList
 * method:   chkIndex
 * property: ---
 */

const {
  initTest,
} = require('../../../test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const preloads = {
  EMPTY_CONTAINER: {
    options: undefined,
    items: [],
    curIndex: undefined,
    status: {
      count: 0,
      curIndex: -1,
    },
  },
  TEST_CONTAINER: {
    options: undefined,
    items: [ 'sam_t0', 'sam_t1', 'sam_t2' ],
    curIndex: undefined,
    status: {
      count: 3,
      curIndex: -1,
    },
  },
};

const valueTestDescr = {
  msg: 'run tests against "index" param',
  rem: '',
  param: [
    {
      msg: 'no args are given',
      param: [
        ({
          msg: 'undefined value is passed',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        ({
          msg: 'value is a "null"',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: null,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a boolean',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: true,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a string',
          rem: '(not convertable to a number)',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: 'not a number',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'valid args are given',
      param: [
        ({
          msg: 'value is a positive number',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: 2,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a string',
          rem: '(convertable to a positive number)',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: '2',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

const rangeTestDescr = {
  msg: 'run tests against "index" range',
  rem: '',
  param: [
    {
      msg: 'index value is out of range',
      param: [
        ({
          msg: 'value is less than "0"',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: -127,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is greater than "maxIndex"',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: 15,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a string',
          rem: '(not convertable to a positive number)',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: '-127',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a string',
          rem: '(convertable to a positive number)',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: '127',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'index is in range',
      param: [
        ({
          msg: 'value is a positive number',
          rem: '',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: 1,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a string',
          rem: '(convertable to a positive number)',
          preloads: preloads.TEST_CONTAINER,
          values: {
            index: '1',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
    rangeTestDescr,
  },
  tests: [
    valueTestDescr,
    rangeTestDescr,
  ],
};
