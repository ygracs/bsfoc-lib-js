// [v0.1.007-20220831]

/* module:   `baseClass`
 * class:    TItemsList
 * method:   ---
 * property: ---
 */

module.exports = {
  main: {
    addItem: require('./addItem.data.js'),
    addItemEx: require('./addItemEx.data.js'),
    loadItems: require('./loadItems.data.js'),
    setItem: require('./setItem.data.js'),
    delItem: require('./delItem.data.js'),
    delItemEx: require('./delItemEx.data.js'),
    clear: require('./clear.data.js'),
    chkIndex: require('./chkIndex.data.js'),
    srchIndex: require('./srchIndex.data.js'),
    setIndex: require('./setIndex.data.js'),
  },
  parent: undefined,
  getProp: {},
  setProp: {
    curIndex: require('./setProp.curIndex.data.js'),
    curItem: require('./setProp.curItem.data.js'),
  },
};
