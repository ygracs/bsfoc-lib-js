// [v0.1.001-20220831]

/* module:   `baseClass`
 * class:    TItemsList
 * method:   ---
 * property: curItem
 */

const TEST_FUNC = (value) => { return value; };

const samples = [
  'sam_t0', 'sam_t1', 'sam_t2', 'sam_t3', 'sam_t4',
];

const preloads = {
  items: samples,
  curIndex: 2,
  status: {
    count: 5,
    curIndex: 2,
    isEmpty: false,
    items: [
      'sam_t0', 'sam_t1', 'sam_t2', 'sam_t3', 'sam_t4',
    ],
  },
};

const valueTestDescr = {
  msg: 'run tests aganst "item" param',
  param: [
    {
      msg: 'no args are given',
      preloads: preloads,
      param: [
        {
          msg: 'undefined value is passed',
          value: undefined,
          status: {
            ops: undefined,
            get before(){
              return valueTestDescr.param[0].preloads.status;
            },
            get after(){
              return valueTestDescr.param[0].preloads.status;
            },
          },
        },
      ],
    }, {
      msg: 'args are given',
      preloads: preloads,
      param: [
        {
          msg: 'value is a "null"',
          value: null,
          status: {
            ops: undefined,
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            after: {
              count: 5,
              curIndex: 2,
              isEmpty: false,
              items: [
                'sam_t0', 'sam_t1', null, 'sam_t3', 'sam_t4',
              ],
            },
          },
        }, {
          msg: 'value is a boolean',
          value: true,
          status: {
            ops: undefined,
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            after: {
              count: 5,
              curIndex: 2,
              isEmpty: false,
              items: [
                'sam_t0', 'sam_t1', true, 'sam_t3', 'sam_t4',
              ],
            },
          },
        }, {
          msg: 'value is a number',
          value: 5,
          status: {
            ops: undefined,
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            after: {
              count: 5,
              curIndex: 2,
              isEmpty: false,
              items: [
                'sam_t0', 'sam_t1', 5, 'sam_t3', 'sam_t4',
              ],
            },
          },
        }, {
          msg: 'value is a string',
          value: '__some_text__',
          status: {
            ops: undefined,
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            after: {
              count: 5,
              curIndex: 2,
              isEmpty: false,
              items: [
                'sam_t0', 'sam_t1', '__some_text__', 'sam_t3', 'sam_t4',
              ],
            },
          },
        }, {
          msg: 'value is a function',
          value: TEST_FUNC,
          status: {
            ops: undefined,
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            after: {
              count: 5,
              curIndex: 2,
              isEmpty: false,
              items: [
                'sam_t0', 'sam_t1', TEST_FUNC, 'sam_t3', 'sam_t4',
              ],
            },
          },
        },
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    valueTestDescr,
  ],
};
