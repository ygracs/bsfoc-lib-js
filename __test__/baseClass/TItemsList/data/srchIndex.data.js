// [v0.1.002-20230729]

/* module:   `baseClass`
 * class:    TItemsList
 * method:   srchIndex
 * property: ---
 */

const {
  initTest,
} = require('../../../test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

/*const samples_1 = [
  'sam_t0', 'sam_t1', 'sam_t2', 'sam_t3', 'sam_t4', 'sam_t5', 'sam_t6',
];*/

/*const samples_2 = [
  'sam_t0', null, true, 234, 'sam_t4', TEST_FUNC,
  [ 'sam_t5' ], { name: 'sam_t6' },
];*/

//const samples_3 = [ ...samples_1, ...samples_1 ];

const preloads = {
  TEST_CONTAINER_1: {
    options: undefined,
    items: [
      'sam_t0', null, true, 234, 'sam_t4', TEST_FUNC,
      [ 'sam_t5' ], { name: 'sam_t6' },
    ],
    curIndex: undefined,
    status: {
      count: 8,
      curIndex: -1,
    },
  },
  TEST_CONTAINER_2: {
    options: undefined,
    items: [
      'sam_t0', null, true, 234, 'sam_t4', TEST_FUNC,
      [ 'sam_t5' ], { name: 'sam_t6' },
      'sam_t0', null, true, 234, 'sam_t4', TEST_FUNC,
      [ 'sam_t5' ], { name: 'sam_t6' },
    ],
    curIndex: undefined,
    status: {
      count: 16,
      curIndex: -1,
    },
  },
};

const valueTestDescr = {
  msg: 'run tests against "item" param',
  rem: '',
  param: [
    {
      msg: 'no args are given',
      param: [
        ({
          msg: 'undefined value is passed',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            item: undefined,
            index: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: -1,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'args are given',
      param: [
        ({
          msg: 'value is a "null"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            item: null,
            index: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 1,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a boolean',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            item: true,
            index: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 2,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a number',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            item: 234,
            index: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 3,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a string',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            item: 'sam_t4',
            index: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 4,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

const indexTestDescr = {
  msg: 'run tests against "index" param',
  rem: '',
  param: [
    {
      msg: 'no args are given',
      param: [
        ({
          msg: 'undefined value is passed',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            item: 'sam_t4',
            index: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 4,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        ({
          msg: 'value is a "null"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            item: 'sam_t4',
            index: null,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 4,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'valid args are given',
      param: [
        ({
          msg: 'value is a positive number',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            item: 'sam_t4',
            index: 5,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 12,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a string that represents a positive number',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            item: 'sam_t4',
            index: '5',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 12,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

const rangeTestDescr = {
  msg: 'run tests aganst "index" range',
  rem: '',
  param: [
    {
      msg: 'index value is out of range',
      param: [
        ({
          msg: 'value is less than "0"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            item: 'sam_t4',
            index: -15,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 4,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is greater than "maxIndex"',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            item: 'sam_t4',
            index: 15,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: -1,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'index is in range',
      param: [
        ({
          msg: 'run test',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            item: 'sam_t4',
            index: 2,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 4,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

const resultTestDescr = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    {
      msg: 'target element is not exists',
      param: [
        ({
          msg: 'run test',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            item: 'sam_t8',
            index: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: -1,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'target element is exists',
      param: [
        ({
          msg: 'given index is ahead of the searched element',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            item: 'sam_t4',
            index: 2,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 4,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'given index is behind the searched element',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            item: 'sam_t4',
            index: 5,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: -1,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
    indexTestDescr,
    rangeTestDescr,
    resultTestDescr,
  },
  tests: [
    valueTestDescr,
    indexTestDescr,
    rangeTestDescr,
    resultTestDescr,
  ],
};
