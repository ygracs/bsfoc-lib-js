// [v0.1.001-20220831]

/* module:   `baseClass`
 * class:    TItemsList
 * method:   ---
 * property: curIndex
 */

const TEST_FUNC = (value) => { return value; };

const samples = [
  'sam_t0', 'sam_t1', 'sam_t2', 'sam_t3', 'sam_t4', 'sam_t5', 'sam_t6',
];

const preloads = {
  items: samples,
  curIndex: -1,
  status: {
    count: 7,
    curIndex: -1,
    isEmpty: false,
  },
};

const valueTestDescr = {
  msg: 'run tests aganst "index" param',
  param: [
    {
      msg: 'no args are given',
      preloads: preloads,
      param: [
        {
          msg: 'undefined value is passed',
          value: undefined,
          status: {
            ops: undefined,
            get before(){
              return valueTestDescr.param[0].preloads.status;
            },
            get after(){
              return valueTestDescr.param[0].preloads.status;
            },
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      preloads: preloads,
      param: [
        {
          msg: 'value is a "null"',
          value: null,
          status: {
            ops: undefined,
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            get after(){
              return valueTestDescr.param[1].preloads.status;
            },
          },
        }, {
          msg: 'value is a boolean',
          value: true,
          status: {
            ops: undefined,
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            get after(){
              return valueTestDescr.param[1].preloads.status;
            },
          },
        }, {
          msg: 'value is a string that not represents a number',
          value: 'not a number',
          status: {
            ops: undefined,
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            get after(){
              return valueTestDescr.param[1].preloads.status;
            },
          },
        },
      ],
    }, {
      msg: 'valid args are given',
      preloads: preloads,
      param: [
        {
          msg: 'value is a positive number',
          value: 3,
          status: {
            ops: undefined,
            get before(){
              return valueTestDescr.param[2].preloads.status;
            },
            after: {
              count: 7,
              curIndex: 3,
              isEmpty: false,
            },
          },
        }, {
          msg: 'value is a string that represents a positive number',
          value: '2',
          status: {
            ops: undefined,
            get before(){
              return valueTestDescr.param[2].preloads.status;
            },
            after: {
              count: 7,
              curIndex: 2,
              isEmpty: false,
            },
          },
        },
      ],
    },
  ],
};

const rangeTestDescr = {
  msg: 'run tests aganst "index" range',
  param: [
    {
      msg: 'index value is out of range',
      preloads: preloads,
      param: [
        {
          msg: 'value is less than "0"',
          value: -15,
          status: {
            ops: undefined,
            get before(){
              return rangeTestDescr.param[0].preloads.status;
            },
            get after(){
              return rangeTestDescr.param[0].preloads.status;
            },
          },
        }, {
          msg: 'value is greater than "maxIndex"',
          value: 15,
          status: {
            ops: undefined,
            get before(){
              return rangeTestDescr.param[0].preloads.status;
            },
            get after(){
              return rangeTestDescr.param[0].preloads.status;
            },
          },
        },
      ],
    }, {
      msg: 'index is in range',
      preloads: preloads,
      param: [
        {
          msg: 'value is a positive number',
          value: 4,
          status: {
            ops: undefined,
            get before(){
              return rangeTestDescr.param[1].preloads.status;
            },
            after: {
              count: 7,
              curIndex: 4,
              isEmpty: false,
            },
          },
        },
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    valueTestDescr,
    rangeTestDescr,
  ],
};
