// [v0.1.003-20230624]

/* module:   `baseClass`
 * class:    TItemsList
 * method:   addItem
 * property: ---
 */

const {
  initTest,
} = require('../../../test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

/*const samples = [
  'sam_t0', 'sam_t1', 'sam_t2', 'sam_t3', 'sam_t4', 'sam_t5', 'sam_t6',
];*/

const preloads = {
  EMPTY_CONTAINER: {
    options: undefined,
    items: [],
    curIndex: undefined,
    status: {
      count: 0,
      curIndex: -1,
    },
  },
  /*items: samples,
  curIndex: -1,
  status: {
    count: 7,
    curIndex: -1,
    isEmpty: false,
  },*/
};

const valueTestDescr = {
  msg: 'run tests against "item" param',
  rem: '',
  param: [
    {
      msg: 'no args are given',
      param: [
        ({
          msg: 'undefined value is passed',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.EMPTY_CONTAINER.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'args are given',
      param: [
        ({
          msg: 'value is a "null"',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: null,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a boolean',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: true,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a number',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: 21,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a string',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: 'null',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is a function',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: TEST_FUNC,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is an array',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: [ 1, 2, 3 ],
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'value is an object',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            item: { items: [ 1, 2, 3 ] },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: {
              count: 1,
              curIndex: -1,
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
  },
  tests: [
    valueTestDescr,
  ],
};
