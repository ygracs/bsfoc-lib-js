// [v0.1.020-20230912]

const { TItemsList } = require("#lib/baseClass.js");

const t_Dat = require('./data/index.data.js');

const { runTestFn } = require('../../test-hfunc.js');

// *** class: TItemsList()
describe('class: TItemsList()', () => {
  describe('1 - create new instance', () => {
    let testInst = null;
    it('done', () => {
      expect(() => { testInst = new TItemsList(); }).not.toThrow();
    });
    it('check instance status', () => {
      expect(testInst instanceof TItemsList).toBe(true);
      expect(testInst.count).toStrictEqual(0);
      expect(testInst.curIndex).toStrictEqual(-1);
      expect(testInst.curItem).toStrictEqual(undefined);
      expect(testInst.isEmpty()).toStrictEqual(true);
      expect(testInst.isNotEmpty()).toStrictEqual(false);
    });
  });

  describe('2 - add items to the instance', () => {
    describe.each([
      {
        msg: '2.1 - method: addItem()',
        method: 'addItem',
      }, {
        msg: '2.1 - method: addItemEx()',
        method: 'addItemEx',
      },
    ])('$msg', ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              testInst = new TItemsList();
              const { items, curIndex } = preloads;
              if (Array.isArray(items)) {
                items.forEach((item, index) => {
                  testInst.addItem(item);
                  if (index === curIndex) testInst.setIndex(index);
                });
              };
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_b.count);
              expect(testInst.curIndex).toStrictEqual(stat_b.curIndex);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = undefined; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  switch (typeof className) {
                    case 'string' : {
                      expect(result instanceof globalThis[className]).toBe(true);
                      if (className !== 'HTMLElement') break;
                    }
                    default : {
                      expect(result).toStrictEqual(value);
                      break;
                    }
                  };
                };
              };
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_a.count);
              expect(testInst.curIndex).toStrictEqual(stat_a.curIndex);
            });
          });
        });
      });
    });

    describe('2.3 - method: loadItems()', () => {
      const testData = t_Dat.main.loadItems;
      describe.each(testData.descr)('$msg', ({ param }) => {
        describe.each(param)('$msg', ({ preloads, param }) => {
          describe.each(param)('$msg', ({ value, status }) => {
            const testFn = (...args) => { return testInst.loadItems(...args); };
            const { before: stat_b, after: stat_a } = status;
            let testInst = null;
            beforeAll(() => {
              testInst = new TItemsList();
              const { items, curIndex } = preloads;
              if (Array.isArray(items)) {
                items.forEach((item) => { testInst.addItem(item); });
              };
              if (curIndex !== undefined) testInst.setIndex(curIndex);
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_b.count);
              expect(testInst.curIndex).toStrictEqual(stat_b.curIndex);
              expect(testInst.isEmpty()).toStrictEqual(stat_b.isEmpty);
              expect(testInst.isNotEmpty()).toStrictEqual(!stat_b.isEmpty);
            });
            it('perform ops', () => {
              const { items, options } = value;
              let result = undefined;
              expect(() => { result = testFn(items, options); }).not.toThrow(Error);
              expect(result).toStrictEqual(status.ops);
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_a.count);
              expect(testInst.curIndex).toStrictEqual(stat_a.curIndex);
              expect(testInst.isEmpty()).toStrictEqual(stat_a.isEmpty);
              expect(testInst.isNotEmpty()).toStrictEqual(!stat_a.isEmpty);
            });
          });
        });
      });
    });
  });

  describe('3 - check index range for an instance', () => {
    describe.each([
      {
        msg: '3.1 - method: chkIndex()',
        method: 'chkIndex',
      },
    ])('$msg', ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              testInst = new TItemsList();
              const { items, curIndex } = preloads;
              if (Array.isArray(items)) {
                items.forEach((item, index) => {
                  testInst.addItem(item);
                  if (index === curIndex) testInst.setIndex(index);
                });
              };
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_b.count);
              expect(testInst.curIndex).toStrictEqual(stat_b.curIndex);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = undefined; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  switch (typeof className) {
                    case 'string' : {
                      expect(result instanceof globalThis[className]).toBe(true);
                      if (className !== 'HTMLElement') break;
                    }
                    default : {
                      expect(result).toStrictEqual(value);
                      break;
                    }
                  };
                };
              };
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_a.count);
              expect(testInst.curIndex).toStrictEqual(stat_a.curIndex);
            });
          });
        });
      });
    });
  });

  describe('4 - delete items from an instance', () => {
    describe.each([
      {
        msg: '4.1 - method: delItem()',
        method: 'delItem',
      }, {
        msg: '4.2 - method: delItemEx()',
        method: 'delItemEx',
      }, {
        msg: '4.3 - method: clear()',
        method: 'clear',
      },
    ])('$msg', ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              testInst = new TItemsList();
              const { items, curIndex } = preloads;
              if (Array.isArray(items)) {
                items.forEach((item, index) => {
                  testInst.addItem(item);
                  if (index === curIndex) testInst.setIndex(index);
                });
              };
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_b.count);
              expect(testInst.curIndex).toStrictEqual(stat_b.curIndex);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = undefined; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  switch (typeof className) {
                    case 'string' : {
                      expect(result instanceof globalThis[className]).toBe(true);
                      if (className !== 'HTMLElement') break;
                    }
                    default : {
                      expect(result).toStrictEqual(value);
                      break;
                    }
                  };
                };
              };
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_a.count);
              expect(testInst.curIndex).toStrictEqual(stat_a.curIndex);
            });
          });
        });
      });
    });
  });

  describe('5 - search items in the instance', () => {
    describe.each([
      {
        msg: '5.1 - method: srchIndex()',
        method: 'srchIndex',
      },
    ])('$msg', ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              testInst = new TItemsList();
              const { items, curIndex } = preloads;
              if (Array.isArray(items)) {
                items.forEach((item, index) => {
                  testInst.addItem(item);
                  if (index === curIndex) testInst.setIndex(index);
                });
              };
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_b.count);
              expect(testInst.curIndex).toStrictEqual(stat_b.curIndex);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = undefined; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  switch (typeof className) {
                    case 'string' : {
                      expect(result instanceof globalThis[className]).toBe(true);
                      if (className !== 'HTMLElement') break;
                    }
                    default : {
                      expect(result).toStrictEqual(value);
                      break;
                    }
                  };
                };
              };
            });
          });
        });
      });
    });
  });

  describe('6 - select current item in the instance', () => {
    describe.each([
      {
        msg: '6.1 - method: setIndex()',
        method: 'setIndex',
      },
    ])('$msg', ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              testInst = new TItemsList();
              const { items, curIndex } = preloads;
              if (Array.isArray(items)) {
                items.forEach((item, index) => {
                  testInst.addItem(item);
                  if (index === curIndex) testInst.setIndex(index);
                });
              };
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_b.count);
              expect(testInst.curIndex).toStrictEqual(stat_b.curIndex);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = undefined; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  switch (typeof className) {
                    case 'string' : {
                      expect(result instanceof globalThis[className]).toBe(true);
                      if (className !== 'HTMLElement') break;
                    }
                    default : {
                      expect(result).toStrictEqual(value);
                      break;
                    }
                  };
                };
              };
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_a.count);
              expect(testInst.curIndex).toStrictEqual(stat_a.curIndex);
            });
          });
        });
      });
    });

    describe('6.2 - property: curIndex (direct excess)', () => {
      const testData = t_Dat.setProp.curIndex;
      describe.each(testData.descr)('$msg', ({ param }) => {
        describe.each(param)('$msg', ({ preloads, param }) => {
          describe.each(param)('$msg', ({ value, status }) => {
            const testFn = (value) => { testInst['curIndex'] = value; };
            const { before: stat_b, after: stat_a } = status;
            let testInst = null;
            beforeAll(() => {
              testInst = new TItemsList();
              const { items, curIndex } = preloads;
              if (Array.isArray(items)) {
                items.forEach((item) => { testInst.addItem(item); });
              };
              if (curIndex !== undefined) testInst.setIndex(curIndex);
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_b.count);
              expect(testInst.curIndex).toStrictEqual(stat_b.curIndex);
              expect(testInst.isEmpty()).toStrictEqual(stat_b.isEmpty);
              expect(testInst.isNotEmpty()).toStrictEqual(!stat_b.isEmpty);
            });
            it('perform ops', () => {
              expect(() => { testFn(value); }).not.toThrow(Error);
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_a.count);
              expect(testInst.curIndex).toStrictEqual(stat_a.curIndex);
              expect(testInst.isEmpty()).toStrictEqual(stat_a.isEmpty);
              expect(testInst.isNotEmpty()).toStrictEqual(!stat_a.isEmpty);
            });
          });
        });
      });
    });
  });

  describe('7 - replace items in the instance', () => {
    describe.each([
      {
        msg: '7.1 - method: setItem()',
        method: 'setItem',
      },
    ])('$msg', ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
        describe.each(param)('+ $msg', ({ param }) => {
          describe.each(param)('+ $msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              testInst = new TItemsList();
              const { items, curIndex } = preloads;
              if (Array.isArray(items)) {
                items.forEach((item, index) => {
                  testInst.addItem(item);
                  if (index === curIndex) testInst.setIndex(index);
                });
              };
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_b.count);
              expect(testInst.curIndex).toStrictEqual(stat_b.curIndex);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = undefined; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  switch (typeof className) {
                    case 'string' : {
                      expect(result instanceof globalThis[className]).toBe(true);
                      if (className !== 'HTMLElement') break;
                    }
                    default : {
                      expect(result).toStrictEqual(value);
                      break;
                    }
                  };
                };
              };
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_a.count);
              expect(testInst.curIndex).toStrictEqual(stat_a.curIndex);
            });
          });
        });
      });
    });

    describe('7.2 - property: curItem (direct excess)', () => {
      const testData = t_Dat.setProp.curItem;
      describe.each(testData.descr)('$msg', ({ param }) => {
        describe.each(param)('$msg', ({ preloads, param }) => {
          describe.each(param)('$msg', ({ value, status }) => {
            const testFn = (value) => { testInst['curItem'] = value; };
            const { before: stat_b, after: stat_a } = status;
            let testInst = null;
            beforeAll(() => {
              testInst = new TItemsList();
              const { items, curIndex } = preloads;
              if (Array.isArray(items)) {
                items.forEach((item) => { testInst.addItem(item); });
              };
              if (curIndex !== undefined) testInst.setIndex(curIndex);
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_b.count);
              expect(testInst.curIndex).toStrictEqual(stat_b.curIndex);
              expect(testInst.isEmpty()).toStrictEqual(stat_b.isEmpty);
              expect(testInst.isNotEmpty()).toStrictEqual(!stat_b.isEmpty);
            });
            it('check instance content (before)', () => {
              let result = [];
              for (let i = 0; i < testInst.count; i++) {
                result.push(testInst.getItem(i));
              };
              expect(result).toStrictEqual(stat_b.items);
            });
            it('perform ops', () => {
              expect(() => { testFn(value); }).not.toThrow(Error);
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TItemsList).toBe(true);
              expect(testInst.count).toStrictEqual(stat_a.count);
              expect(testInst.curIndex).toStrictEqual(stat_a.curIndex);
              expect(testInst.isEmpty()).toStrictEqual(stat_a.isEmpty);
              expect(testInst.isNotEmpty()).toStrictEqual(!stat_a.isEmpty);
            });
            it('check instance content (after)', () => {
              let result = [];
              for (let i = 0; i < testInst.count; i++) {
                result.push(testInst.getItem(i));
              };
              expect(result).toStrictEqual(stat_a.items);
            });
          });
        });
      });
    });
  });
});
