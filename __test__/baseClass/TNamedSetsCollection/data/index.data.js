// [v0.1.005-20230729]

/* module:   `baseClass`
 * class:    TNamedSetsCollection
 * method:   ---
 * property: ---
 */

module.exports = {
  main: {
    add: require('./add.data.js'),
    addTo: require('./addTo.data.js'),
    set: require('./set.data.js'),
    insert: require('./insert.data.js'),
    rename: require('./rename.data.js'),
    merge: require('./merge.data.js'),
    delete: require('./delete.data.js'),
    deleteFrom: require('./deleteFrom.data.js'),
    replace: require('./replace.data.js'),
    clear: require('./clear.data.js'),
  },
  parent: undefined,
  getProp: {},
  setProp: {},
};
