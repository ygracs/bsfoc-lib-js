// [v0.1.005-20230729]

/* module:   `baseClass`
 * class:    TNamedSetsCollection
 * method:   insert
 * property: ---
 */

const keys = [
  'key_321', 'key_125', 'key_234', 'key_345',
  'key_521', 'key_525', 'key_534', 'key_545',
]; // initial set of the item keys for tests

const items = [
  { index: 0, name: 'item_321' }, { index: 1, name: 'item_125' },
  { index: 2, name: 'item_234' }, { index: 3, name: 'item_345' },
  { index: 4, name: 'item_521' }, { index: 4, name: 'item_525' },
  { index: 6, name: 'item_534' }, { index: 7, name: 'item_545' },
]; // initial set of an item values for tests

// *** class: TNamedSetsCollection()
const descr = {
  preloads: {
    keys: [ keys[0], keys[1] ],
  },
  descr: [
    {
      msg: 'undefined',
      param: [
        {
          msg: 'exists',
          values: [ keys[0], undefined ],
          result: {
            ops: [ true, true ],
            obj: {
              keys: [ keys[0], keys[1] ],
              values: [ [], [] ],
            },
          },
        }, {
          msg: 'not exists',
          values: [ keys[3], undefined ],
          result: {
            ops: [ true, false ],
            obj: {
              keys: [ keys[0], keys[1], keys[3] ],
              values: [ [], [], [] ],
            },
          },
        },
      ],
    }, {
      msg: 'single item',
      param: [
        {
          msg: 'exists',
          values: [ keys[0], items[0] ],
          result: {
            ops: [ true, true ],
            obj: {
              keys: [ keys[0], keys[1] ],
              values: [ [ items[0] ], [] ],
            },
          },
        }, {
          msg: 'not exists',
          values: [ keys[3], items[1] ],
          result: {
            ops: [ true, false ],
            obj: {
              keys: [ keys[0], keys[1], keys[3] ],
              values: [ [ items[0] ], [], [ items[1] ] ],
            },
          },
        },
      ],
    }, {
      msg: 'an array of items',
      param: [
        {
          msg: 'exists',
          values: [ keys[0], [ items[0], items[1] ] ],
          result: {
            ops: [ true, true ],
            obj: {
              keys: [ keys[0], keys[1] ],
              values: [ [ items[0], items[1] ], [] ],
            },
          },
        }, {
          msg: 'not exists',
          values: [ keys[3], [ items[2], items[1] ] ],
          result: {
            ops: [ true, false ],
            obj: {
              keys: [ keys[0], keys[1], keys[3] ],
              values: [ [ items[0], items[1] ], [], [ items[2], items[1] ] ],
            },
          },
        },
      ],
    }, {
      msg: 'a set of items',
      param: [
        {
          msg: 'exists',
          values: [
            keys[0], new Set([ items[2], items[1] ]),
          ],
          result: {
            ops: [ true, true ],
            obj: {
              keys: [ keys[0], keys[1] ],
              values: [ [ items[2], items[1] ], [] ],
            },
          },
        }, {
          msg: 'not exists',
          values: [
            keys[3], new Set([ items[0], items[1] ]),
          ],
          result: {
            ops: [ true, false ],
            obj: {
              keys: [ keys[0], keys[1], keys[3] ],
              values: [ [ items[2], items[1] ], [], [ items[0], items[1] ] ],
            },
          },
        },
      ],
    },
  ],
};

module.exports = descr;
