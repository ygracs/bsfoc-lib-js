// [v0.1.005-20230729]

/* module:   `baseClass`
 * class:    TNamedSetsCollection
 * method:   delete
 * property: ---
 */

const keys = [
  'key_321', 'key_125', 'key_234', 'key_345',
  'key_521', 'key_525', 'key_534', 'key_545',
]; // initial set of the item keys for tests

const items = [
  { index: 0, name: 'item_321' }, { index: 1, name: 'item_125' },
  { index: 2, name: 'item_234' }, { index: 3, name: 'item_345' },
  { index: 4, name: 'item_521' }, { index: 4, name: 'item_525' },
  { index: 6, name: 'item_534' }, { index: 7, name: 'item_545' },
]; // initial set of an item values for tests

// *** class: TNamedSetsCollection()
// *** method: delete()
const descr = {
  preloads: {
    keys: [ keys[0], keys[2], keys[5] ],
    items: {
      [keys[0]]: [ items[2] ],
      [keys[2]]: [ items[3], items[0] ],
    },
  },
  descr: [
    {
      msg: 'not exists',
      value: keys[3],
      result: {
        ops: false,
        obj: {
          keys: [ keys[0], keys[2], keys[5] ],
          values: [ [ items[2] ], [ items[3], items[0] ], [] ],
        },
      },
    }, {
      msg: 'exists',
      value: keys[2],
      result: {
        ops: true,
        obj: {
          keys: [ keys[0], keys[5] ],
          values: [ [ items[2] ], [] ],
        },
      },
    },
  ],
};

module.exports = descr;
