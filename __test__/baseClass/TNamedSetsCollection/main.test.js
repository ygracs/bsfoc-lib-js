// [v0.1.009-20230624]

const {
  TNamedSetsCollection, TNamedItemsCollection
} = require("#lib/baseClass.js");

const t_Dat = require('./data/index.data.js');

let obj = null; // test object
let ekv = new Set(); // the value of an empty key

function unwrap_sets(line, value){
  let DBG = false;//typeof line === 'number';
  if (value instanceof Set) {
    if (DBG) console.log('CHECK: ['+line+'] unwrap_sets() - "Set" is received...');
    value = Array.from(value);
  } else if (Array.isArray(value)) {
    if (DBG) console.log('CHECK: ['+line+'] unwrap_sets() - "Array" is received...');
    let arr = [];
    for (let item of value) {
      arr.push((item instanceof Set) ? Array.from(item) : item);
    };
    value = arr;
  } else if (value === null){
    if (DBG) console.log('CHECK: ['+line+'] unwrap_sets() - "NULL" is received...');
  } else {
    if (DBG) console.log('CHECK: ['+line+'] unwrap_sets() - "'+typeof value+'" is received...');
  };
  return value;
};

function is_array_of_sets(value){
  let isSUCCEED = Array.isArray(value);
  if (isSUCCEED && value.length > 0) {
    for (let element of value) {
      if (!(element instanceof Set)) {
        isSUCCEED = false; break;
      };
    };
  };
  return isSUCCEED;
}

function preloads(obj, samples){
  for (let key of samples.keys) {
    if (
      obj.add(key) && samples.items && samples.items[key]
    ) {
      for (let item of samples.items[key]) { obj.addTo(key, item)};
    };
  };
}

// *** class: TNamedSetsCollection()
describe('class: TNamedSetsCollection()', () => {
  describe('1. create new instance', () => {
    let testInst = null;
    it('done', () => {
      expect(() => { testInst = new TNamedSetsCollection(); }).not.toThrow();
      expect(testInst instanceof TNamedSetsCollection).toBe(true);
    });
    it('check instance status', () => {
      expect(testInst instanceof TNamedSetsCollection).toBe(true);
      expect(testInst instanceof TNamedItemsCollection).toBe(true);
      expect(testInst.size).toBe(0);
      expect(testInst.keys).toStrictEqual([]);
      expect(is_array_of_sets(testInst.values)).toBe(true);
      expect(unwrap_sets(69, testInst.values)).toStrictEqual([]);
      expect(unwrap_sets(70, testInst.entries)).toStrictEqual([]);
      expect(typeof testInst.isValidKey).toBe('function');
      expect(typeof testInst.isValidItem).toBe('function');
    });
  });

  describe('2. add items to the instance', () => {
    let size = 0; // initial instance size for tests
    describe('2.1 - method: add()', () => {
      const testFn = (value) => { return obj.add(value); };
      const testData = t_Dat.main.add;
      beforeAll(() => { obj = new TNamedSetsCollection(); });
      beforeEach(() => { size = obj.size; });
      it.each(testData)('add $msg', ({ value, result }) => {
        expect(obj instanceof TNamedSetsCollection).toBe(true);
        expect(obj.size).toBe(size);
        expect(testFn(value)).toBe(result.ops);
        expect(obj.size).toBe(size + (result.ops ? 1 : 0));
        expect(obj.keys).toStrictEqual(result.obj.keys);
        expect(is_array_of_sets(obj.values)).toBe(true);
        expect(unwrap_sets(91, obj.values)).toStrictEqual(result.obj.values);
      });
    });

    describe('2.2 - method: addTo()', () => {
      const testFn = (...args) => { return obj.addTo(...args); };
      const testData = t_Dat.main.addTo;
      const samples = testData.preloads;
      describe.each(testData.descr)('key is $msg', ({ param }) => {
        beforeAll(() => {
          obj = new TNamedSetsCollection(); preloads(obj, samples);
          size = obj.size;
        });
        it.each(param)('$msg', ({ values, result }) => {
          expect(obj instanceof TNamedSetsCollection).toBe(true);
          expect(obj.size).not.toBe(0);
          expect(testFn(...values)).toBe(result.ops);
          expect(obj.size).toBe(size);
          expect(obj.keys).toStrictEqual(result.obj.keys);
          expect(is_array_of_sets(obj.values)).toBe(true);
          expect(unwrap_sets(112, obj.values)).toStrictEqual(result.obj.values);
        });
      });
    });

    describe('2.3 - method: set()', () => {
      const testFn = (...args) => { return obj.set(...args); };
      const testData = t_Dat.main.set;
      const samples = testData.preloads;
      describe.each(testData.descr)('"item" param is $msg', ({ param }) => {
        beforeAll(() => {
          obj = new TNamedSetsCollection(); preloads(obj, samples);
          size = obj.size;
        });
        it.each(param)('key is $msg', ({ values, result }) => {
          expect(obj instanceof TNamedSetsCollection).toBe(true);
          expect(obj.size).not.toBe(0);
          expect(testFn(...values)).toBe(result.ops);
          expect(obj.size).toBe(size);
          expect(obj.keys).toStrictEqual(result.obj.keys);
          expect(is_array_of_sets(obj.values)).toBe(true);
          expect(unwrap_sets(134, obj.values)).toStrictEqual(result.obj.values);
        });
      });
    });

    describe('2.4 - method: insert()', () => {
      const testFn = (...args) => { return obj.insert(...args); };
      const testData = t_Dat.main.insert;
      const samples = testData.preloads;
      describe.each(testData.descr)('"item" param is $msg', ({ param }) => {
        beforeAll(() => {
          obj = new TNamedSetsCollection(); preloads(obj, samples);
        });
        beforeEach(() => { size = obj.size; });
        it.each(param)('key is $msg', ({ values, result }) => {
          expect(obj instanceof TNamedSetsCollection).toBe(true);
          expect(obj.size).toBe(size);
          expect(testFn(...values)).toBe(result.ops[0]);
          expect(obj.size).toBe(size + (result.ops[1] ? 0 : 1));
          expect(obj.keys).toStrictEqual(result.obj.keys);
          expect(is_array_of_sets(obj.values)).toBe(true);
          expect(unwrap_sets(156, obj.values)).toStrictEqual(result.obj.values);
        });
      });
    });
  });

  describe('3. transforms the instance', () => {
    let size = 0; // initial instance size for tests
    describe('3.1 - method: rename()', () => {
      const testFn = (...args) => { return obj.rename(...args); };
      const testData = t_Dat.main.rename;
      const samples = testData.preloads;
      describe.each(testData.descr)('old key is $msg', ({ param }) => {
        beforeAll(() => {
          obj = new TNamedSetsCollection(); preloads(obj, samples);
        });
        beforeEach(() => { size = obj.size; });
        it.each(param)('$msg', ({ values, result }) => {
          expect(obj instanceof TNamedSetsCollection).toBe(true);
          expect(obj.size).not.toBe(0);
          expect(testFn(...values)).toBe(result.ops[0]);
          expect(obj.size).toBe(size + (result.ops[1] ? -1 : 0));
          expect(obj.keys).toStrictEqual(result.obj.keys);
          expect(is_array_of_sets(obj.values)).toBe(true);
          expect(unwrap_sets(181, obj.values)).toStrictEqual(result.obj.values);
        });
      });
    });

    describe('3.2 - method: merge()', () => {
      const testFn = (...args) => { return obj.merge(...args); };
      const testData = t_Dat.main.merge;
      const samples = testData.preloads;
      beforeEach(() => {
        obj = new TNamedSetsCollection(); preloads(obj, samples);
        size = obj.size;
      });
      it.each(testData.descr)('target key is $msg', ({ values, result }) => {
        expect(obj instanceof TNamedSetsCollection).toBe(true);
        expect(obj.size).not.toBe(0);
        expect(testFn(values[0], ...values[1])).toBe(result.ops);
        expect(obj.size).toBe(size - result.count);
        expect(obj.keys).toStrictEqual(result.obj.keys);
        expect(is_array_of_sets(obj.values)).toBe(true);
        expect(unwrap_sets(202, obj.values)).toStrictEqual(result.obj.values);
      });
    });
  });

  describe('4. delete items from the instance', () => {
    let size = 0; // initial instance size for tests
    describe('4.1 - method: delete()', () => {
      const testFn = (value) => { return obj.delete(value); };
      const testData = t_Dat.main.delete;
      const samples = testData.preloads;
      beforeAll(() => {
        obj = new TNamedSetsCollection(); preloads(obj, samples);
      });
      beforeEach(() => { size = obj.size; });
      it.each(testData.descr)('key is $msg', ({ value, result }) => {
        expect(obj instanceof TNamedSetsCollection).toBe(true);
        expect(obj.size).not.toBe(0);
        expect(testFn(value)).toBe(result.ops);
        expect(obj.size).toBe(size - (result.ops ? 1 : 0));
        expect(obj.keys).toStrictEqual(result.obj.keys);
        expect(is_array_of_sets(obj.values)).toBe(true);
        expect(unwrap_sets(225, obj.values)).toStrictEqual(result.obj.values);
      });
    });

    describe('4.2 - method: deleteFrom()', () => {
      const testFn = (...args) => { return obj.deleteFrom(...args); };
      const testData = t_Dat.main.deleteFrom;
      const samples = testData.preloads;
      describe.each(testData.descr)('key is $msg', ({ param }) => {
        beforeAll(() => {
          obj = new TNamedSetsCollection(); preloads(obj, samples);
        });
        beforeEach(() => { size = obj.size; });
        it.each(param)('$msg', ({ values, result }) => {
          expect(obj instanceof TNamedSetsCollection).toBe(true);
          expect(obj.size).not.toBe(0);
          expect(testFn(...values)).toBe(result.ops);
          expect(obj.size).toBe(size);
          expect(obj.keys).toStrictEqual(result.obj.keys);
          expect(is_array_of_sets(obj.values)).toBe(true);
          expect(unwrap_sets(246, obj.values)).toStrictEqual(result.obj.values);
        });
      });
    });

    describe('4.3 - method: replace()', () => {
      const testFn = (...args) => { return obj.replace(...args); };
      const testData = t_Dat.main.replace;
      const samples = testData.preloads;
      describe.each(testData.descr)('key is $msg', ({ param }) => {
        beforeAll(() => {
          obj = new TNamedSetsCollection(); preloads(obj, samples);
        });
        beforeEach(() => { size = obj.size; });
        it.each(param)('$msg', ({ values, result }) => {
          expect(obj instanceof TNamedSetsCollection).toBe(true);
          expect(obj.size).not.toBe(0);
          expect(testFn(...values)).toBe(result.ops);
          expect(obj.size).toBe(size);
          expect(obj.keys).toStrictEqual(result.obj.keys);
          expect(is_array_of_sets(obj.values)).toBe(true);
          expect(unwrap_sets(265, obj.values)).toStrictEqual(result.obj.values);
        });
      });
    });

    describe('4.4 - method: clear()', () => {
      const testFn = (...args) => { return obj.clear(...args); };
      const testData = t_Dat.main.clear;
      const samples = testData.preloads;
      beforeEach(() => {
        obj = new TNamedSetsCollection(); preloads(obj, samples);
      });
      it.each(testData.descr)('keys are $msg', ({ values, result }) => {
        expect(obj instanceof TNamedSetsCollection).toBe(true);
        expect(obj.size).not.toBe(0);
        expect(() => { testFn(...values); }).not.toThrow(Error);
        expect(obj.size).toBe(result.size);
        expect(obj.keys).toStrictEqual(result.keys);
        expect(is_array_of_sets(obj.values)).toBe(true);
        expect(unwrap_sets(290, obj.values)).toStrictEqual(result.values);
      });
    });
  });
});
