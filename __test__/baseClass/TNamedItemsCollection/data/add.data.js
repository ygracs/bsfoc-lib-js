// [v0.1.003-20230916]

/* module:   `baseClass`
 * class:    TNamedItemsCollection
 * method:   add
 * property: ---
 */

const {
  initTest,
} = require('../../../test-hfunc.js');

const preloads = {
  EMPTY_CONTAINER: {
    options: undefined,
    items: [],
    status: {
      size: 0,
      keys: [],
    },
  },
  TEST_CONTAINER_1: {
    options: undefined,
    items: [ 'key_321' ],
    status: {
      size: 1,
      keys: [ 'key_321' ],
    },
  },
};

const resultTestDescr = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    {
      msg: 'none keys are exists',
      param: [
        ({
          msg: 'add a new key',
          rem: '',
          preloads: preloads.EMPTY_CONTAINER,
          values: {
            value: 'key_234',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: {
              size: 1,
              keys: [ 'key_234' ],
              //values: [ null ],
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'some keys are exists',
      param: [
        ({
          msg: 'add the same key',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            value: 'key_321',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER_1.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'add new key',
          rem: '',
          preloads: preloads.TEST_CONTAINER_1,
          values: {
            value: 'key_234',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: {
              size: 2,
              keys: [ 'key_321', 'key_234' ],
              //values: [ null ],
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

module.exports = {
  descr: {
    resultTestDescr,
  },
  tests: [
    resultTestDescr,
  ],
};
