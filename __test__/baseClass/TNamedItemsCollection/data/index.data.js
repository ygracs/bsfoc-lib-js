// [v0.0.002-20230729]

/* module:   `baseClass`
 * class:    TNamedItemsCollection
 * method:   ---
 * property: ---
 */

const keys = [
  'key_321', 'key_125', 'key_234', 'key_345', 'key_457', 'key_543',
]; // initial set of the item keys for tests

const items = [
  'item_321', 'item_125', 'item_234', 'item_345', 'item_457', 'item_543',
]; // initial set of an item values for tests

const samples = [
  { key: keys[0], item: items[0] }, { key: keys[1], item: items[1] },
  { key: keys[2], item: items[2] }, { key: keys[3], item: items[3] },
];

module.exports = {
  main: {
    add: require('./add.data.js'),
    set: require('./set.data.js'),
    insert: require('./insert.data.js'),
    rename: require('./rename.data.js'),
    has: require('./has.data.js'),
    get: require('./get.data.js'),
    delete: require('./delete.data.js'),
    clear: require('./clear.data.js'),
  },
  parent: undefined,
  getProp: {},
  setProp: {},
};
