// [v0.1.003-20230916]

/* module:   `baseClass`
 * class:    TNamedItemsCollection
 * method:   rename
 * property: ---
 */

const {
  initTest,
} = require('../../../test-hfunc.js');

const preloads = {
  EMPTY_CONTAINER: {
    options: undefined,
    items: [],
    status: {
      size: 0,
      keys: [],
    },
  },
  TEST_CONTAINER_1: {
    options: undefined,
    items: [ 'key_321', 'key_125', 'key_234' ],
    status: {
      size: 3,
      keys: [ 'key_321', 'key_125', 'key_234' ],
    },
  },
  TEST_CONTAINER_2: {
    options: undefined,
    items: [
      { key: 'key_321', value: undefined },
      { key: 'key_125', value: 'item_125', },
      { key: 'key_234', value: undefined },
    ],
    status: {
      size: 3,
      keys: [ 'key_321', 'key_125', 'key_234' ],
    },
  },
};

const resultTestDescr = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    {
      msg: 'old key is not exists',
      param: [
        ({
          msg: 'perform test',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            key: 'key_237',
            newKey: 'key_457',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER_2.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    }, {
      msg: 'old key is exists',
      param: [
        ({
          msg: 'new key is not exists',
          rem: '[*]',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            key: 'key_321',
            newKey: 'key_457',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: {
              size: 3,
              keys: [ 'key_125', 'key_234', 'key_457' ],
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'new key is the same',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            key: 'key_321',
            newKey: 'key_321',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: {
              size: 3,
              keys: [ 'key_321', 'key_125', 'key_234' ],
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'new key is exists',
          rem: '(but is not the same)',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            key: 'key_321',
            newKey: 'key_125',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER_2.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

module.exports = {
  descr: {
    resultTestDescr,
  },
  tests: [
    resultTestDescr,
  ],
};
