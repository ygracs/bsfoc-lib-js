// [v0.1.003-20230916]

/* module:   `baseClass`
 * class:    TNamedItemsCollection
 * method:   delete
 * property: ---
 */

const {
  initTest,
} = require('../../../test-hfunc.js');

const preloads = {
  EMPTY_CONTAINER: {
    options: undefined,
    items: [],
    status: {
      size: 0,
      keys: [],
    },
  },
  TEST_CONTAINER_1: {
    options: undefined,
    items: [ 'key_321', 'key_125', 'key_234' ],
    status: {
      size: 3,
      keys: [ 'key_321', 'key_125', 'key_234' ],
    },
  },
  TEST_CONTAINER_2: {
    options: undefined,
    items: [
      { key: 'key_321', value: undefined },
      { key: 'key_125', value: 'item_125', },
      { key: 'key_234', value: undefined },
    ],
    status: {
      size: 3,
      keys: [ 'key_321', 'key_125', 'key_234' ],
    },
  },
};

const resultTestDescr = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    {
      msg: 'some keys are exists',
      param: [
        ({
          msg: 'key is not exists',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            value: 'key_237',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: undefined,
            after: preloads.TEST_CONTAINER_2.status,
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
        ({
          msg: 'key is exists',
          rem: '',
          preloads: preloads.TEST_CONTAINER_2,
          values: {
            value: 'key_125',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: undefined,
            after: {
              size: 2,
              keys: [ 'key_321', 'key_234' ],
            },
          },
          initTest(options){ return initTest(this, options); },
        }).initTest(),
      ],
    },
  ],
};

module.exports = {
  descr: {
    resultTestDescr,
  },
  tests: [
    resultTestDescr,
  ],
};
