// [v0.1.012-20230916]

const t_Mod = require("#lib/baseClass.js");
const { TNamedItemsCollection } = t_Mod;

const t_Dat = require('./data/index.data.js');

const { isPlainObject } = require("#lib/baseObj.js");

const { runTestFn } = require('../../test-hfunc.js');

// *** class: TNamedItemsCollection()
describe('class: TNamedItemsCollection()', () => {
  describe('1. create new instance', () => {
    let testInst = null;
    it('done', () => {
      expect(() => { testInst = new TNamedItemsCollection(); }).not.toThrow();
      expect(testInst instanceof TNamedItemsCollection).toBe(true);
    });
    it('check instance status', () => {
      expect(testInst instanceof TNamedItemsCollection).toBe(true);
      expect(testInst.size).toBe(0);
      expect(testInst.keys).toStrictEqual([]);
      expect(testInst.values).toStrictEqual([]);
      expect(testInst.entries).toStrictEqual([]);
      expect(typeof testInst.isValidKey).toBe('function');
    });
  });

  describe('2. add items to the instance', () => {
    describe.each([
      {
        msg: '2.1 - method: add()',
        method: 'add',
      }, {
        msg: '2.2 - method: set()',
        method: 'set',
      }, {
        msg: '2.3 - method: insert()',
        method: 'insert',
      }, {
        msg: '2.4 - method: rename()',
        method: 'rename',
      },
    ])('$msg', ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
        describe.each(param)('+ $msg', ({ param }) => {
          describe.each(param)('+ $msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              testInst = new TNamedItemsCollection();
              const { items } = preloads;
              if (Array.isArray(items)) {
                items.forEach((item, index) => {
                  if (!isPlainObject(item)) item = { key: item };
                  const { key, value } = item;
                  testInst.add(key);
                  if (value !== undefined) testInst.set(key, value);
                });
              };
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TNamedItemsCollection).toBe(true);
              expect(testInst.size).toStrictEqual(stat_b.size);
              expect(testInst.keys).toStrictEqual(stat_b.keys);
              //expect(testInst.values).toStrictEqual(stat_b.values);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = undefined; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  switch (typeof className) {
                    case 'string' : {
                      expect(result instanceof globalThis[className]).toBe(true);
                      if (className !== 'HTMLElement') break;
                    }
                    default : {
                      expect(result).toStrictEqual(value);
                      break;
                    }
                  };
                };
              };
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TNamedItemsCollection).toBe(true);
              expect(testInst.size).toStrictEqual(stat_a.size);
              expect(testInst.keys).toStrictEqual(stat_a.keys);
              //expect(testInst.values).toStrictEqual(stat_a.values);
            });
          });
        });
      });
    });

    /**/
  });

  describe('3. check items in the instance', () => {
    describe.each([
      {
        msg: '3.1 - method: has()',
        method: 'has',
      }, {
        msg: '3.2 - method: get()',
        method: 'get',
      },
    ])('$msg', ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
        describe.each(param)('+ $msg', ({ param }) => {
          describe.each(param)('+ $msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              testInst = new TNamedItemsCollection();
              const { items } = preloads;
              if (Array.isArray(items)) {
                items.forEach((item, index) => {
                  if (!isPlainObject(item)) item = { key: item };
                  const { key, value } = item;
                  testInst.add(key);
                  if (value !== undefined) testInst.set(key, value);
                });
              };
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TNamedItemsCollection).toBe(true);
              expect(testInst.size).toStrictEqual(stat_b.size);
              expect(testInst.keys).toStrictEqual(stat_b.keys);
              //expect(testInst.values).toStrictEqual(stat_b.values);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = undefined; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  switch (typeof className) {
                    case 'string' : {
                      expect(result instanceof globalThis[className]).toBe(true);
                      if (className !== 'HTMLElement') break;
                    }
                    default : {
                      expect(result).toStrictEqual(value);
                      break;
                    }
                  };
                };
              };
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TNamedItemsCollection).toBe(true);
              expect(testInst.size).toStrictEqual(stat_a.size);
              expect(testInst.keys).toStrictEqual(stat_a.keys);
              //expect(testInst.values).toStrictEqual(stat_a.values);
            });
          });
        });
      });
    });

    /**/
  });

  describe('4. delete items from the instance', () => {
    describe.each([
      {
        msg: '4.1 - method: delete()',
        method: 'delete',
      }, {
        msg: '4.2 - method: clear()',
        method: 'clear',
      },
    ])('$msg', ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
        describe.each(param)('+ $msg', ({ param }) => {
          describe.each(param)('+ $msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              testInst = new TNamedItemsCollection();
              const { items } = preloads;
              if (Array.isArray(items)) {
                items.forEach((item, index) => {
                  if (!isPlainObject(item)) item = { key: item };
                  const { key, value } = item;
                  testInst.add(key);
                  if (value !== undefined) testInst.set(key, value);
                });
              };
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TNamedItemsCollection).toBe(true);
              expect(testInst.size).toStrictEqual(stat_b.size);
              expect(testInst.keys).toStrictEqual(stat_b.keys);
              //expect(testInst.values).toStrictEqual(stat_b.values);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = undefined; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  switch (typeof className) {
                    case 'string' : {
                      expect(result instanceof globalThis[className]).toBe(true);
                      if (className !== 'HTMLElement') break;
                    }
                    default : {
                      expect(result).toStrictEqual(value);
                      break;
                    }
                  };
                };
              };
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TNamedItemsCollection).toBe(true);
              expect(testInst.size).toStrictEqual(stat_a.size);
              expect(testInst.keys).toStrictEqual(stat_a.keys);
              //expect(testInst.values).toStrictEqual(stat_a.values);
            });
          });
        });
      });
    });

    /**/
  });
});
