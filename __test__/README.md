>|***rev.*:**|0.0.2|
>|:---|---:|
>|date:|2023-06-24|

## Introduction

This paper describes a tests shipped within the package.

## Provided tests

- `baseFunc` module
  - main group of tests
  - functions `readAsString` and `readAsStringEx`
- `baseObj` module
  - main group of tests
  - function `readAsListS`
  - function `readAsListE`
- `baseClass` module
  - classes `TItemsList` and `TItemsListEx`
  - class `TNamedItemsCollection`
  - class `TNamedSetsCollection`
  - class `TItemsICollection`

## Use cases

### All tests

To run all tests available use the next command:

`npm test` or `npm run test`

### Separate module tests

To run separate tests for each module or class you can use `--object` option:

|option value|module or class name|
|---|---|
|`bsf`|`baseFunc`|
|`bso`|`baseObj`|
|`bsc`|`baseClass`|

> Example: `npm test -- --object=bsc`

### `baseFunc` module tests

To run a test for a `baseFunc` module use:

|test type|command|
|---|---|
|all tests|`npm run test-bsf`|
|main tests|`npm run test-bsf:main`|
|func `readAsString`|`npm run test-bsf:ras`|

### `baseObj` module tests

To run a test for a `baseObj` module use:

|test type|command|
|---|---|
|all tests|`npm run test-bso`|
|main tests|`npm run test-bso:main`|
|func `readAsListS`|`npm run test-bso:rals`|
|func `readAsListE`|`npm run test-bso:rale`|

### `baseClass` module tests

To run a test for a `baseClass` module use:

|test type|command|
|---|---|
|all tests|`npm run test-bsc`|
|class `TItemsList`|`npm run test-bsc:il`|
|class `TItemsListEx`|`npm run test-bsc:ilex`|
|class `TNamedItemsCollection`|`npm run test-bsc:nic`|
|class `TNamedSetsCollection`|`npm run test-bsc:nsc`|
|class `TItemsICollection`|`npm run test-bsc:iic`|
